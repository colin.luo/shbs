<?php

return array(
	'LOAD_EXT_CONFIG' 		=> 'db,info,email,safe,upfile,cache,route,app,alipay,sms,platform,rippleos_key',
	'APP_AUTOLOAD_PATH'     =>'@.ORG',
	'OUTPUT_ENCODE'         =>  true, 			//页面压缩输出
	'PAGE_NUM'				=> 15,
	'OPEN_API_LOG' =>1,     //接口日志开关 默认打开
	'OPEN_API_DETAIL_LOG' =>1,//接口详细日志开关 默认关闭
	'OPEN_CERTIFICATE' =>1, //接口证书开关 默认关闭
	'DEFAULT_COMPANYCODE'=>'gznxbank',//默认companyCode
	'UPLOAD_RES_ADDR'=>'',//本地上传资源内部地址默认为空时为web服务器域名，取值格式：http://10.161.215.64/
	/*Cookie配置*/
	'COOKIE_PATH'           => '/',     		// Cookie路径
    'COOKIE_PREFIX'         => '',      		// Cookie前缀 避免冲突
	/*定义模版标签*/
	'TMPL_L_DELIM'   		=>'{pigcms:',			//模板引擎普通标签开始标记
	'TMPL_R_DELIM'			=>'}',				//模板引擎普通标签结束标记
	'RES_URL'=>'http://cdn.strong365.com:80/',  //图片，视频等资源服务器域名，所有上传和获取的图片均要做域名替换
	'SOURSE_URL'=>'http://120.26.101.143:8080/',
	'WAP_URL'=>'http://edu.testvms.com/',
	'INTERFACE_URL'=>'http://120.26.101.143:80/welearning/api/',//java接口地址
	 // 'INTERFACE_URL'=>'http://172.16.18.117:8080/wetogether/api/',//java接口地址
	//'INTERFACE_URL'=>'http://172.16.0.32:8080/wetogether/api/',//java接口地址
	'OPEN_PLATFORM_MAINTENANCE'=>array(
		'open'=>0,    //0关闭，1打开  默认0
		'title'=>'周周学维护公告',
		'content'=>'本服务器将于2016年10月28日21:30-2016年10月29日00:30进行维护,在此期间给您带来不便，请谅解，谢谢！！！',
		'footer'=>'上海思创网络有限公司',
	)
);
?>