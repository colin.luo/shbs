<?php
//判断客户端类型
function isAndroid(){
	if(strstr($_SERVER['HTTP_USER_AGENT'],'Android')) {
		return 1;
	}
	return 0;
}

function arr_htmlspecialchars($value){
	return is_array($value) ? array_map('htmlspecialchars', $value) : htmlspecialchars($value);
}

function urlGetTpl($action){
	$url_content = curl_get_tpl($action);
	return $url_content;
}

function subTitle($str){
 	if (strlen($str) > 18){
 		return mb_substr($str,0,8,'utf-8')."...";
 	}
 	return $str;
}

//表情符号替换为具体图片
function emothionFilter($content){
    $emotion=array(
     "/::)", "/::~", "/::B", "/::|", "/:8-)", "/::<", "/::$", "/::X", "/::Z", "/::'(", "/::-|", "/::@", "/::P", "/::D", "/::O", "/::(", "/::+", "/:–b", "/::Q", "/::T", "/:,@P", "/:,@-D", "/::d", "/:,@o", "/::g", "/:|-)", "/::!", "/::L", "/::>", "/::,@", "/:,@f", "/::-S", "/:?", "/:,@x", "/:,@@", "/::8", "/:,@!", "/:!!!", "/:xx", "/:bye", "/:wipe", "/:dig", "/:handclap", "/:&-(", "/:B-)", "/:<@", "/:@>", "/::-O", "/:>-|", "/:P-(", "/::'|", "/:X-)", "/::*", "/:@x", "/:8*", "/:pd", "/:<W>", "/:beer", "/:basketb", "/:oo", "/:coffee", "/:eat", "/:pig", "/:rose", "/:fade", "/:showlove", "/:heart", "/:break", "/:cake", "/:li", "/:bome", "/:kn", "/:footb", "/:ladybug", "/:shit", "/:moon", "/:sun", "/:gift", "/:hug", "/:strong", "/:weak", "/:share", "/:v", "/:@)", "/:jj", "/:@@", "/:bad", "/:lvu", "/:no", "/:ok", "/:love", "/:<L>", "/:jump", "/:shake", "/:<O>", "/:circle", "/:kotow", "/:turn", "/:skip", "[挥手]", "/:#-0", "[街舞]", "/:kiss", "/:<&", "/:&>"
    );
    $stctic=STATICS.'/forum/images/';
    $imgs=array_flip($emotion);
    foreach ($imgs as $k=>$v){
     $path[]="<img src='".$stctic.$v.".gif' data-innerhtml='$k' />";
    }
    $res=str_replace($emotion, $path,$content);
    
    unset($path);
    $emotion=array("[微笑]", "[撇嘴]", "[色]", "[发呆]", "[得意]", "[流泪]", "[害羞]", "[闭嘴]", "[睡]", "[大哭]", "[尴尬]", "[发怒]", "[调皮]", "[吡牙]", "[惊讶]", "[难过]", "[酷]", "[冷汗]", "[抓狂]", "[吐]", "[偷笑]", "[愉快]", "[白眼]", "[傲慢]", "[饥饿]", "[困]", "[惊恐]", "[流汗]", "[憨笑]", "[悠闲]", "[奋斗]", "[咒骂]", "[疑问]", "[嘘]", "[晕]", "[疯了]", "[衰]", "[骷髅]", "[敲打]", "[再见]", "[擦汗]", "[抠鼻]", "[鼓掌]", "[糗大了]", "[坏笑]", "[左哼哼]", "[右哼哼]", "[哈欠]", "[鄙视]", "[委屈]", "[快哭了]", "[阴险]", "[亲亲]", "[吓]", "[可怜]", "[菜刀]", "[西瓜]", "[啤酒]", "[篮球]", "[乒乓]", "[咖啡]", "[饭]", "[猪头]", "[玫瑰]", "[凋谢]", "[嘴唇]", "[爱心]", "[心碎]", "[蛋糕]", "[闪电]");
    
    $imgs=array_flip($emotion);
    foreach ($imgs as $k=>$v){
        $path[]="<img src='".$stctic.$v.".gif' title='$k'/>";
    }
    $res=str_replace($emotion, $path,$res);
    return $res;
}
//将图片地址根据不同环境替换为具体域名地址，例如：RES_URLcontent/columbia/image/2016-06-08/03D357405F81301AA311749744776061/f57315e1.png
function spanResUrl($url){
    if($url=='')
        return ;
	$url = htmlspecialchars_decode($url);
	$newUrl = str_replace('RES_URL',C('RES_URL'), $url);
	$newUrl = str_replace('LOCAL_URL',C('site_url').'/', $newUrl);
	$urlhttp = strtolower(substr($newUrl,0,7));
	$urlhttps = strtolower(substr($newUrl,0,8));
	if($urlhttp != 'http://' && $urlhttps != 'https://') {
		$newUrl = C('RES_URL').$newUrl;
	}
	return $newUrl;
}

//与spanResUrl相反，将上传的文件根据不同环境域名替换为字符串
function splitResUrl($url){
    $find=strstr($url,'RES_URL');
    $find2=strstr($url,'LOCAL_URL');
    //原本地址中有RES_URL等关键字，直接退出
    if($find||$find2){
       return $url; 
    }
	$newUrl = str_replace(C('RES_URL'),'RES_URL',htmlspecialchars_decode($url));
	//替换http地址为RES_URL关键字，没有被替换则进行LOCAL_URL替换
	if($url == $newUrl && $url != ""){
	   $newUrl = str_replace(C('site_url')."/",'LOCAL_URL', $url);
	   $urlhttp = strtolower(substr($newUrl,0,7));
	   $urlhttps = strtolower(substr($newUrl,0,8));
	   //两种关键字都没有被替换，并且不是网络资源（比如:tpl/User/images/1.png）则加入LOCAL_URL前缀
	   if($url == $newUrl&&$urlhttp != 'http://' && $urlhttps != 'https://'){
	    	$newUrl = "LOCAL_URL".$newUrl;
	   }
	} 
	return $newUrl;
}
?>