<?php
class BaseAction extends Action{
	public $isAgent;
	public $home_theme;
	public $reg_needCheck;
	public $minGroupid;
	public $reg_validDays;
	public $reg_groupid;
	public $thisAgent;
	public $agentid;
	public $adminMp;
	public $siteUrl;
	public $isQcloud = false;
	protected function _initialize(){
		if (GROUP_NAME == 'Home'){
    		$themeDir =  './tpl/Home/' . $_GET['company'] . '/';
    		if (isset($_GET['company']) && is_dir($_SERVER['DOCUMENT_ROOT'].$themeDir)){
    			$themePath = './tpl/Home/' . $_GET['company'];
    		}else{
    			$themePath = './tpl/Home/' . C('DEFAULT_THEME');
    		}
            define('RES',$themePath.'/common');
		}else{		
			define('RES',THEME_PATH.'common');
		}
		define('STATICS',TMPL_PATH.'static');
		$this->assign('action',$this->getActionName());
		if (C('STATICS_PATH')){
			$staticPath='';
		}else {
			$staticPath=C('WAP_URL');//正式地址
		}
		if (C('RES_URL')){
		    defined("RES_URL")?RES_URL:define("RES_URL",C('RES_URL'));
		}
		$this->assign('staticPath',$staticPath);
		if (!$this->isAgent){
			$this->agentid=0;
			if (!C('site_logo')){
				$f_logo='tpl/Home/pigcms/common/images/logo-pigcms.png';
			}else {
				$f_logo=C('site_logo');
			}
			$this->home_theme=C('DEFAULT_THEME');
		}
		$this->siteUrl=C('site_url');
		$maintInfo = C('OPEN_PLATFORM_MAINTENANCE');
		if ($maintInfo['open'] == 1 && ACTION_NAME != 'maintenance'){
			if (IS_GET){				
				$this->redirect('Home/Index/maintenance',array('company'=>$companyCode));
			}
			else{
				echo '###'.C('site_url').'/index.php?g=Home&m=Index&a=index&company='.$companyCode;
			}	
			exit;	
		}
	}
    
	public function getRpcDataBase($path,$param,$companyCode,$certificate="", $json=true){		
		$url =C('INTERFACE_URL').$path;
		
		if ($path != "company/getByToken" && $companyCode !== 'welearning'){
			$param['companyCode'] = $companyCode;
			if ($param['companyCode'] == ""){
				$param['companyCode'] = C("DEFAULT_COMPANYCODE");
				\Log::write("compnayCode is null!".$path,'WARN');				
			}
		}		
		
		if ($certificate){
		  if (C('OPEN_CERTIFICATE') ){
			$param['certificate']= $certificate;
		  }
		}
		
		$timeout = 300;
		if ($path == 'project/getExcelContentExam'){
			$timeout = 3600;
		}
		if ($path == 'content/uploadDoc'){
		    $timeout = 13600;
		}
		if ($path == 'person/importPerson'){
		    $timeout = 13600;
		}
		
		/*
		$data = http_build_query($param); 		
		$opts = array(  
			'http'=>array(  
			'method'=>"POST",
			'timeout'=>$timeout,  
			'header'=>"Content-type: application/x-www-form-urlencoded\r\n",
			"Content-length:".strlen($data)."\r\n",  
			"Cookie: foo=bar\r\n" . "\r\n",  
			'content' => $data,  
			)  
		); 	
		
		$start_time = time();
	
		$cxContext = stream_context_create($opts);  							
		$result = file_get_contents($url, false, $cxContext); 
		$resultData = json_decode($result,true);		
		*/
		
		$start_time = time();
		$data_string =  http_build_query($param); 
		$ch = curl_init();
		if(stripos($url,"https://")!==FALSE){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSLVERSION, 1); 
		}
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
                curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
  
		$result = curl_exec($ch);
		$resultCode = curl_getinfo($ch);
		$errno = curl_errno( $ch );
		
		$errorStr = "";		
		if ($resultCode['http_code'] != 200){
			$errorStr = "#HTTPRESPCODE=".$resultCode['http_code']."#CURLCODE=".(int)$errno;
		}
		curl_close($ch);
		$resultData = json_decode($result,true);		
		
		//统计耗时接口
		$end_time = time();
		$length_time = $end_time - $start_time;
		if (C('OPEN_API_LOG') ){			
			$log = "API:".$path."#TIMELENGHT=".$length_time."#ERRORCODE=".$resultData['errorCode']."#ERRORMSG=".$resultData['errorMassage'].$errorStr;
			if (C('OPEN_API_DETAIL_LOG') && $path != 'courseStudyProcess/updateContentTimes'){
				
				$log .="\r\n#PARAMS=".json_encode($param)."\r\n#RESULT=".json_encode($resultData['data']);
			}			
			\Log::write($log,'WARN');		
		}
        /*
        * 11 证书正确
        * 12 证书错误    (证书无法解析)
        * 13 没有证书    (证书参数为空)
        * 14 证书过期    (有效时间为24小时)
        * 15 证书信息不一致     （传递companyCode和证书所保存的company信息不一致）
        */

		if ($resultData['errorCode'] >11 && $resultData['errorCode'] <= 15){			
			\Log::write("[".session('certificate')."]certificate failure,API=".$path."#errorCode=".$resultData['errorCode'],'WARN');
			return -10001;
		}
		
		if ( $path == "content/getBody"
		  || $path=='forumComment/getBody' 
		  || $path=='contentTemplate/getBody'
		  || $path=='forumTopic/getBody'
		  || $path=='publicNumber/getBody' 
		  || $path == 'noticePcSend/getContent'
		  || $path=='notice/getBody'
		  || $path=='mallProduct/getProductIntro'
		  || $path=='siteMapPath/getBody'
		  || $path=='question/getSuggest'
		  || $path=='BaseUtil/getText'
		  || $path=='content/uploadDoc'){
			return $result;	
		}

	 	if ($json){
			return json_decode($result,true);		
		}
		else{
			return $resultData;
		}
	}
	
	public function getclientip($type = 0,$adv=false) {
	    $type       =  $type ? 1 : 0;
	    static $ip  =   NULL;
	    if ($ip !== NULL) return $ip[$type];
	    if($adv){
	        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
	            $ip     =   $_SERVER['HTTP_CLIENT_IP'];
	        }elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	            $arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
	            $pos    =   array_search('unknown',$arr);
	            if(false !== $pos) unset($arr[$pos]);
	            $ip     =   trim($arr[0]);
	        }elseif (isset($_SERVER['REMOTE_ADDR'])) {
	            $ip     =   $_SERVER['REMOTE_ADDR'];
	        }
	    }elseif (isset($_SERVER['REMOTE_ADDR'])) {
	        $ip     =   $_SERVER['REMOTE_ADDR'];
	    }
	    // IP地址合法验证
	    $long = sprintf("%u",ip2long($ip));
	    $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
	    return $ip[$type];
	}
	
}

