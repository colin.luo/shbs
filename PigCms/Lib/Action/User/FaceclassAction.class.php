<?php
class FaceclassAction extends UserAction{
    public function _initialize(){
        parent::_initialize();
    }
    public function index(){
		$year = $this->getRpcData('msScheduledOffering/getNdList');
		$month = $this->getRpcData('msScheduledOffering/getYfList');
		$em = $this->getRpcData('msScheduledOffering/getEmList');
		$pm = $this->getRpcData('msScheduledOffering/getPmList');
		$vendor = $this->getRpcData('msScheduledOffering/getVendorList');
		$trainer = $this->getRpcData('msScheduledOffering/getTrainerList');
		$country = $this->getRpcData('msScheduledOffering/getCountry');
		$state = $this->getRpcData('msScheduledOffering/getCancelled');
		$this->assign('year',$year['data']);
		$this->assign('month',$month['data']);
		$this->assign('em',$em['data']);
		$this->assign('pm',$pm['data']);
		$this->assign('vendor',$vendor['data']);
		$this->assign('trainer',$trainer['data']);
		$this->assign('country',$country['data']);
		$this->assign('state',$state['data']);
		$this->display();
    }
	//获取城市
	public function getcity(){
		$country['country'] = $_GET['country'];
		$city = $this->getRpcData('msScheduledOffering/getCityBYCountry',$country);	
		echo json_encode($city);
		}
	//默认列表
	public function faceclasslist(){
		if(IS_POST){
				$fields['year'] = $_POST['year'];
				$fields['year'] = $_POST['month'];
				$fields['year'] = $_POST['em'];
				$fields['year'] = $_POST['pm'];
				$fields['year'] = $_POST['vendor'];
				$fields['year'] = $_POST['trainer'];
				$fields['year'] = $_POST['status'];
				$fields['year'] = $_POST['changed'];
				$fields['year'] = $_POST['country'];
				$fields['year'] = $_POST['city'];
				$fields['year'] = $_POST['scheduledOfferingID'];
				$fields['year'] = $_POST['title'];
				$list = $this->getRpcData('msScheduledOffering/getAdminScheduledList',$fields);
			}else{
				$list = $this->getRpcData('msScheduledOffering/getAdminScheduledList');
			}
        echo json_encode($list);
    }
    public function expenses(){
        $this->display();
    }

}
?>