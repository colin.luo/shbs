<?php 
class TestAction extends UserAction{
    public function _initialize(){
        parent::_initialize();
    }
    public function index(){
        $this->display();
    }
    
    public function query(){
        $keyword = $_GET['keyword'];
        $page = (int)$_GET['page'];
        $rows =(int) $_GET['rows'];
        
        if (isset($keyword)){
            $conf['keyword'] = $keyword;
        }else{
            $conf['keyword'] = "";
        }
        if(session('is_open_domain')){
            $domainId =$_GET['domainId'];
            $isCludePublic = $_GET['isCludePublic'];
            $conf['isCludePublic'] =(string)$isCludePublic;
            $conf['domainId'] =(int)$domainId;
        }
        $conf['pageNo'] = isset($page)?$page:1;
        $conf['pageSize'] = isset($rows)?$rows:15;
        $conf['publicNumberId'] = $this->publicNumberId;
        $conf['needTotalSize'] = "true";
        
        $list = $this->getRpcData('active/matchPage',$conf);
        foreach ($list['data']['records'] as $k=>$v) {
            $records[$k]['id'] = $v['id'];
            $records[$k]['cover'] = spanResUrl($v['cover']);
            $records[$k]['title'] = $v['title'];
            $records[$k]['keyword'] = $v['keyword'];
            $records[$k]['status'] = $v['status']=='show'?'已开启':'已关闭';
            $records[$k]['registerNumber'] = $v['registerNumber'];
            $records[$k]['domain_name'] = $v['domain_name'];
            $records[$k]['domain_type'] = $v['domain_type'];
            $records[$k]['update'] = $v['update'];
            $records[$k]['delete'] = $v['delete'];
            $records[$k]['insert'] = $v['insert'];
            $records[$k]['domain_type'] = $v['domain_type'];
            $records[$k]['quota'] = $v['quota'];
            $records[$k]['passNumber'] = $v['passNumber'];
            $records[$k]['active_time'] = date('Y-m-d H:i:s',$v['start_date']/1000)."-".date('Y-m-d H:i:s', $v['end_date']/1000);
        }
        $data['total'] = $list['data']['totalSize']?$list['data']['totalSize']:0;
        $data['rows'] = $records?$records:"";
        echo json_encode($data);
    }
}
?>