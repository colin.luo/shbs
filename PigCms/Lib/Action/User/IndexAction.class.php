<?php
class IndexAction extends UserAction{
	protected function _initialize() {
		parent::_initialize();
		}
	//公众帐号列表
	public function index(){		

		$statics = $this->getRpcData('publicNumber/getPublicConfig');
		$this->assign('statics',$statics['data']);
		
		$info = $this->getRpcData('publicNumber/allPublicInfo');

		$this->assign('info',$info['data']);
		$this->display();
	}
	//域切换
	public function setDomain(){
	    session('currDomainId',$this->_post('id'));
	    session('currDomainType',$this->_post('type'));
	    session('currDomainName',$this->_post('name'));
	    $domainSearchList=$this->getDomainSearchList();
	    session("domainList",$domainSearchList);
	}
	
	//
	public function get_token($randLength=6,$attatime=1,$includenumber=0){
		if ($includenumber){
			$chars='abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQEST123456789';
		}else {
			$chars='abcdefghijklmnopqrstuvwxyz';
		}
		$len=strlen($chars);
		$randStr='';
		for ($i=0;$i<$randLength;$i++){
			$randStr.=$chars[rand(0,$len-1)];
		}
		$tokenvalue=$randStr;
		if ($attatime){
			$tokenvalue=$randStr.time();
		}
		return $tokenvalue;
	}
	//添加公众帐号
	public function add(){
		if (IS_POST){
			
			//添加
			$fields['original_id']=$_POST['wid'];
			$fields['wechat_no']=$_POST['wechatNo'];
			$fields['name']=$_POST['name'];
			$fields['app_id']=$_POST['appId'];
			$fields['app_secret']=$_POST['appSecret'];
			$fields['token']= $this->get_token();
			$fields['encoding_aes_key']= $_POST['aeskey'];
			$fields['expiration_time']= $_POST['expiration_time'];
			if($_POST['qrcode']==''){
			  //  $this->error("公众号二维码不可为空!");
				echo "公众号二维码不可为空!";
				exit;
			}else{
                $fields['mall_qr_code']= splitResUrl($_POST['qrcode']);
			}
			$type = $_POST['publicType'];
			if ($type==1){
				$fields['public_type']="订阅号";
			}
			else if($type==2){
				$fields['public_type']="服务号";
			}
			else{
				$fields['public_type']="企业号";
			}
			$fields['is_authentication']=(bool)$_POST['is_authentication'];
			$data['fields'] = json_encode($fields);
		
			//update
			$result = $this->getRpcData('publicNumber/insert',$data);

			if ($result['errorCode'] == 0){
			//	$this->success('操作成功',U(MODULE_NAME.'/index'));
			  echo 5;
			  exit;
			}else{
				echo $result['errorMassage'];
				exit;
			//	$this->error('操作失败',U(MODULE_NAME.'/index'));
			 
			}
		}
		else{
		
			$this->display();
		}
	}
	public function edit(){
		if (IS_POST){
			$data['id'] = intval($_POST['id']);
			$fields['original_id']=$_POST['wid'];
			$fields['wechat_no']=$_POST['wechatNo'];
			$fields['name']=$_POST['name'];
			$fields['app_id']=$_POST['appId'];
			$fields['app_secret']=$_POST['appSecret'];
			$fields['encoding_aes_key']= $_POST['aeskey'];
			$fields['expiration_time']= $_POST['expiration_time'];
			if($_POST['qrcode']==''){
			  //  $this->error("公众号二维码不可为空");
				echo "公众号二维码不可为空!";
				exit;
			}else{
                $fields['mall_qr_code']= splitResUrl($_POST['qrcode']);
			}
			$type = $_POST['publicType'];
			if ($type==1){
				$fields['public_type']="订阅号";
			}
			else if($type==2){
				$fields['public_type']="服务号";
			}
			else{
				$fields['public_type']="企业号";
			}
			$fields['is_authentication']=(bool)$_POST['is_authentication'];
			$data['fields'] = json_encode($fields);
		
			//update
			$result = $this->getRpcData('publicNumber/update',$data);
			if ($result['errorCode'] == 0){
				echo 5;
				exit;
			}else{
				echo $result['errorMassage'];
				exit;
			
			}
		}
		else{
			$id = $_GET['id'];
			if (isset($id)){			
				$data['id'] =$id;
				
				$info = $this->getRpcData('publicNumber/get',$data);
				$type = $info['data']['public_type'];			
				 $info['data']["expiration_time"] = date("Y-m-d H:i:s", $info['data']["expiration_time"]/1000);
				 if($info['data']["mall_qr_code"]){
				 	$info['data']["mall_qr_code"] = spanResUrl($info['data']["mall_qr_code"]);
				 }
			}
			$this->assign('info',$info['data']);
			$this->display();
		}
	}
	
	public function del(){
		$conf['id']=$this->_get('id','intval');
		
		$result = $this->getRpcData('publicNumber/delete',$conf);
		if ($result['errorCode'] == 0){
			$this->success('操作成功',U(MODULE_NAME.'/index'));
		}
		else{
			$this->error('操作失败',U(MODULE_NAME.'/index'));
		}
	}
	public function apiInfo(){
		
		$pbid = $_GET['id'];
		$conf['id'] = $pbid;
		$info = $this->getRpcData('publicNumber/get',$conf);
		
		
		$this->assign('info',$info['data']);
		$this->display();
	}

	
	
	//首页欢迎页
    public function welcome(){
        $data=array();
        if(session('is_open_domain')){
            $data['isCludePublic'] ='true';
            $data['domainId'] =(int)session("currDomainId");
        }
        $res=$this->getRpcData('analysis/systemLearnTotals',$data);
        $this->assign('countInfo',$res['data']);
        
        $res=$this->getRpcData('analysis/statusDistributions',$data);
        $this->assign('userStatusInfo',$res['data']);
        
        $companyInfo=$this->publicInfo;
        $companyName=$companyInfo['name'];
        
        $currDomainId=(int)session("currDomainId");
        $currDomainType=session('currDomainType');
        $domainName='';
        if($currDomainType!='公共域'){
            $domain=$this->getRpcData('domain/get',array('id'=>$currDomainId));
            $domainName=$domain['data']['name'];
        }
        $name=$companyName.$domainName;
        $this->assign("welcomeName",$name);
        $this->display();
    }		
    
}
?>