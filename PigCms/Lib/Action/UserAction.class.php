<?php
class UserAction extends BaseAction {
	public $userGroup;
	public $token;
	public $user;
	public $userFunctions;
	public $wxuser;
	public $usertplid;
	public $publicInfo;
	public $domainList;
	protected function _initialize() {
		parent::_initialize();	
		
	  if (isset($_GET['certificate'])){
			session('certificate',$_GET['certificate'],3600*24);
		}
		
	  \Log::write("UserAction::_initialize##URL=".$_SERVER['REQUEST_METHOD'] .$_SERVER['REQUEST_URI'] ,'WARN');
    if ($_GET['from'] != "Wap" && $_GET['type'] != 'image' && $_GET['type'] != 'video'){
     
      $this->canUseFunction(MODULE_NAME ,ACTION_NAME );
      $personInfo = $this->getRpcData('person/get', array('id'=>session('uid')));	
   
    	if (session('uid') == false || !$personInfo['data'] || $personInfo['data']['p_status'] == '禁用') {
    		$companyCode = session('companyCode')?session('companyCode'):$_GET['company'];
   			\Log::write("logout get userinfo failure client ip=".$this->getclientip(0)." company=".$companyCode,'WARN');
    		if (IS_GET && !isset($_SERVER["HTTP_X_REQUESTED_WITH"])){
					$this->redirect('Home/Index/index',array('company'=>$companyCode));
				}
				else{
					echo '###'.C('site_url').'/index.php?g=Home&m=Index&a=index&company='.$companyCode;
				}
				exit;
			}
		}
		
		$token = session('token');
		$tokenInfo = $this->getRpcData('publicNumber/getToken');	
				
		$token = $tokenInfo['data'];
		session('token',$token );
		$this->token = $token;
		
	  $data['token']= $this->token;
    
		$result = $this->getRpcData('publicNumber/getByToken',$data);	
		if ($result['data']['token'] != $this->token){
			//$this->error('非法操作',U('Home/Index/index'));
		}
		$this->getMenus();
		
		$this->publicInfo = $result['data'];
		
		
		$this->publicNumberId = $result['data']['id'];

		
		//获取用户信息
		$data['id'] = $_SESSION['uid'];
		$userinfo = $this->getRpcData('person/get',$data);
		$this->userGroup = $userinfo;  
		
		//根据用户获取用户权限

		session('usertplid', 1);
		
		$this->usertplid = 1;
		
	
		$this->assign('usertplid', session('usertplid'));
		$this->token = session('token');
		$companyCode = session('companyCode')?session('companyCode'):$_GET['company'];
		$mainBanner=RES."/images/logos/banner_".$companyCode.'.png';
		if (file_exists($mainBanner))
		$this->assign('mainBanner',$mainBanner);
		$this->assign('publicInfo',$this->publicInfo);
		
		$is_open_domain=session('is_open_domain');
		$this->assign("is_open_domain",$is_open_domain);
		
		if($is_open_domain){
		    $domainRes=$this->getRpcData('domain/getDomainByPersonId',array('personId'=>session('uid')));
		    $domainArr=$domainRes['data'];
		    $this->domainList=$domainArr;
		    $this->assign('domainSource',$domainArr);
		    $this->initDomain();
		    
		    //获取模块首页，切换域时跳转到模块首页
		    $actionName=ACTION_NAME;
		    $moduleName=MODULE_NAME;
		    $pageName=$moduleName.'/'.$actionName;
		    if($actionName=='welcome'){
		        $moduleIndex=U(MODULE_NAME."/welcome");
		    }else{
		        $moduleIndex=U(MODULE_NAME."/index");
		    }
		    if($pageName=='Content/tmpl'){
		        $moduleIndex=U("Content/tmpl");
		    }else if($pageName=='Integral/detail'){
		    	 $moduleIndex=U("Integral/detail");
		    }
		    $this->assign("moduleIndex",$moduleIndex);
		}
		$this->getCurrUserRight();
	}
	
	//获取当前模块id，用于判断当前模块是否拥有新建等权限
	public function getCurrModuleId(){
	    $uid=session('uid');
	    $moduleData = $this->getRpcData('person/getModule',array('id'=>(int)$uid));
	    $menus=$this->getMenuArray();
	    
	    //获取当前模块名称
	    $name='';
	    foreach ($menus as $menu) {
	        $subMenus=$menu['subs'];
	        foreach ($subMenus as $subMenu) {
	            if($subMenu['selectedCondition']['m']==MODULE_NAME){
	                $name=$subMenu['name'];
	                break;
	            }
	        }
	    }
	    //根据模块名称获取模块id
	    $id=0;
	    //推荐词管理属于内容管理模块
	    if(MODULE_NAME=='RecommendWord'){
	        $name='内容管理';
	    }
	    foreach ($moduleData['data'] as $module) {
	        $children=$module['children'];
	        foreach ($children as $child) {
	            if($child['name']==$name){
	                $id=$child['id'];
	                return $id;
	            }
	        }
	    }
	    return false;
	}
	
	//获取当前登录用户的权限,主要用于判断是否有新建的权限
	public function getCurrUserRight(){
	    $moduleId=$this->getCurrModuleId();
	    if($moduleId){
	        if(session('is_open_domain')){
	            $param['domainId']=(int)session("currDomainId");
	        }
	        $param['moduleId']=(int)$this->getCurrModuleId();
	        $param['personId']=(int)session('uid');
	        $right=$this->getRpcData("person/getRightsByPerson",$param);
	        if($right['errorCode']==0){
	            $rightArr=$right['data'];
	            $this->assign('userRight',$rightArr);
	        }
	    }
	}
	
	//获取域来源搜索列表
	public function getDomainSearchList(){
	    $conf['id'] = session('uid');
	    $isAdmin = $this->getRpcData('person/getRoles',$conf);
	    if($isAdmin['data']['is_admin']){
	        $domainSource=$this->domainList;
	    }else{
	        $domainRes=$this->getRpcData('domain/getDomain',array('personId'=>session('uid')));
	        $domainArr=$domainRes['data'];
	        $domainSource=$domainArr;
	    }
	    
	    $currDomainType=session("currDomainType");
	    $currDomainId=session("currDomainId");
	    $currDomainName=session("currDomainName");
	    $commonDomain=array();
	    
	    foreach ($domainSource as $domain) {
	        if($domain['type']=='公共域'){
	            $commonDomain['name']=$domain['name'];
	            $commonDomain['id']=$domain['id'];
	            break;
	        }
	    }
	    if($currDomainType=='部门域'){
	        $domainArr['type']='';
            $domainArr['name']='全部';
            $domainArr['id']='';
            $domainSearchList[]=$domainArr;
             
            $domainArr['type']='部门域';
            $domainArr['name']=$currDomainName;
            $domainArr['id']=$currDomainId;
            $domainSearchList[]=$domainArr;
             
            $domainArr['type']='公共域';
            $domainArr['name']=$commonDomain['name'];
            $domainArr['id']=$commonDomain['id'];
            $domainSearchList[]=$domainArr;
	    }else if($currDomainType=='公共域'){
	        $domainArr['type']='公共域';
	        $domainArr['name']=$commonDomain['name'];
	        $domainArr['id']=$commonDomain['id'];
	        $domainSearchList[]=$domainArr;
	    }else{
	        $domainSearchList=$domainSource;
	    }
	    return $domainSearchList;
	}
	
	//初始化域列表，session中没有域的信息则进行赋值
	public function initDomain(){
	    $currDomainId=session("currDomainId");
	    if($currDomainId===null){
	        $domainSource=$this->domainList;
	        session("currDomainId",$domainSource[0]['id']);
	        session("currDomainType",$domainSource[0]['type']);
	        session("currDomainName",$domainSource[0]['name']);
	        $domainSearchList=$this->getDomainSearchList();
	        session("domainList",$domainSearchList);
	    }
	}
	
	private $g_RightList= array(
//"Index"=> array("module_name"=>"公众号配置","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Log"=> array("module_name"=>"系统日志","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"NotificationSettings"=> array("module_name"=>"通知设置","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Account"=> array("module_name"=>"配置管理员","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Role"=> array("module_name"=>"角色管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"College"=> array("module_name"=>"学院管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Catemenu"=> array("module_name"=>"底部导航菜单","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Management"=> array("module_name"=>"班级管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"ManagementMember"=> array("module_name"=>"班级成员管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Classify"=> array("module_name"=>"考勤管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Content"=> array("module_name"=>"内容管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"User"=> array("module_name"=>"模板管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"User"=> array("module_name"=>"系列课程管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Essay"=> array("module_name"=>"使用情况","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Material"=> array("module_name"=>"素材管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Examination"=> array("module_name"=>"考试管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Question"=> array("module_name"=>"题库管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Classify"=> array("module_name"=>"考试分析","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Active"=> array("module_name"=>"活动报名","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Sign"=> array("module_name"=>"培训签到","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Research"=> array("module_name"=>"投票调研","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Evaluation"=> array("module_name"=>"培训评价（供应商版）","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Homework"=> array("module_name"=>"作业管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"EvaluationReport"=> array("module_name"=>"巡店报告","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Week"=> array("module_name"=>"周周学","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Caitu"=> array("module_name"=>"猜图","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Scratch"=> array("module_name"=>"刮刮卡","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Coupon"=> array("module_name"=>"优惠券","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"LuckyFruit"=> array("module_name"=>"幸运水果机","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"GoldenEgg"=> array("module_name"=>"砸金蛋","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Greeting_card"=> array("module_name"=>"祝福卡片","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Wall"=> array("module_name"=>"微信墙","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Shake"=> array("module_name"=>"摇一摇","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Lottery"=> array("module_name"=>"大转盘","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Store"=> array("module_name"=>"积分商城","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Adma"=> array("module_name"=>"DIY宣传页","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Photo"=> array("module_name"=>"相册管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Forum"=> array("module_name"=>"基本信息设置","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Forum"=> array("module_name"=>"微社区栏目设置","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Forum"=> array("module_name"=>"帖子管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Forum"=> array("module_name"=>"评论管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Forum"=> array("module_name"=>"消息管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Lector"=> array("module_name"=>"讲师管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"LectorReport"=> array("module_name"=>"讲师报表","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Integral"=> array("module_name"=>"积分配置","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Subordinate"=> array("module_name"=>"下属管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Notice"=> array("module_name"=>"通知管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Identity"=> array("module_name"=>"身份验证","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"UserManage"=> array("module_name"=>"学员管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"LearningSituation"=> array("module_name"=>"学习记录","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"ExamAnalysis"=> array("module_name"=>"考试记录","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,


"ContentAnalysis"=> array("module_name"=>"资源使用","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Analysis"=> array("module_name"=>"登录情况","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Analysis_search"=> array("module_name"=>"搜索行为","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Sheet"=> array("module_name"=>"自定义报表","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Areply"=> array("module_name"=>"智能应答","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Text"=> array("module_name"=>"关注时应答","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Diymen"=> array("module_name"=>"微信菜单","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Other"=> array("module_name"=>"回答不上来的配置","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Wechat_group"=> array("module_name"=>"粉丝管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Wechat_group"=> array("module_name"=>"分组管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Wechat_behavior"=> array("module_name"=>"粉丝行为分析","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Recognition"=> array("module_name"=>"渠道二维码","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"ServiceUser"=> array("module_name"=>"在线客服","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Message"=> array("module_name"=>"群发消息","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Domain"=> array("module_name"=>"域管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"Alert"=> array("module_name"=>"提示语管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
"APP"=> array("module_name"=>"APP管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Share"=> array("module_name"=>"分享管理","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"Requerydata"=> array("module_name"=>"请求数详情","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,
//"ManagementGroup"=> array("module_name"=>"管理组","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,	
"GoldManagement"=> array("module_name"=>"金币记录","index"=>"查看","add"=>"新增","edit"=>"修改","delete"=>"删除") ,	
);

    private $gModuleOpt = array(
		'view'=>array(
			'item'=>'view',
			 'name'=>'查看',				
			'isRight'=>0
		),
		'add'=>array(
		  'item'=>'add',	
		  'name'=>'新增',		
			'isRight'=>0
		),
		'edit'=>array(
		  'item'=>'edit',						
			'name'=>'修改',
			'isRight'=>0
		),
		'delete'=>array(
			'item'=>'delete',
			'name'=>'删除',
			'isRight'=>0
		),
		'category'=>array(
			'item'=>'category',
			'name'=>'新增分类',
			'isRight'=>0
		),
		'status'=>array(
			'item'=>'status',
			'name'=>'设置状态',
			'isRight'=>0
		)
    );
    
    private $gModules =array(
    
    "学院管理"=> "College" ,
    "班级管理"=> "Management" ,
    "讲师管理"=> "Lector" ,
    "内容管理"=> "Content" ,
    "模版管理"=> "Content" ,
    "系列课程管理"=> "Series" ,
    "素材管理"=> "Material" ,
    "使用情况"=> "Essay" ,
    
    "考试管理"=> "Examination" ,
    "题库管理"=> "Question" ,
    "错题集"=> "ExamErrGroup" ,
    "投票调研"=> "Research" ,
    "培训评价（供应商版）"=> "Evaluation" ,
    "培训签到"=> "Sign" ,
    "活动报名"=> "Active" ,
    "巡店报告"=> "EvaluationReport" ,
    "周周学"=> "Week" ,
    "作业管理"=> "Homework" ,
    //"DIY宣传页"=> "Adma" ,
    //"相册管理"=> "Photo" ,
    "游戏管理"=> "GameManagement" ,
    "积分商城"=> "Store" ,
    //"社区管理"=> "Forum" ,
    //"群发消息"=> "Message" ,
    "在线客服"=> "ServiceUser" ,
    "关键词应答"=> "Text" ,
    "成员管理"=> "UserManage" ,
    "角色管理"=> "Role" ,
    "配置管理员"=> "Account" ,
    "身份验证"=> "Identity" ,
    "学习记录"=> "LearningSituation" ,
    "考试记录"=> "ExamAnalysis" ,
    "积分记录"=> "Integral" ,
    "金币记录"=> "GoldManagement" ,
    "资源使用"=> "ContentAnalysis" ,
    "登录情况"=> "Analysis" ,
    "搜索行为"=> "Analysis_search" ,
    "追求数详情"=> "Requerydata" ,
    "自定义报表"=> "Sheet" ,
    "讲师报表"=> "LectorReport" ,
    //"通知设置"=> "NotificationSettings" ,
    //"域管理"=> "Domain" ,
    //"管理组"=> "ManagementGroup" ,
    "APP管理"=> "APP" ,
    //"系统日志"=> "Log" ,
    //"公众号配置"=> "Index" ,
    //"积分配置"=> "Integral" ,
    //"智能应答"=> "Text" ,
    "提示语管理"=> "Alert" ,
    //"分享管理"=> "Share" ,
    
    "套餐管理"=> "Package" ,
    "考勤管理"=> "CheckWorkAttendance" ,
    "考试分析"=> "Classify" ,
        
    "猜图游戏"=> "Caitu" ,
    "刮刮卡"=> "Scratch" ,
    "优惠券"=> "Coupon" ,
    "幸运水果机"=> "LuckyFruit" ,
    "砸金蛋"=> "GoldenEgg" ,
    "祝福卡片"=> "Greeting_card" ,
    "微信墙"=> "Wall" ,
    "摇一摇"=> "Shake" ,
    "大转盘"=> "Lottery" ,
    //"微信商城系统"=> "Store" ,
    //"基本信息设置"=> "Forum" ,
    //"微社区栏目设置"=> "Forum" ,
    //"帖子管理"=> "Forum" ,
    //"评论管理"=> "Forum" ,
    "消息管理"=> "Forum" ,
    "下属管理"=> "Subordinate" ,
    "通知管理"=> "Notice" ,
    "学员管理"=> "UserManage" ,
    //"关注时应答"=> "Areply" ,
    //"微信菜单"=> "Diymen" ,
    //"回答不上来的配置"=> "Other" ,
    "粉丝管理"=> "Wechat_group" ,
    "分组管理"=> "Wechat_group" ,
    "粉丝行为分析"=> "Wechat_behavior" ,
    "渠道二维码"=> "Recognition" ,
    "人工客服"=> "ServiceUser" ,
    //"群发消息"=> "Message" ,
    "请求数详情"=> "Requerydata"
    );

	public function canUseFunction($module,$func) {
		if ($module == "Message"){
			$module = "Notice";
		}
		if ($module == "College" && $func == "add" && $_GET['act'] == 'update'){
			$func = "edit";
		}
		
		foreach($this->gModules as $k=>$v){
			if ($v == $module){
				$moduleName = $k;
				break;
			}
		}
		
		$moduleIndex = $module;
		$moduleOpt = $func;
        //权限验证
        $uid = session('uid');
        if (!$uid){
    			$companyCode = session('companyCode')?session('companyCode'):$_GET['company'];
    			$companyCode = $companyCode?$companyCode:cookie('companyCode');
    			\Log::write("logout timeout client ip=".$this->getclientip(0)." company=".$companyCode,'WARN');
    			if (IS_GET && !isset($_SERVER["HTTP_X_REQUESTED_WITH"])){
    				$this->redirect('Home/Index/index',array('company'=>$companyCode));
    			}
    			else{
    				echo '###'.C('site_url').'/index.php?g=Home&m=Index&a=index&company='.$companyCode;
    			}
    			exit;
        }
       
        //管理员
        $conf['id'] = $uid;
		$isAdmin = $this->getRpcData('person/getRoles',$conf);	
			
		if ($isAdmin['data']['is_admin']){	
			session('firstOpen',null);		
			$this->assign('IS_ADMIN', 1);
			return;
		}	

		$flag = 0;
		$rightlist = $this->getRpcData('person/getRights',$conf);
		foreach($rightlist['data'] as $v){		
			
			if ($v['module_name'] == $moduleName){
				$flag = 1;
				foreach($this->gModuleOpt as &$item){
					if ($item['name'] == $v['right_name']){
					    if($item['name']=='新增'){
					        //一般管理员在当前域为公共域的情况下没有新建权限
					        if($isAdmin['data']['is_admin']==0&&session("currDomainType")=='公共域')
					            $item['isRight'] = 0;
					        else
					            $item['isRight'] =1;
					    }else{
					        $item['isRight'] =1;
					    }
					}		
				}						
			}
		}
		$this->assign('rightlist', $this->gModuleOpt);
		
		
		//首页没有权限跳转到第一个有权限的模块
		$firstOpen = session('firstOpen');
		
		if ($firstOpen){	
			session('firstOpen',null);
			$this->redirect('User/Index/welcome');
		}
	
	
		//不在权限数据组中或者isRight=1表示有权限
		if (!$this->gModuleOpt[$moduleOpt] || $this->gModuleOpt[$moduleOpt]['isRight']){
			return;
		}
		
		if (!$flag){
		    if($module=='Caitu' || $module=='Scratch' || $module=='Wall' || $module=='Lottery' ||$module=='Coupon' || $module=='LuckyFruit' || $module=='GoldenEgg' || $module=='Greeting_card' || $module=='Shake'){
		        //游戏是一个GameManagement模块接口未返回单个游戏的模块名称
		    }else{
                $this->assign('IS_ADMIN', 1);
		    }
			return;
		}
		
		if (IS_GET){
		  $this->error("你没有权限访问，请联系管理员！");
		}
		
	}
	
	/**
	 * 获取所有菜单的数组
	 * @return array 返回菜单数组
	 */
	public function getMenuArray(){
	    $token = $this->token;
	    $menus=array(
	        array(
	            'name'=>'培训管理',
	            'iconName'=>'site',
	            'display'=>0,
	            'subs'=>array(
	                array('name'=>'Portal管理','link'=>U('Portal/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Portal')),
	                array('name'=>'学院管理','link'=>U('College/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'College')),
	                array('name'=>'班级管理','link'=>U('Management/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Management')),
	                array('name'=>'讲师管理','link'=>U('Lector/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Lector')),
	                //array('name'=>'学习路径设计','link'=>U('StudyMapDesign/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'StudyMapDesign')),
	            )),
	        array(
	            'name'=>'资源管理',
	            'iconName'=>'site',
	            'display'=>0,
	            'subs'=>array(
	                array('name'=>'内容管理','link'=>U('Content/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Content')),
	                array('name'=>'系列课程管理','link'=>U('Series/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Series')),
	                array('name'=>'模板管理','link'=>U('Content/tmpl',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Content','a'=>'tmpl')),
	                array('name'=>'素材管理','link'=>U('Material/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Material')),
	                array('name'=>'使用情况','link'=>U('Essay/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Essay')),
	            )),
	        array(
	            'name'=>'考试管理',
	            'iconName'=>'site',
	            'display'=>0,
	            'subs'=>array(
	                array('name'=>'考试管理','link'=>U('Examination/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Examination')),
	                array('name'=>'题库管理','link'=>U('Question/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Question')),
	                array('name'=>'错题集','link'=>U('ExamErrGroup/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'ExamErrGroup')),
	            )),
	    
	        array(
	            'name'=>'活动管理',
	            'iconName'=>'site',
	            'display'=>0,
	            'subs'=>array(
	                array('name'=>'投票调研','link'=>U('Research/index',array('token'=>$token)),'new'=>1,'test'=>0,'selectedCondition'=>array('m'=>'Research')),
	                array('name'=>'培训评价（供应商版）','link'=>U('Evaluation/index',array('token'=>$token)),'new'=>1,'test'=>0,'selectedCondition'=>array('m'=>'Evaluation')),
	                array('name'=>'培训签到','link'=>U('Sign/index',array('token'=>$token)),'new'=>1,'test'=>0,'selectedCondition'=>array('m'=>'Sign')),
	                array('name'=>'活动报名','link'=>U('Active/index',array('token'=>$token)),'new'=>1,'test'=>0,'selectedCondition'=>array('m'=>'Active')),
	                array('name'=>'巡店报告','link'=>U('EvaluationReport/index',array('token'=>$token)),'new'=>1,'test'=>0,'selectedCondition'=>array('m'=>'EvaluationReport')),
	                array('name'=>'周周学','link'=>U('Week/index',array('token'=>$token)),'new'=>1,'test'=>0,'selectedCondition'=>array('m'=>'Week')),
	                array('name'=>'作业管理','link'=>U('Homework/index',array('token'=>$token)),'new'=>1,'test'=>0,'selectedCondition'=>array('m'=>'Homework')),
	                array('name'=>'DIY宣传页','link'=>U('Adma/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Adma')),
	                array('name'=>'相册管理','link'=>U('Photo/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Photo')),
	                array('name'=>'游戏管理','link'=>U('GameManagement/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'GameManagement')),
	                array('name'=>'积分商城','link'=>U('Store/index',array('token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'Store')),
	            )),
	    
	        array(
	            'name'=>'UGC管理',
	            'iconName'=>'interact',
	            'display'=>0,
	            'subs'=>array(
	                array('name'=>'社区管理','link'=>U('Forum/index',array('tab'=>'config','token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'Forum')),
	            )),
	    
	        array(
	            'name'=>'消息管理',
	            'iconName'=>'message',
	            'display'=>0,
	            'subs'=>array(
	                array('name'=>'微信推送','link'=>U('Message/index',array('token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'Message')),
	                array('name'=>'通知管理','link'=>U('Notice/index',array('token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'Notice')),
	                array('name'=>'公告管理','link'=>U('Affiche/index',array('token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'Affiche')),
	                array('name'=>'在线客服','link'=>U('ServiceUser/index',array('token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'ServiceUser')),
	                array('name'=>'关键词应答','link'=>U('Text/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Text')),
	            )),
	    
	        array(
	            'name'=>'用户管理',
	            'iconName'=>'site',
	            'display'=>0,
	            'subs'=>array(
	                array('name'=>'成员管理','link'=>U('UserManage/index',array('token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'UserManage')),
	                array('name'=>'角色管理','link'=>U('Role/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Role')),
	                array('name'=>'配置管理员','link'=>U('Account/index',array('token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'Account')),
	                array('name'=>'身份验证','link'=>U('Identity/index',array('token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'Identity')),
	            )),
	    
	        array(
	            'name'=>'报表管理',
	            'iconName'=>'site',
	            'display'=>0,
	            'subs'=>array(
	                array('name'=>'学习记录','link'=>U('LearningSituation/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'LearningSituation')),
	                array('name'=>'考试记录','link'=>U('ExamAnalysis/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'ExamAnalysis')),
	                array('name'=>'积分记录','link'=>U('Integral/detail',array('token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'detail','a'=>'detail')),
	                array('name'=>'金币记录','link'=>U('GoldManagement/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'GoldManagement')),
	                array('name'=>'资源使用','link'=>U('ContentAnalysis/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'ContentAnalysis')),
	                array('name'=>'登录情况','link'=>U('Analysis/index',array('token'=>$token,'tip'=>1)),'new'=>0,'selectedCondition'=>array('m'=>'Analysis')),
	                array('name'=>'搜索行为','link'=>U('Analysis_search/index',array('token'=>$token,'tip'=>2)),'new'=>1,'selectedCondition'=>array('m'=>'Analysis_search')),
	                array('name'=>'请求数详情','link'=>U('Requerydata/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Requerydata')),
	                array('name'=>'讲师报表','link'=>U('LectorReport/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'LectorReport')),
	                array('name'=>'自定义报表','link'=>U('Sheet/index',array('token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'Sheet')),
	            )),
	    
	        array(
	            'name'=>'系统管理',
	            'iconName'=>'site',
	            'display'=>0,
	            'subs'=>array(
	                array('name'=>'通知设置','link'=>U('NotificationSettings/index',array('token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'NotificationSettings')),
	                array('name'=>'域管理','link'=>U('Subdomain/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Subdomain')),
	                array('name'=>'管理组','link'=>U('ManagementGroup/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'ManagementGroup')),
	                array('name'=>'APP管理','link'=>U('APP/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'APP')),
	                array('name'=>'系统日志','link'=>U('Log/index',array('token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'Log')),
	                array('name'=>'公众号配置','link'=>U('Index/index',array('token'=>$token,'id'=>session('wxid'))),'new'=>0,'selectedCondition'=>array('m'=>'Index')),
	                array('name'=>'积分配置','link'=>U('Integral/index',array('token'=>$token)),'new'=>1,'selectedCondition'=>array('m'=>'Integral','a'=>'index')),
	                array('name'=>'智能应答','link'=>U('Areply/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Areply')),
	                array('name'=>'提示语管理','link'=>U('Alert/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Alert')),
	                array('name'=>'分享管理','link'=>U('Share/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Share')),
	                array('name'=>'个人中心配置','link'=>U('Personcenter/index',array('token'=>$token)),'new'=>0,'selectedCondition'=>array('m'=>'Personcenter'))
	            )),
	    );
	    
	    return $menus;
	}
	
	public function getMenus(){
        $menus=$this->getMenuArray();
        $uid = session('uid');
        $moduleData = $this->getRpcData('person/getModule',array('id'=>(int)$uid));
        
        $i=0;
        $parms=$_SERVER['QUERY_STRING'];
        $parms1=explode('&',$parms);
        $parmsArr=array();
        if ($parms1){
        	foreach ($parms1 as $p){
        		$parms2=explode('=',$p);
        		$parmsArr[$parms2[0]]=$parms2[1];
        	}
        }
        
        $subMenus=array();
        $t=0;
        $currentMenuID=0;
        $currentParentMenuID=0;
        foreach ($menus as $m){
        	$loopContinue=1;
        	if ($m['subs']){
        		$st=0;
        		foreach ($m['subs'] as $s){
        			$includeArr=1;
        			if ($s['selectedCondition']){
        				
        				foreach ($s['selectedCondition'] as $condition){
        					if (!in_array($condition,$parmsArr)){
        						$includeArr=0;
        						break;
        					}
        				}
        			 }
        			if ($includeArr){
        				$currentMenuID=$st;
        				$currentParentMenuID=$t;
        				$loopContinue=0;
        				break;
        			}
        			$st++;
        		}
        		if ($loopContinue==0){
        			break;
        		}
        	}
        	$t++;
        }
        //var_dump($subMenus);var_dump($menus);exit;
        //学院管理
        if ($parmsArr['m'] == "Slides" || $parmsArr['m'] == "Catemenu" || $parmsArr['m'] == "Column"){
        	$currentParentMenuID = 0;
            $currentMenuID = 1;
        }
        //班级
        if ($parmsArr['m'] == "CheckWorkAttendance" || $parmsArr['m'] == "ManagementMember"){
            $currentParentMenuID = 0;
            $currentMenuID = 2;
        }
        
        if ($parmsArr['m'] == "Flash"){
        	$currentParentMenuID = 4;
            $currentMenuID = 0;
        }
        
        if ($parmsArr['m'] == "Content" && ($parmsArr['a'] == "tmpl" || $parmsArr['a'] == "tmpl_add"|| $parmsArr['a'] == "tmplEdit")){
            $currentMenuID = 2;
        }
        
        if ($parmsArr['m'] == "Wechat_group" &&  ($parmsArr['a'] == 'groupSet')||($parmsArr['a']=='groups')){
            $currentParentMenuID = 14;
            $currentMenuID = 0;
        }
        //游戏管理
        if ($parmsArr['m'] == "Caitu" || $parmsArr['m'] == "Scratch" || $parmsArr['m'] == "Coupon" || $parmsArr['m'] == "LuckyFruit" || $parmsArr['m'] == "GoldenEgg" || $parmsArr['m'] == "Greeting_card" || $parmsArr['m'] == "Wall" || $parmsArr['m'] == "Shake" || $parmsArr['m'] =="Lottery"){
            $currentParentMenuID = 3;
            $currentMenuID = 9;
        }
        if ($parmsArr['m'] == "Forum" &&  ($parmsArr['a'] == 'columnAdd')){
            $currentParentMenuID = 4;
            $currentMenuID = 0;
        }
        //用户管理
        if ($parmsArr['m'] == 'Subordinate'){
            $currentParentMenuID = 6;
            $currentMenuID = 0;
        }
        //消息管理
        /* if ($parmsArr['m'] == "Notice"){
            $currentParentMenuID = 5;
            $currentMenuID = 0;
        } */
        //粉丝行为管理
        if ($parmsArr['m'] == "Wechat_behavior"){
            $currentParentMenuID = 6;
            $currentMenuID = 0;
        }
        //讲师报表管理
        if ($parmsArr['m'] == "Lector" && ($parmsArr['a'] == 'view') && ($parmsArr['p'] == 'LectorReport')){
            $currentParentMenuID = 7;
            $currentMenuID = 8;
        }
        //金币记录
        if ($parmsArr['m'] == "GoldManagement" &&  ($parmsArr['a'] == 'detail')){
            $currentParentMenuID = 7;
            $currentMenuID = 3;
        }
        if ($parmsArr['m'] == "RecommendWord"){
            $currentParentMenuID = 1;
            $currentMenuID = 0;
        }
        //模板管理 
        if ($parmsArr['m'] == "Content" &&  ($parmsArr['a'] == 'tmpl')){
            $currentParentMenuID = 1;
            $currentMenuID = 2;
        }
        /*
        echo $currentMenuID;
        echo $currentParentMenuID;
        */
        //dump($menus);
        //
        foreach ($menus as $m){
        	$isShow = $this->checkMenuItem($m['name'],$moduleData);
        	if (!$isShow){
        		$i++;
        		continue;
        	}
        	
        	$displayStr='';
        	if ($currentParentMenuID!=0||0!=$currentMenuID){
        		$m['display']=0;
        	}
        	if (!$m['display']){
        		$displayStr=' style="display:none"';
        	}
        	if ($currentParentMenuID==$i){
        		$displayStr='';
        	}
        	$aClassStr='';
        	if ($displayStr){
        		$aClassStr=' nav-header-current';
        	}
        	if($i == 0){
        		 $menulist.= '<a class="nav-header'.$aClassStr.'" style="border-top:none !important;"><b class="base"></b>'.$m['name'].'</a><ul class="ckit"'.$displayStr.'>';
        	}else{
        		 $menulist.= '<a class="nav-header'.$aClassStr.'"><b class="base"></b>'.$m['name'].'</a><ul class="ckit"'.$displayStr.'>';
        	}
        	if ($m['subs']){
        		$j=0;
        		foreach ($m['subs'] as $s){
        			$isShow = $this->checkMenuItem($s['name'],$moduleData,1);
        			if ($isShow){
        			
        			$selectedClassStr='subCatalogList';
        			if ($currentParentMenuID==$i&&$j==$currentMenuID){
        				$selectedClassStr='selected';
        			}
        	        $menulist.= '<li class="'.$selectedClassStr.'" id="leftMenu'.$i.'_'.$j.'"> <a href="'.$s['link'].'">'.$s['name'].'</a></li>';
        			}
        			$j++;
        		}
        	}
        	 $menulist.= '</ul>';
        	$i++;
        }
		$this->assign("menulist", 	$menulist);
	}
	
	public function checkMenuItem($currItem,$menuList,$sub=0){
		
		foreach($menuList['data'] as $v){
			if ($sub){
					foreach($v['children'] as $sv){
						if ($currItem == $sv['name']){
							return true;
						}
					}				
			}
			else{
				if ($currItem == $v['name']){
					return true;
				}
			}
		}
		
		return false;
	}
	
	public function getProvinces(){
	    $res=$this->getRpcData('district/getProvinces','');
	    echo json_encode($res['data']);
	}
	public function getCityOrArea(){
	    $pid=$this->_post('pid');
	    $res=$this->getRpcData('district/getChildrens',array('pid'=>$pid));
	    echo json_encode($res['data']);
	}
	
	public function getRpcData($path,$param="",$companyCode=""){	
		$certificate = session('certificate');
		if (!$certificate){
			\Log::write("logout certificate expired client ip=".$this->getclientip(0)." company=".$companyCode,'WARN');
			if (IS_GET && !isset($_SERVER["HTTP_X_REQUESTED_WITH"])){			
				$this->redirect('Home/Index/index',array('company'=>$companyCode));
			}
			else{
				echo '###'.C('site_url').'/index.php?g=Home&m=Index&a=index&company='.$companyCode;
			}			
			exit;		
		}			
		
		if (!$companyCode){
			$companyCode = session('companyCode');
		}
		$result = $this->getRpcDataBase($path,$param,$companyCode, $certificate);			
		if ($result == -10001){		
			\Log::write("logout result=-10001 client ip=".$this->getclientip(0)." company=".$companyCode,'WARN');		
			if (IS_GET && !isset($_SERVER["HTTP_X_REQUESTED_WITH"])){				
				$this->redirect('Home/Index/index',array('company'=>$companyCode));
			}
			else{
				echo '###'.C('site_url').'/index.php?g=Home&m=Index&a=index&company='.$companyCode;
			}	
			exit;		
		}			
		return $result;
	}	
}

?>