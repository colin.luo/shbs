<?php 
class FaceClassAction extends WapAction{
	//课程列表
    public function index(){
		//评论自定标签
			$comtag = $this->getRpcData('msCommentTag/getCommentTag');
			if($comtag['errorCode'] == 0){
					$this->assign('comtag',$comtag['data']);
				}else{
						$msg = $comtag['errorMassege'];
						$this->assign('error',$msg);
					}
			$this->display();
		}
	public function kecheng(){
			$param['memberId'] = $this->fans['id'];
			$param['pageNo']= 1;
			if($_GET['pageNo']!=null){
				$param['pageNo']= $_GET['pageNo'];
			}
			$param['pageSize']= 10;
			$param['portfolio'] = $_GET['competencefields'];
			$param['title'] = $_GET['keyword']; //搜索内容
			$cclist = $this->getRpcData('msCourse/getCourseList',$param);
			echo json_encode($cclist);
		}
	//搜索
	public function search(){
			$this->display();
		}
	//插入评论
	public function insertcomment(){
			$backdata['content'] = $_POST['comment'];
			$backdata['member_id'] = $this->fans['id'];
			$backdata['reference_id'] = $_GET['reference_id'];
			$backdata['comment_score'] = (double)$_POST['star'];
			$backdata['reference_type'] = 'course';
			$field['fields'] = json_encode($backdata);
			$return = $this->getRpcData('msCommentInfo/insert',$field);
			echo json_encode($return);
		}
	//课程详情
	public function detail(){
			$param['itemId']=$this->_get('itemId');
			$xqitem = $this->getRpcData('msCourse/getCourseByItemId',$param);
			if($xqitem['errorCode'] == 0){
					$this->assign('xqitem',$xqitem['data'][0]);
				}else{
						$msg = $xqitem['errorMassege'];
						$this->assign('error',$msg);
					}
			//评论自定标签
			$comtag = $this->getRpcData('msCommentTag/getCommentTag');
			if($comtag['errorCode'] == 0){
					$this->assign('comtag',$comtag['data']);
				}else{
						$msg = $comtag['errorMassege'];
						$this->assign('error',$msg);
					}
			//课程场次 list
			$kcparam['pageNo'] = 1;
			$kcparam['pageSize'] = 5;
			$kcparam['memberId'] = $this->fans['id'];
			$kcparam['itemId'] = $xqitem['data'][0]['item_id'];
			$kccc = $this->getRpcData('msScheduledOffering/getScheduledList',$kcparam);
			if($kccc['errorCode'] == 0){
					$this->assign('kccc',$kccc['data']);
				}else{
						$msg = $kccc['errorMassege'];
						$this->assign('error',$msg);
					}
			$this->display();
		}
	//评论
	public function comments(){
			//课程评论
			$param['pageNo']= 1;
			if($_GET['pageNo']!=null){
					$param['pageNo']= $_GET['pageNo'];
				}
			$param['pageSize']=10;
			$param['referenceId']=$this->_get('itemId');
			$param['referenceType']="course";
			$param['memberId'] = $this->fans['id'];
			$param['sort']=$_GET['sort'];
			$kccomment = $this->getRpcData('msCommentInfo/getCommentInfo',$param);
			if($kccomment['errorCode'] == 0){
					//评论回复
					foreach($kccomment['data'] as $k=>$v){
						$replyparam['pageNo'] = 1;
						if($_GET['pageNo']!=null){
								$replyparam['pageNo']= $_GET['pageNo'];
							}
						$replyparam['pageSize'] = 2;
						$replyparam['commentId'] = $v['id'];
						$replyparam['memberId'] = $this->fans['id'];
						$replycomment = $this->getRpcData('msCommentReply/getCommentReply',$replyparam);
						if($replycomment['errorCode'] == 0){
								if(empty($replycomment['data'])){
										$replycomment['data'] = 0;
									}
								$kccomment['data'][$k]['reply'] = $replycomment['data'];
							}else{
									$msg = $replycomment['errorMassege'];
									$this->assign('error',$msg);
								}
						
					}
					$this->assign('kccomment',$kccomment['data']);
				}else{
						$msg = $kccomment['errorMassege'];
						$this->assign('error',$msg);
					}
			echo json_encode($kccomment);
		}
		//评论回复
		public function replies(){
			$replyparam['comment_id'] = $_GET['commentid'];
			$replyparam['member_id'] = $this->fans['id'];
			$replyparam['content'] = $_POST['comment'];
			$field['fields'] = json_encode($replyparam);
			$replycomment = $this->getRpcData('msCommentReply/insert',$field);
			echo json_encode($replycomment);
		}
		//评论回复列表
		public function replieslist(){
			$commentparam['commentId'] = $_GET['commentid'];
			$commentparam['memberId'] = $this->fans['id'];
			$commentone = $this->getRpcData('msCommentInfo/getCommentInfoById',$commentparam);
			if($commentone['errorCode'] == 0){
					$this->assign('commentone',$commentone['data'][0]);
				}else{
						$msg = $commentone['errorMassege'];
						$this->assign('error',$msg);
					}
			$this->display('replies');
		}
		public function replylist(){
			$replylistparam['pageNo'] = 1;
			if($_GET['pageNo']!=null){
					$replylistparam['pageNo']= $_GET['pageNo'];
				}
			$replylistparam['pageSize'] = 10;
			$replylistparam['commentId'] = $_GET['commentid'];
			$replylistparam['memberId'] = $this->fans['id'];
			$replylist = $this->getRpcData('msCommentReply/getCommentReply',$replylistparam);
			echo json_encode($replylist);
		}

		//点赞
		public function praise(){
			$data['member_id'] = $this->fans['id'];
			$data['comment_id'] = $_GET['commentid'];
			if($_GET['replyid']!=null){
				$data['reply_id'] = $_GET['replyid'];
			}
			$field['fields'] = json_encode($data);
			$praiselist = $this->getRpcData('msCommentFavor/insert',$field);
			echo json_encode($praiselist);
		}
		//取消赞
		public function nopraise(){
			$pdata['memberId'] = $this->fans['id'];
			$pdata['commentId'] = $_GET['commentid'];
			$pdata['replyId'] = $_GET['replyid'];
			$praiselist = $this->getRpcData('msCommentFavor/delete',$pdata);
			echo json_encode($praiselist);
		}
		//课程分类
		public function menu(){
			$menu = $this->getRpcData('msCourse/getCompetenceFields');
			$menuary = array();
			if($menu['errorCode'] == 0){
					foreach($menu['data'] as $k=>$v){
						$twommenu['competenceFields'] = $v;
						$tmreturn = $this->getRpcData('msCourse/getByCompetenceFields',$twommenu);
						if($tmreturn['errorCode'] == 0){
							$menuary[$k]['menu'] = $v;
							$menuary[$k]['tmenu'] = $tmreturn['data'];
						}else{
								$msg = $tmreturn['errorMassege'];
								$this->assign('error',$msg);
							}
					}
					$this->assign('menu',$menuary);
				}else{
						$msg = $menu['errorMassege'];
						$this->assign('error',$msg);
					}
			$this->display();
		}
		//课次  list
		public function sessionalindex(){
			$this->display();
		}
		public function sessionalindexlist(){
			$kcparam['pageNo'] = 1;
			if($_GET['pageNo']!=null){
					$kcparam['pageNo']= $_GET['pageNo'];
				}
			$kcparam['pageSize'] = 10;
			$kcparam['memberId'] = $this->fans['id'];
			$kcparam['itemId'] = $_GET['itemid'];
			$kclist = $this->getRpcData('msScheduledOffering/getScheduledList',$kcparam);
			echo json_encode($kclist);
				
		}
		//课次详情
		public function sessionaldetails(){
			$kcparam['scheduledOfferingId'] = $_GET['id'];
			$kcparam['memberId'] = $this->fans['id'];
			$kclist = $this->getRpcData('msScheduledOffering/getScheduledById',$kcparam);
			if($kclist['errorCode'] == 0){
					$this->assign('kcdetail',$kclist['data'][0]);
				}else{
						$msg = $kclist['errorMassege'];
						$this->assign('error',$msg);
					}
			$this->display();
				
		}
		//预报名
		public function register(){
			$regparam['title'] = $_GET['title'];
			$regparam['links'] = $_GET['links'];
			$regparam['email'] = $this->fans['email'];
			$regparam['memberId'] = $this->fans['id'];
			$regparam['scheduledOfferingId'] = $_GET['scheduledOfferingId'];
			$reglist = $this->getRpcData('msPreRegisterLog/toPreRegister',$regparam);
			echo json_encode($reglist);
		}
		//我的场次
		public function mysessional(){
			$this->display();
		}
		public function mysessionallist(){
			$myparam['pageNo'] = 1;
			if($_GET['pageNo']!=null){
					$myparam['pageNo']= $_GET['pageNo'];
				}
			$myparam['pageSize'] = 10;
			$myparam['memberId'] = $this->fans['id'];
			$mylist = $this->getRpcData('msRegistrationStatusReport/getRegistrationList',$myparam);	
			echo json_encode($mylist);
		}
}

?>