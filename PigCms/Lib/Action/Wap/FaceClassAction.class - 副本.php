<?php 
class FaceClassAction extends WapAction{
	//课程列表
    public function index(){
			$param['workNo'] = $this->fans['id'];
			$param['pageNo']=$_GET['itemId'];
			$param['pageSize']=10;
			$param['portfolio'] = $_GET['competencefields'];
			$clist = $this->getRpcData('msCourse/getCourseList',$param);
			if($clist['errorCode'] == 0){
					$this->assign('courelist',$clist['data']);
				}else{
						$msg = $clist['errorMassege'];
						$this->assign('error',$msg);
					}
		//评论自定标签
			$comtag = $this->getRpcData('msCommentTag/getCommentTag');
			if($comtag['errorCode'] == 0){
					$this->assign('comtag',$comtag['data']);
				}else{
						$msg = $comtag['errorMassege'];
						$this->assign('error',$msg);
					}
			$this->display();
		}
	public function test(){
			$param['workNo'] = $this->fans['id'];
			$param['pageNo']=$_GET['itemId'];
			$param['pageSize']=10;
			$param['portfolio'] = $_GET['competencefields'];
			$clist = $this->getRpcData('msCourse/getCourseList',$param);
			echo json_encode($clist);
		}
	//插入评论
	public function insertcomment(){
			$backdata['content'] = $_POST['content'];
			$backdata['global_id'] = $this->fans['id'];
			$backdata['reference_id'] = $_POST['reference_id'];
			$backdata['comment_score'] = (double)$_POST['comment_score'];
			$backdata['reference_type'] = 'course';
			$field['fields'] = json_encode($backdata);
			$return = $this->getRpcData('msCommentInfo/insert',$field);
			echo json_encode($return);
		}
	//课程详情
	public function detail(){
			$param['itemId']=$this->_get('itemId');
			$xqitem = $this->getRpcData('msCourse/getCourseByItemId',$param);
			if($xqitem['errorCode'] == 0){
					
				
					$this->assign('xqitem',$xqitem['data'][0]);
				}else{
						$msg = $xqitem['errorMassege'];
						$this->assign('error',$msg);
					}
			//课程场次 list
			$kcparam['pageNo'] = 1;
			$kcparam['pageSize'] = 10;
			$kcparam['globalId'] = $this->fans['id'];
			$kcparam['itemId'] = $xqitem['data'][0]['item_id'];
			$kccc = $this->getRpcData('msScheduledOffering/getScheduledList',$kcparam);
			if($xqitem['errorCode'] == 0){
					$this->assign('kccc',$kccc['data']);
				}else{
						$msg = $xqitem['errorMassege'];
						$this->assign('error',$msg);
					}
			$this->display();
		}
	//评论
	public function comments(){
			//课程评论
			$param['pageNo']=1;
			$param['pageSize']=10;
			$param['referenceId']=$this->_get('itemId');
			$param['referenceType']="course";
			$param['sort']=$_GET['sort'];
			$kccomment = $this->getRpcData('msCommentInfo/getCommentInfo',$param);
			if($xqitem['errorCode'] == 0){
					//评论回复
					foreach($kccomment['data'] as $k=>$v){
						$replyparam['pageNo'] = 1;
						$replyparam['pageSize'] = 2;
						$replyparam['commentId'] = $v['id'];
						$replycomment = $this->getRpcData('msCommentReply/getCommentReply',$replyparam);
						if($xqitem['errorCode'] == 0){
								if(empty($replycomment['data'])){
										$replycomment['data'] = 0;
									}
								$kccomment['data'][$k]['reply'] = $replycomment['data'];
							}else{
									$msg = $xqitem['errorMassege'];
									$this->assign('error',$msg);
								}
						
					}
					$this->assign('kccomment',$kccomment['data']);
				}else{
						$msg = $xqitem['errorMassege'];
						$this->assign('error',$msg);
					}
			
			$this->display();
		}
		//回复
		public function replies(){
			//评论回复 
			$replyparam['commentId'] = $_GET['commentid'];
			$replyparam['global_id'] = $this->fans['id'];
			$replyparam['content'] = $_GET['reply'];
			$replycomment = $this->getRpcData('msCommentReply/getCommentReply',$replayparam);
			if($xqitem['errorCode'] == 0){
					$this->assign('rycomment',$replycomment['data']);
				}else{
						$msg = $xqitem['errorMassege'];
						$this->assign('error',$msg);
					}
			
			$this->display();
		}
		//点赞
		public function praise(){
			$data['global_id'] = $this->fans['id'];
			$data['comment_id'] = $_GET['commentid'];
			$field['fields'] = json_encode($data);
			$replycomment = $this->getRpcData('msCommentFavor/insert',$field);
		}
		//课程分类
		public function menu(){
			$menu = $this->getRpcData('msCourse/getCompetenceFields');
			$menuary = array();
			if($menu['errorCode'] == 0){
					foreach($menu['data'] as $k=>$v){
						$twommenu['competenceFields'] = $v;
						$tmreturn = $this->getRpcData('msCourse/getByCompetenceFields',$twommenu);
						if($tmreturn['errorCode'] == 0){
							$menuary[$k]['menu'] = $v;
							$menuary[$k]['tmenu'] = $tmreturn['data'];
						}else{
								$msg = $tmreturn['errorMassege'];
								$this->assign('error',$msg);
							}
					}
					$this->assign('menu',$menuary);
				}else{
						$msg = $menu['errorMassege'];
						$this->assign('error',$msg);
					}
			$this->display();
		}
		//课次  list
		public function sessionalindex(){
			$kcparam['pageNo'] = 1;
			$kcparam['pageSize'] = 10;
			$kcparam['globalId'] = $this->fans['id'];
			$kcparam['title'] = $_GET['title'];
			$kclist = $this->getRpcData('msScheduledOffering/getScheduledList',$kcparam);
			if($xqitem['errorCode'] == 0){
					$this->assign('kclist',$kclist['data']);
				}else{
						$msg = $xqitem['errorMassege'];
						$this->assign('error',$msg);
					}
			$this->display();
				
		}
		//课次详情
		public function sessionaldetails(){
			$kcparam['scheduledOfferingId'] = $_GET['id'];
			$kclist = $this->getRpcData('msScheduledOffering/getScheduledById',$kcparam);
			if($xqitem['errorCode'] == 0){
					$this->assign('kcdetail',$kclist['data'][0]);
				}else{
						$msg = $xqitem['errorMassege'];
						$this->assign('error',$msg);
					}
			$this->display();
				
		}
		//预报名
		public function register(){
			$regparam['title'] = $_GET['title'];
			$regparam['links'] = $_GET['links'];
			$regparam['email'] = $this->fans['email'];
			$regparam['globalId'] = $this->fans['id'];
			$regparam['scheduledOfferingId'] = $_GET['scheduledOfferingId'];
			$kclist = $this->getRpcData('msPreRegisterLog/toPreRegister',$regparam);
		}
}

?>