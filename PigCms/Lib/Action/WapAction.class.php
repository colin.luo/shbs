<?php
class WapAction extends BaseAction {
	public $token;
	public $wecha_id;
	public $fans;
	public $wxuser;
	public $publicNumberId;
	public $companyCode;
	public $validLogin;
	public $retrieve=false;//是否重新获取accessToken
	public $accessToken;
	public $userAgentType;
	
	protected function _initialize() {
		parent::_initialize();
		
		\Log::write("WapAction::_initialize##URL=".$_SERVER['REQUEST_METHOD'] .$_SERVER['REQUEST_URI'] ,'WARN');
    
		//获取终端类型
		$this->userAgentType = "weixin";
		$agentInfo =  $_SERVER['HTTP_USER_AGENT'];
		if (strstr($agentInfo, "Html5Plus")){
			$this->userAgentType = "html5App";
		}
		$this->assign('userAgentType', $this->userAgentType);
		
		$this->assign('moduleName', MODULE_NAME);
		$this->assign('actionName', ACTION_NAME);
		
		  
		$this->token = $_REQUEST['token'];
		$this->assign('token', $this->token);
		if ($this->token && !preg_match("/^[0-9a-zA-Z]{3,42}$/", $this->token)) {
			exit('error token');
		}
				
		//班级预览
		if ($_GET['show'] ==1){
			cookie('certificate','sinoStrong');
		}
		
		$data['token']= $this->token;	
		
		//根据token获取数据库		
		$this->companyCode = cookie("welearning_companyCode_".$this->token);
		if (!$this->companyCode){			
			$companyInfo = $this->getRpcData('company/getByToken',$data);
			if (!$companyInfo['data']){
				$this->error('根据token['.$this->token.']未找到数据库信息');
				exit();		
			}
			cookie("welearning_companyCode_".$this->token, $companyInfo['data'],3600*24);
			 $this->companyCode = cookie("welearning_companyCode_".$this->token);
			 \Log::write("companyCode from api,companyCode=".$this->companyCode,'WARN');
		}
		else{
			\Log::write("companyCode from cookie,companyCode=".$this->companyCode,'WARN');			
		}   
    
    	//如果当前token与cookie中保存的token不一致，清空缓存中公众号信息
		$cookie_token = cookie('token_' . $this->token);	
		if ($cookie_token != $this->token ){
			S('wxuser_' . $this->token, null);
		}
		
		$this->wxuser = S('wxuser_' . $this->token);
		if (!$this->wxuser){			
			//根据token获取公众号信息	
			$publicNumberInfo = $this->getRpcData('publicNumber/getByToken',$data);
			if ($publicNumberInfo['data']) {
				//将公众号信息保存到缓存中
				S('wxuser_' . $this->token, $publicNumberInfo['data']);
				$this->wxuser = $publicNumberInfo['data'];
			}else{
				$this->error('根据token['.$this->token.']未找到公众号信息');
				exit();
			}
			//保存token到cookie中
			cookie('token_' . $this->token,$this->token,3600*24);
		}
		
		$this->publicNumberId = $this->wxuser['id'];
		$this->assign('wxuser', $this->wxuser);
		
		//获取wecha_id
		$wecha_id = cookie('wecha_id_' . $this->token);	
		
		//app处理
		if ($this->userAgentType == "html5App"){
		    if (isset($_REQUEST['wecha_id']) && $_REQUEST['wecha_id'] != ""){
				  $wecha_id = $_REQUEST['wecha_id'];
			}			
				
			$this->wecha_id = $wecha_id;
			cookie('wecha_id_' . $this->token,$this->wecha_id,3600*24);
				
			if (isset($_GET['isValidate']) && $_GET['isValidate'] == 1){
				cookie('referer'.$this->wecha_id,null);
				$purl=parse_url($_SERVER["REQUEST_URI"]);
                $urlParamStr=$purl['query'];
                $urlParamArr=array();
                parse_str($urlParamStr,$urlParamArr);
                unset($urlParamArr['isValidate']);
    		
				cookie('referer'.$wecha_id,'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.http_build_query($urlParamArr));
				$this->redirect(U('Wap/Index/login',array('token'=>$this->token,'wecha_id'=>$wecha_id)));
			}
		}		
		else{
			if (MODULE_NAME != "Index" && (ACTION_NAME != "login" || ACTION_NAME != "register")){
				cookie('referer'.$this->wecha_id,null);
				$purl=parse_url($_SERVER["REQUEST_URI"]);
                $urlParamStr=$purl['query'];
                $urlParamArr=array();
                parse_str($urlParamStr,$urlParamArr);
				cookie('referer'.$wecha_id,'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.http_build_query($urlParamArr));
			}

			if ($_GET['wecha_id'] == 'oIXgit3laJWepK7f2isoksiqzfX4' || $_GET['wecha_id'] == 'oIXgit5CKoCJydEnGHyKpUdf02P4' ||$_GET['wecha_id'] == 'oZSNXuNSRWXT2PnK5MABamkdtiAA' || $_GET['wecha_id'] =='oIKIewdcQ1OYF8N9HMoYf2W4QTuY' ||  $_GET['wecha_id'] =="oIXgitxPWOlZMNmYCrj8MeBLlYQQ"||$_GET['wecha_id'] =="oIXgitxvjP9VZuuzL_GQiZ65OZo4"||$_GET['wecha_id'] =="oIXgit_Rwt82SCr4lnvKtDTfu5FI"){
				$wecha_id = $_GET['wecha_id'];
			}
		}
		
		if ($wecha_id == '{wecha_id}' || $wecha_id == '{wechat_id}'){
			$wecha_id = "";
		}
		
		if (!$wecha_id){
			$this->wxuser['oauth'] = 1;	//为1时需要获取，需要授权获取wecha_id
		}
		
		//班级预览
		if ($_GET['show'] ==1){
			return;
		}		
		
		//授权获取openid
		if (!$wecha_id && ($this->wxuser['public_type'] == '企业号' || $this->wxuser['public_type'] == "服务号") && !isset($_GET['code']) && $this->wxuser['oauth']) {
			$customeUrl = $this->siteUrl . $_SERVER['REQUEST_URI'];
			$scope = 'snsapi_userinfo';
			if (isset($_GET['diymenu']) || $this->wxuser['public_type'] == '企业号') {
				$scope = 'snsapi_base';
			}
			$oauthUrl = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' . $this->wxuser['app_id'] . '&redirect_uri=' . urlencode($customeUrl) . '&response_type=code&scope=' . $scope . '&state=oauth#wechat_redirect';
			header('Location:' . $oauthUrl);
			exit();
		}

		if (isset($_GET['code']) && isset($_GET['state']) && ($_GET['state'] == 'oauth') ) {
			if ($this->wxuser['public_type'] == '企业号'){
				$rt = $this->curlGet('https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid='.$this->wxuser['app_id'] . '&corpsecret=' . $this->wxuser['app_secret']);
				$accessInfo = json_decode($rt,true);
				$rt = $this->curlGet('https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token='.$accessInfo['access_token'].'&code='.$_GET['code']);
				
				$jsonrt = json_decode($rt, true);	
				if ($jsonrt['UserId']){
					$openid = $jsonrt['UserId'];
				}else{
					$openid = $jsonrt['OpenId'];
				}
			}else{
				$rt = $this->curlGet('https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . $this->wxuser['app_id'] . '&secret=' . $this->wxuser['app_secret'] . '&code=' . $_GET['code'] . '&grant_type=authorization_code');
				$jsonrt = json_decode($rt, true);
				
				$openid = $jsonrt['openid'];
				$access_token = $jsonrt['access_token'];
			}
			$_GET['wecha_id'] = $openid;
			$this->wecha_id = $openid;
			
			if (!$openid) {
				$this->error('授权不对：' . $jsonrt['errcode'], '#');
				exit();
			}
			
			cookie('wecha_id_' . $this->token,$openid,3600*24);
			$customeUrl = $this->siteUrl . $_SERVER['REQUEST_URI'];
			//删除code，修复40029问题
			$customeUrl = str_replace("&code=".$_GET['code'], "",$customeUrl);
			
			//意见反馈的特殊处理
			$this->syncForumInfo();
			
			header('Location:' . $customeUrl);
			exit();
		}else {		
			if ($wecha_id){
				$this->wecha_id = $wecha_id;		
				cookie('wecha_id_' . $this->token,$this->wecha_id,3600*24);	
			}
			else{
				if ($_GET['wecha_id'] != '{wechat_id}' && $this->wxuser['public_type'] == '订阅号' && isset($_GET['from'])){
					//$this->error('从分享链接进入，无法进行该操作，请通过公众号点击菜单进行访问！');
					//exit;
				}else{
					if (isset($_GET['wecha_id']) && $_GET['wecha_id'] != '{wechat_id}'){
						$this->wecha_id = $_GET['wecha_id'];
						cookie('wecha_id_' . $this->token,$this->wecha_id,3600*24);
					}
				}
			}
		}
		

		if ($this->userAgentType == "html5App"){
			//不处理
		}else{
// 			 if (!isset($_GET['show']) && 
// 				(strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') == false
// 			 || (strpos($_SERVER['HTTP_USER_AGENT'], 'Windows') 
// 			 			&& $this->token == 'shtraining33ko4r3oiu5zd50edbc4' 
// 			 			&& MODULE_NAME != 'Content'))) {
// 			  $this->error('请使用移动微信端访问！');
// 			  exit();
// 			}  
		}
		$this->assign('wecha_id', $this->wecha_id);		
		if ($this->userAgentType == "html5App"){
			$return = $this->getRpcData('login/getMemberByOpenid', array('openid'=>$this->wecha_id));
			if ($return['errorCode'] == 0){
				$return['data']['id'] = $return['data']['member_id'];	
			}else{
				$return = $this->getRpcData('member/getByWid',array('wid'=>$this->wecha_id));
			}
		}else{
			$return = $this->getRpcData('member/getByWid',array('wid'=>$this->wecha_id));
		}
		
		$fansInfo = $return['data'];	
		
		//获取微信分享签名
		$signInfo = $this->getSignInfo();			
	    $this->assign('signInfo', $signInfo);
		$this->fans = $fansInfo;
		$this->getShareInfo();//TODO
		
		$this->assign('fans', $fansInfo);
		//意见反馈
		$this->syncForumInfo();
	}
	
	//意见反馈同步信息到新公众号
	private function syncForumInfo(){
	    if(isset($_GET['oldToken'])){
	        $param=$_GET['param'];
	        $infoRes=json_decode(htmlspecialchars_decode($param),true);
	        if($infoRes==''){
	            $this->error('参数错误');
	        }
            $infoRes['wid']=$this->wecha_id;
            $fields['person']=json_encode($infoRes);
            $requestResult=$this->getRpcData("member/guidePerson",$fields);
            if($requestResult['errorCode']==0){
                if($requestResult['data']['forum_colum_id']==''){
                    $this->assign('msg', "您没有论坛栏目的访问权限，请联系管理员！");
    	            $this->display("Index:skip");
                }else{
                    $columnId=$requestResult['data']['forum_colum_id'];
                    $this->redirect('Forum/topics',array('token'=>$this->token,'columid'=>$columnId));
                }
            }else{
                $this->assign('msg','导入人员失败！'.$requestResult['errorMassage']);
                $this->display("Index:skip");
            }
	    }
	}
	public function getLink($url) {
		
		return "";
	}
	
	//通用获取是否分享，以及分享的链接地址，标题，描述，图标
	public function getShareInfo(){
	    $moduleName=MODULE_NAME;
	    $actionName=ACTION_NAME;
	    
	    //清除告警
	    $shareTitle = "";
	    $shareDesc = "";
	    $shareIcon = "";
	    $isShare = "";
	   
	    
	    //分享url处理，为了解决串号问题，wecha_id去除
	    $purl=parse_url($_SERVER["REQUEST_URI"]);
	    $urlParamStr=$purl['query'];
	    $urlParamArr=array();
	    parse_str($urlParamStr,$urlParamArr);
	    unset($urlParamArr['wecha_id']);
	    
	    switch ($moduleName){
	        //内容
	        case 'Content':{
	            $id=(int)$_REQUEST['contentId'];
	            $coverId= $_GET['coverId'];
	            $coverType = $_GET['coverType'];
	            $myCourceType = $_GET['myCourceType']; 
	            $validLogin = $this->checkRight('content',$id,$coverType,$coverId,$myCourceType);
 	           // $validLogin = $this->checkRight('content',$id);
	            $articleList = $this->getRpcData('content/get',array('id'=>$id));
	            $isShare=(int)$articleList['data']['isShare'];
	            $isCollect=$articleList['data']['isCollect'];
	            $this->assign('isCollect',(int)$isCollect);
	            if($isShare){
	                $shareTitle=$articleList['data']['title'];
	                $shareDesc=$articleList['data']['summary'];
	                $shareIcon=spanResUrl($articleList['data']['cover']);
	                //摘要为空则获取内容中的一部分
	                if($shareDesc==''){
	                    $html_body = $this->getRpcData('content/getBody',array('id'=>$id));
	                    $html_body=strip_tags($html_body);
	                    $len=mb_strlen($html_body,'utf-8');
	                    $shareDesc=$len>40?mb_substr($html_body,0,40,'utf-8').'..':$html_body;
	                    if($html_body=='')
	                       $shareDesc='';
	                }
	            }
	        };break;
	        
	        //签到
	        case 'Sign':{	           
                $id=(int)$this->_get('attendanceId');
                $coverId= $_GET['coverId'];
	            $coverType = $_GET['coverType'];
	            $validLogin = $this->checkRight('attendance',$id,$coverType,$coverId);
                $articleList = $this->getRpcData('attendance/get',array('id'=>$id));
                $isShare=(int)$articleList['data']['isShare'];	    
                if($isShare){
                    $shareTitle=$articleList['data']['title'];
                    $shareDesc=$articleList['data']['summary'];
                    $shareIcon=spanResUrl($articleList['data']['cover']);
                }
	        };break;
	        
	        //活动报名
	        case 'Activity':{
                $id=(int)$this->_get('activeId');
                $coverId= $_GET['coverId'];
	            $coverType = $_GET['coverType'];
                $validLogin = $this->checkRight('active',$id,$coverType,$coverId);
                $articleList = $this->getRpcData('active/get',array('id'=>$id));
                $isShare=(int)$articleList['data']['is_share'];
                if($isShare){
                    $shareTitle=$articleList['data']['title'];
                    $shareDesc=$articleList['data']['summary'];
                    $shareIcon=spanResUrl($articleList['data']['cover']);
                }
	           
	        };break;
	        
	        //投票调研
	        case 'Research':{
    	        $id=(int)$this->_get('researchId');
    	        $coverId= $_GET['coverId'];
	            $coverType = $_GET['coverType'];
    	        $validLogin = $this->checkRight('research',$id,$coverType,$coverId);
                $articleList = $this->getRpcData('research/get',array('id'=>$id));
                $isShare=(int)$articleList['data']['is_share'];
                if($isShare){
                    $shareTitle=$articleList['data']['title'];
                    $shareDesc=$articleList['data']['summary'];
                    $shareIcon=spanResUrl($articleList['data']['cover']);
                }
	        };break;	        
	       
	        //作业提交
	        case 'Homework':{
	            $id=(int)$this->_get('homeworkId');
	            $coverId= $_GET['coverId'];
	            $coverType = $_GET['coverType'];
	            $validLogin = $this->checkRight('homework',$id, $coverType,$coverId);
                $articleList = $this->getRpcData('homework/get',array('id'=>$id));
                //管理端没配置默认不允许分享
                $isShare= 0;//(int)$articleList['data']['is_share'];
                if($isShare){
                    $shareTitle=$articleList['data']['title'];
                    $shareDesc=$articleList['data']['summary'];
                    $shareIcon=spanResUrl($articleList['data']['cover']);
                }
	        };break;
	         //考试
	        case 'Exam':{	            
	                $id=(int)$this->_get('examId');
	                $coverId = $_GET['coverId'];
	                $coverType= $_GET['coverType'];
	                if ($actionName != "errors" && $actionName != "shareExam"){
	                    $validLogin = $this->checkRight('exam',$id,$coverType,$coverId);
	                }
	               
	                $articleList = $this->getRpcData('examArrange/get',array('id'=>$id));
	                $isShare=(int)$articleList['data']['isForward']; 
	                if($isShare){
	                    $shareTitle=$articleList['data']['title'];
	                    $shareDesc=$articleList['data']['summary'];
	                    $shareIcon=spanResUrl($articleList['data']['cover']);
	                }
	            
	        };break;
	        
	        //班级
	        case 'Index':{
	            //解决身份验证页面的查询不到数据的问题
	            $coverId = $_GET['coverId'];
	            $coverType = $_GET['coverType'];
	            if($actionName=='index'||$actionName=='lists'){
	                $id=(int)$this->_get('classid');
	                $validLogin = $this->checkRight('project',$id,$coverType,$coverId);
	                $articleList = $this->getRpcData('project/get',array('id'=>$id));
	                $isShare=(int)$articleList['data']['isShare'];
	                 
	                if($isShare){
	                    $shareTitle=$articleList['data']['name'];
	                    $shareDesc=$articleList['data']['summary'];
	                    $shareIcon=spanResUrl($articleList['data']['cover']);
	                }
	            }
	        };break;
	        
	        //论坛
	        
	        case 'Forum':{
	            //$id=(int)$this->_get('classid');
                $articleList = $this->getRpcData('Forum/getForumConfig');
                $isShare=(int)$articleList['data']['isShare'];
              
                if($isShare){
                    $shareTitle=$articleList['data']['name'];
                    $shareDesc=$articleList['data']['summary'];
                    $shareIcon=spanResUrl($articleList['data']['cover']);
                }
	        };break;
	       
	        //学院
	        case 'College':{	            
	                $id=(int)$this->_get('collegeId');
	                //$validLogin = $this->checkRight('college',$id);
	                $articleList = $this->getRpcData('college/get',array('id'=>$id));
	                $isShare= 1;//(int)$articleList['data']['isShare'];
	                 
	                if($isShare){
	                    $shareTitle=$articleList['data']['title'];
	                    $shareDesc=$articleList['data']['summary'];
	                    $shareIcon=spanResUrl($articleList['data']['cover']);
	                }
	            
	        };break;
	         //巡店
	        case 'EvaluationReport':{	 
	        		$isShare = 1;
	        	 	$id=(int)$this->_get('id');   
	        		$evaluation = $this->getRpcData('evaluationReport/get',array('id'=>$id));
	        	 	$shareTitle=$evaluation['data']['title'];
	        	 	
            	    $shareDesc=$evaluation['data']['summary']?$evaluation['data']['summary']:$evaluation['data']['title'];
            	    $shareIcon=$evaluation['data']['cover']?spanResUrl($evaluation['data']['cover']):C('WAP_URL').'tpl/Wap/default/common/images/evaluationreport.png';
 
	        };break;
	        //演示及测试页面
	        case 'Demo':{
	            $isShare = 1;
	        };break;
	        
	        //培训跟踪
	        case 'Train':{
	            if($actionName =='lectorCheckDetail'|| $actionName =="studentDetail"){
	                $name = (string)$this->_get('name');
    	            $isShare = 1;
    	            $shareTitle=$name.'的培训跟踪';
    	            $shareDesc='最新评估出炉 !';
    	            $shareIcon=C('WAP_URL').'tpl/Wap/default/common/images/train/train.png';
	            }
	        };break;
	        //供应商评价
	         case 'Supplier':{	 
	         		$ThePublic = $this->getRpcData('publicNumber/allPublicInfo');
					$info = $this->getRpcData('publicNumber/get',array('id'=>$ThePublic['data'][0]['id']));
					if($info['data']['is_authentication']==true){			
						$isShare = 1;
					}else{
						$isShare = 0;
					}
	        		
	        	 	$id=(int)$this->_get('id');  
	        	 	 // $validLogin = $this->checkRight('supcmt_rate',$id); 
	        		$evaluation = $this->getRpcData('supcmtRateRes/getById',array('id'=>$id));
	        		//dump($evaluation);exit;
	        	 	$shareTitle=$evaluation['data']['title'];
	        	 	
            	$shareDesc=$evaluation['data']['summary']?$evaluation['data']['summary']:$evaluation['data']['title'];
            	$shareIcon=$evaluation['data']['pic']?spanResUrl($evaluation['data']['pic']):C('WAP_URL').'tpl/Wap/default/common/images/evaluationreport.png';
 
	        };break;
	        default:{
	            
	        };
	    }	    
	   
	    //允许粉丝访问：允许浏览基本信息；内容模块，当需要和后台进行交互时，跳转至身份验证页面；活动类模块，点击首页的（下一步）按钮时，跳转至身份验证页面。
        if(($moduleName == 'Sign' || $moduleName == 'Activity' ||$moduleName == 'Research' ||$moduleName == 'Homework' ||$moduleName == 'Supplier') 
          && $actionName != 'index' && $validLogin == 1){
            
          $this->redirect(U('Wap/Index/login',array('token'=>$this->token,'wecha_id'=>$this->wecha_id)));	
          
        }
	    
	    if ($actionName == 'index' || $actionName == 'getReportByMember' || ($moduleName == "Supplier" && $actionName == "share")){
	    	 	if ((isset($_GET['isShare']) && 0 == (int)$_GET['isShare']) || 0 == $isShare){
    	 			$isShare = 0;
    	 		}
    	 		if (isset($_GET['isShare'])){
    	 			 cookie("IS_SHARE_".$moduleName.$id, $isShare);
    	 		}
    	 		else{
    	 			cookie("IS_SHARE_".$moduleName.$id, -1);
    	 		}
	    }
	    else{
	    	
	    	if ($moduleName == "Exam" || $moduleName == "Research"){
	    		$cookieIsShare = cookie("IS_SHARE_".$moduleName.$id);
	    		
	    		if ($isShare == 1 && ($cookieIsShare == 1 || $cookieIsShare == -1)){
	    			$isShare = 1;
	    		}
	    		else{
	    			$isShare = 0;
	    		}
	    	}
	    	else{
	    		//$isShare = 0;
	    	}	    	
	    }
	    
	    $this->validLogin = $validLogin;
	    $this->assign('islogin',$validLogin);
	   
	    
	    ///不允许分享 且 来至于分享链接
	    if (!$isShare && isset($_GET['from']) && !isset($_GET['needVerify'])){
	    	$this->error('从分享链接进入，无法进行该操作，请通过公众号点击菜单进行访问！');
		    exit;    	
	    }
	    
	    $shareUrl= 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.http_build_query($urlParamArr);
	    $shareDesc=str_replace(array("\r\n", "\r", "\n"),"",$shareDesc);
	    
		    $this->assign("shareUrl",$shareUrl);
		    $this->assign("shareTitle",$shareTitle);
		    $this->assign("shareDesc",$shareDesc);
		    $this->assign("shareIcon",$shareIcon);
		   
		    $this->assign("isShare",$isShare);
		  
	    $this->assign("shareResId",$id);
	    $this->assign("moduleName",$moduleName);
	}
	
	//分享时插入分享数据
	public function insertShareRecord(){
	    $moduleName = $_POST['moduleName'];
    	$data['member_id'] = $this->fans['id'];
    	if ($moduleName == 'Content'){
        	$data['content_id'] = (int)$_POST['id'];
        	$fields['fields'] = json_encode($data);
    	    $result = $this->getRpcData('contentShare/insert',$fields);
    	}
    	
	    if ($result['data'] >0){
	    	echo 1;
	    }
	    else{
	    	echo 0;
	    }    	
    }
	public function strExists($haystack, $needle){
		return !(strpos($haystack, $needle) === FALSE);
	}
	public function curlGet($url){
		$ch = curl_init();
		$header = "Accept-Charset: utf-8";
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$temp = curl_exec($ch);
		return $temp;
	}
	
	/**
	 * 访问权限判断
	 * @return 1 只允许查看; 2 查看和发表
	 */
	public function checkRight($type,$id,$coverType,$coverId,$myCourceType){
	 
		if (isset($_REQUEST['needVerify']) && (int)$_REQUEST['needVerify'] == 0){
			return 2;
		}
		if (IS_GET || (isset($_GET['method']) && $_GET['method'] == 'get')){			
			$haveC['id'] = $this->fans['id'];
	 	 	$haveC['resourceType'] = $type;
	 	 	$haveC['resourceId'] = (int)$id;
	 	 	$haveC['coverType'] = $coverType;
	 	 	$coverId = (int)$coverId;
	 	 	if($coverId > 0){
	 	 		$haveC['coverId'] = (int)$coverId;
	 	 		
	 	 	}
	 	 	if($myCourceType == 'type'){//课程中心跳转权限
	 	 		$haveR = $this->getRpcData('member/haveRightForCourseCenter',$haveC);		
			}else{	
				if($coverType == 'myTask'){//我的任务跳转权限
					$haveR = $this->getRpcData('member/haveRightByMyTask',$haveC);
				}else{//基本跳转权限
					// var_dump($haveC);
					$haveR = $this->getRpcData('member/haveRight',$haveC);
					// var_dump($haveR);
				}			
			}
			if ($haveR['errorCode'] != 0 && $haveR['errorCode'] != 13){
				$this->error($haveR['errorMassage']);	
				exit;	
			}else{
    		  	if ($haveR['data'] == 0 || $haveR['data'] == 13){  
    		  		if (isset($_GET['from']) && isset($haveR['data']) && $haveR['data'] == 0 && $this->wxuser['public_type'] == '订阅号'){
    		  			$this->error('从分享链接进入，无法进行该操作，请通过公众号点击菜单进行访问！');
    				    exit;
    		  		}else{		  		    
    				  $this->redirect(U('Wap/Index/login',array('token'=>$this->token,'wecha_id'=>$this->wecha_id)));		
    			    }
    		  	}
    		  	else if($haveR['data'] == 1){
    		  		 return 1;
    		  	}else{
    		  		return 2;
    		  	}
		    }	
		}
	}
	
	public function getUserById($id){
		
		$result = $this->getRpcData('member/getPerson',array('id'=>$id)); //ok
		
		return $result['data'];
	}
	
	public function  getSignInfo(){
		$jssdk = new WechatAddr($this->wxuser['app_id'],$this->wxuser['app_secret'],$this->token,$this->wxuser['public_type']);
		$signInfo = $jssdk->GetSignPackage();
		return $signInfo;
	}
	
	public function  _getAccessToken(){
		$jssdk = new WechatAddr($this->wxuser['app_id'],$this->wxuser['app_secret'],$this->token,$this->wxuser['public_type']);
		$accessToken = $jssdk->getAccessToken();
		return $accessToken;
	}
	
	public function getRpcData($path,$param=NULL,$companyCode =NULL){			
		  $certificate = cookie('certificate' . $this->wecha_id);				 	 	
		  if (!$companyCode){
			$companyCode = cookie("welearning_companyCode_".$this->token);
		  }
         //证书已过期
    	 if (!$certificate){			 	
     		if ($this->fans['certificate']){//从member/getByWid证书			 				
     			cookie('certificate'.$this->wecha_id,$this->fans['certificate'],3600*24);
     		}
     		else{ //从login/connectValidate证书
     			 if ($path != 'company/getByToken'){
    	 			$this->getCertificate($companyCode);
    	 		}
     		}
     		$certificate = cookie('certificate' . $this->wecha_id);	
    	 }
    
    	
    	$result = $this->getRpcDataBase($path,$param,$companyCode,$certificate);
    	if ($result == -10001){				
    		//重新获取login/connectValidate证书
    		$this->getCertificate($companyCode);				
    		$certificate = cookie('certificate' . $this->wecha_id);	
    		
    		$result = $this->getRpcDataBase($path,$param,$companyCode,$certificate);				
    		//如果重新获取证书还是失败，则跳转到绑定页面
    		if ($result == -10001){
    			$this->redirect(U('Wap/Index/login',array('token'=>$this->token,'wecha_id'=>$this->wecha_id)));		
    		}
    	}
    	
    	return $result;
	}
	
	private function getCertificate($companyCode){
  	    //获取签名证书
  	    if (!$this->wecha_id ){//&& isset($_GET['from'])){
  	    	$login_conf['wid'] = "Fans".time();
  	    }
  	    else{
	  	    $login_conf['wid'] = $this->wecha_id; 
	  	  }
        $login_conf['ip'] = $this->getclientip(0);
        $login_conf['appId'] = 'welearning_wechat'; 
        $login_conf['appSecret'] = 'learn2strong';
     
		$isLogin = $this->getRpcDataBase('login/connectValidate',$login_conf, $companyCode);
		
		if ($isLogin['errorCode'] == 0 && $isLogin['data']['certificate']){
			cookie('certificate'.$this->wecha_id,$isLogin['data']['certificate'],3600*24);
		}			
	}
	
	public function spanResUrl($url){		
		
		return str_replace('RES_URL',C('RES_URL'),htmlspecialchars_decode($url));
	}
	
	public function splitResUrl($url){
		
		return str_replace(C('RES_URL'),'RES_URL',htmlspecialchars_decode($url));
	}
	
	//发送模板消息
	public function sendTempMsg($tempKey,$dataArr){
        //获取access_token
	    $accessToken=$this->_getAccessToken();
	    if(!$accessToken){
	        exit('无法获取accessToken');
	    }
	    // 准备发送请求的数据 
		$requestUrl = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token='.$accessToken;
		$data = $this->getData($tempKey,$dataArr,"#666");
		
		$sendData = '{"touser":"'.$dataArr["touser"].'","template_id":"'.$tempKey.'","url":"'.$dataArr["href"].'","topcolor":"#029700","data":'.$data.'}';
	    //var_dump($data,$sendData);exit;
		$rt=$this->postCurl($requestUrl,$sendData);
		if($rt['errorno']==40001){
		    $accessToken=$this->retrieveAccessToken();
		    $requestUrl = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token='.$accessToken;
		    $rt=$this->postCurl($requestUrl,$sendData);
		}
		return $rt;
	}
	// Post Request
	public function postCurl($url, $data){
	    $ch = curl_init();
	    $header = "Accept-Charset: utf-8";
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	    curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	    curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $tmpInfo = curl_exec($ch);
	    $errorno=curl_errno($ch);
	    if ($errorno) {
	        	
	        return array('rt'=>false,'errorno'=>$errorno);
	    }else{
	        $js=json_decode($tmpInfo,1);
	        if ($js['errcode']=='0'){
	
	            return array('rt'=>true,'errorno'=>0);
	        }else {
	            //exit('模板消息发送失败。错误代码'.$js['errcode'].',错误信息：'.$js['errmsg']);
	            return array('rt'=>false,'errorno'=>$js['errcode'],'errmsg'=>$js['errmsg']);
	
	        }
	    }
	}
	// Get Data.data
	public function getData($key,$dataArr,$color){
	    $tempsArr = $this->templates();
	    $data = $tempsArr["$key"]['vars'];
	    $data = array_flip($data);
	    $jsonData = '';
	    foreach($dataArr as $k => $v){
	        if(in_array($k,array_flip($data))){
	            if($k=='remark')
	                $color='#3366FF';
	            $jsonData .= '"'.$k.'":{"value":"'.$v.'","color":"'.$color.'"},';
	        }
	    }
	    $jsonData = rtrim($jsonData,',');
	    return "{".$jsonData."}";
	}
	
	public function templates(){
	    return array(
	        'o1teebLgjuySQUHghe0dXJ5JUA1UsRpgDa4VEfT0Eug' =>
	        array(
	            'name'=>'工作室提醒',
	            'vars'=>array('first','keyword1','keyword2','remark'),
	            'industry'=>'教育-培训',
	            'content'=>'
{{first.DATA}}
时间：{{keyword1.DATA}}
内容：{{keyword2.DATA}}
{{remark.DATA}}'),
	        'qazO-BrbDg-9ZZ-TW79TEC-Ek78R2CBRsIcTNM14MsA' =>
	        array(
	            'name'=>'班级通知',
	            'vars'=>array('first','keyword1','keyword2','keyword3','keyword4','remark'),
	            'industry'=>'教育-培训',
	            'content'=>'
{{first.DATA}}
所在班级：{{keyword1.DATA}}
通知老师：{{keyword2.DATA}}
通知时间：{{keyword3.DATA}}
通知内容：{{keyword4.DATA}}
{{remark.DATA}}'
	        ),
	    );
	}
	
	private function retrieveAccessToken(){
	    $this->retrieve=true;
	    $this->accessToken=$this->_getAccessToken();
	    return $this->accessToken;
	}
    public function pictureColor($return){//底部当前模块
  	$m 	=	MODULE_NAME;
  	$a 	=	ACTION_NAME;
  	$link = 'LOCAL_URLtpl/User/default/common/images/portal/bottomMenu/default/';
  	$link1 = 'LOCAL_URLtpl/User/default/common/images/portal/bottomMenu/active/';
	foreach ($return['data'] as &$v) {
		if($v['type'] == 1){
				if($v['refType'] =='index' && $m == 'Portal' && $a == 'index'){
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($v['refType'] =='class' && $m == 'Portal' && $a == 'grade'){
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($v['refType'] =='coursecenter' && $m == 'Course' && $a == 'courseCenter'){
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($v['refType'] =='mycourse' && $m == 'Portal' && $a == 'curriculum'){
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($v['refType'] =='pushcourse' && $m == 'Portal' && $a == 'curriculum1'){//推荐课程 0
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($v['refType'] =='newcourse' && $m == 'Portal' && $a == 'newcourse'){//最新课程 0
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($v['refType'] =='integral' && $m == 'Store' && $a == 'index'){//积分商城
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($v['refType'] =='exam' && $m == 'Portal' && $a == 'exam'){//我的考试
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($v['refType'] =='errorquestion' && $m == 'Portal' && $a == 'mistakes'){//错题集
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($v['refType'] =='myjob' && $m == 'Portal' && $a == 'task'){//我的作业
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($v['refType'] =='game' && $m == 'Portal' && $a == 'game'){//游戏 0
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($v['refType'] =='active' && $m == 'Portal' && $a == 'active'){//活动报名
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($v['refType'] =='research' && $m == 'Portal' && $a == 'active'){//投票调研
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($v['refType'] =='patrol' && $m == 'Portal' && $a == 'active'){//巡店汇报
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($v['refType'] =='training' && $m == 'Train' && $a == 'menuIndex'){//培训跟踪
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($v['refType'] =='fourm' && $m == 'Forum' && $a == 'index'){//微社区
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($v['refType'] =='askanswer' && $m == 'Forum' && $a == 'index'){//你问我答
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($v['refType'] =='zzx' && $m == 'WeekPlan' && $a == 'index'){//周周学
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($v['refType'] =='sign' && $m == 'Portal' && $a == 'active'){//签到
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($v['refType'] =='personcenter' && $m == 'Personcenter' && $a == 'index'){//个人中心
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}			
		}
		else{
			$str=$v['url'];
		    $urlParamArr=array();
		   	parse_str($str,$urlParamArr);
		   	$nemm = $urlParamArr['amp;amp;m'];
		   	$nema = $urlParamArr['amp;amp;a'];
				if($m == $nemm && $a == $nema){
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($m == $nemm && $a == $nema){
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($m == $nemm && $a == $nema){
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($m == $nemm && $a == $nema){
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($m == $nemm && $a == $nema){//推荐课程 0
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($m == $nemm && $a == $nema){//最新课程 0
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($m == $nemm && $a == $nema){//积分商城
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($m == $nemm && $a == $nema){//我的考试
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($m == $nemm && $a == $nema){//错题集
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($m == $nemm && $a == $nema){//我的作业
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($m == $nemm && $a == $nema){//游戏 0
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($m == $nemm && $a == $nema){//活动报名
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($m == $nemm && $a == $nema){//投票调研
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($m == $nemm && $a == $nema){//巡店汇报
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($m == $nemm && $a == $nema){//培训跟踪
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($m == $nemm && $a == $nema){//微社区
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($m == $nemm && $a == $nema){//你问我答
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}	
				else if($m == $nemm && $a == $nema){//周周学
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($m == $nemm && $a == $nema){//签到
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
				else if($m == $nemm && $a == $nema){//个人中心
					$v['logo'] = str_replace($link,$link1,$v['logo']);
					$v['name'] = '<font style="color:#29c3db;"">'.$v['name'].'</font>';
				}
 		}
	}
	return $return;
  }
  public function Navigation(){
    $ht = '';
  	$return = $this->getRpcData('portalMenu/all');
  	$return = $this->pictureColor($return);
  	$res = $this->getRpcData('portalMenuSet/all');
  	$width = 100/count($return['data']);

  	foreach ($return['data'] as $key => $value) {
  		if($margin1 == 5 && $res['data'][0]['type'] == 2){
  			$value['name'] = mb_strcut($value['name'],0,8,'utf-8');
  		}
  	
  		if($value['logo']){
			$value['logo']=spanResUrl($value['logo']);
		}
	   
	   	if($value['type'] == 2){
	   		if($value['url']){
	   			$value['url'] = spanResUrl($value['url']);
	   			$url2 = 'onclick="document.location=\''.$value['url'].'\'"';
	   		}else{
				$url2 = '';
	   		}
	   		$ht .= '<div class="bottom-div" style="width:'.$width.'%;" '.$url2.'>';
	   	}else if($value['type'] == 1){
	   		$ht .= '<div  class="bottom-div" style="width:'.$width.'%;" onClick="bottom(\''.$value['refType'].'\',\''.$value['id'].'\');">';
	   	}
	   	if($res['data'][0]['type'] == 1){
			$ht .=	'<img src="'.$value['logo'].'"  class="bottom-img">';
			$ht .=	'<div class="bottom-div-div" style="color:#505050;">'.$value['name'].'</div>';
		}
		else if ($res['data'][0]['type'] == 2){
		
			$ht .=  '<div  class="bottom-li-x">';
			if($key == 0){
				$ht .=	'<img src="'.$value['logo'].'"  class="bottom-img1" >';	
			}else{
				$ht .=	'<img src="'.$value['logo'].'"  class="bottom-img1" >';	
			}
			$ht .= '<span class="bottom-div-div1" >'.$value['name'].'</span>';
			$ht .=  '</div>';
		}
		else if ($res['data'][0]['type'] == 3){
			$ht .= '<span class="bottom-div-div2" >'.$value['name'].'</span>';
		}
		
		
		$ht .='</div>';
  	}	
  	  	$data[2] = $ht;
  	  	$data[3] = $res['data'][0]['status'];
  	  	return $data;
  }
}
?>