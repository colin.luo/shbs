<?php
class AppAction extends BaseAction {
	public $token;
	public $wecha_id;
	public $fans;
	public $wxuser;
	public $publicNumberId;
  public $companyCode;

	
	protected function _initialize() {
		parent::_initialize();
		
		\Log::write("AppAction::_initialize##URL=".$_SERVER['REQUEST_METHOD'] .$_SERVER['REQUEST_URI'] ,'WARN');
   	
   	$this->assign('moduleName', MODULE_NAME);
		$this->assign('actionName', ACTION_NAME);
		    
		$this->token = $_REQUEST['token'];
		$this->assign('token', $this->token);			
		
		$data['token']= $this->token;	
		//根据token获取数据库		
		$this->companyCode = "ruixue_test";//cookie("welearning_companyCode_".$this->token);
    
		if (!$this->companyCode){			
			$companyInfo = $this->getRpcData('company/getByToken',$data);
			if (!$companyInfo['data']){
				$this->error('根据token['.$this->token.']未找到数据库信息');
				exit();		
			}
			cookie("welearning_companyCode_".$this->token, $companyInfo['data'],3600*24);
			$this->companyCode = cookie("welearning_companyCode_".$this->token);
			\Log::write("companyCode from api,companyCode=".$this->companyCode,'WARN');
		}
		else{
			if ($this->token){
				cookie("welearning_companyCode_".$this->token, $this->companyCode,3600*24);
			}
			\Log::write("companyCode from cookie,companyCode=".$this->companyCode,'WARN');			
		}   
    
  	//app调用以下接口不处理
		if (MODULE_NAME == "Login" 
			&& ACTION_NAME == "getAppConfigMap"){
					
			return;						
		}
    
		//如果当前token与cookie中保存的token不一致，清空缓存中公众号信息
		$cookie_token = cookie('token_' . $this->token);	
		if ($cookie_token != $this->token ){
			S('wxuser_' . $this->token, null);
		}
		
		$this->wxuser = S('wxuser_' . $this->token);
		if (!$this->wxuser){			
			//根据token获取公众号信息	
			$publicNumberInfo = $this->getRpcData('publicNumber/getByToken',$data);
			if ($publicNumberInfo['data']) {
				//将公众号信息保存到缓存中
				S('wxuser_' . $this->token, $publicNumberInfo['data']);
				$this->wxuser = $publicNumberInfo['data'];
			}	
			else{
				$this->error('根据token['.$this->token.']未找到公众号信息');
				exit();
			}
			//保存token到cookie中
			cookie('token_' . $this->token,$this->token,3600*24);
		}
		
		$this->publicNumberId = $this->wxuser['id'];
		$this->assign('wxuser', $this->wxuser);		
	
		//获取wecha_id
		$wecha_id = cookie('wecha_id_' . $this->token);	
		if (isset($_REQUEST['wecha_id']) && $_REQUEST['wecha_id'] != ""){
			$wecha_id = $_REQUEST['wecha_id'];
		}
		$this->wecha_id = $wecha_id;
		cookie('wecha_id_' . $this->token,$this->wecha_id,3600*24);		
			
		if (isset($_GET['isValidate']) && $_GET['isValidate'] == 1){
			cookie('referer'.$this->wecha_id,null);
			$purl=parse_url($_SERVER["REQUEST_URI"]);
	                $urlParamStr=$purl['query'];
	                $urlParamArr=array();
	                parse_str($urlParamStr,$urlParamArr);
	                unset($urlParamArr['isValidate']);
			
			cookie('referer'.$wecha_id,'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.http_build_query($urlParamArr));
			$this->redirect(U('Wap/Index/login',array('token'=>$this->token,'wecha_id'=>$wecha_id)));
		}		
		$this->assign('wecha_id', $this->wecha_id);
		
		//获取用户信息
		$return = $this->getRpcData('login/getMemberByOpenid', array('openid'=>$this->wecha_id));
		if ($return['errorCode'] == 0){
			$return['data']['id'] = $return['data']['member_id'];	
		}
		else{
			$return = $this->getRpcData('member/getByWid',array('wid'=>$this->wecha_id));
		}
		$this->assign('fans', $return['data']);
		
	}

	public function curlGet($url){
		$ch = curl_init();
		$header = "Accept-Charset: utf-8";
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$temp = curl_exec($ch);
		return $temp;
	}
	
	///返回值： 1 只允许查看; 2 查看和发表
	public function checkRight($type,$id){
	 
		if (isset($_REQUEST['needVerify']) && (int)$_REQUEST['needVerify'] == 0){
			return 2;
		}
		if (IS_GET || (isset($_GET['method']) && $_GET['method'] == 'get')){			
			$haveC['id'] = $this->fans['id'];
	 	 	$haveC['resourceType'] = $type;
	 	 	$haveC['resourceId'] = (int)$id;
			$haveR = $this->getRpcData('member/haveRight',$haveC);
		  
			if ($haveR['errorCode'] != 0 && $haveR['errorCode'] != 13){
				$this->error($haveR['errorMassage']);	
				exit;	
			}
			else{
    		  	if ($haveR['data'] == 0 || $haveR['data'] == 13){  
    		  		if (isset($_GET['from']) && isset($haveR['data']) && $haveR['data'] == 0 && $this->wxuser['public_type'] == '订阅号'){
    		  			$this->error('从分享链接进入，无法进行该操作，请通过公众号点击菜单进行访问！');
    				    exit;
    		  		}
    		  		else{		  		    
    				  $this->redirect(U('Wap/Index/login',array('token'=>$this->token,'wecha_id'=>$this->wecha_id)));		
    			    }
    		  	}
    		  	else if($haveR['data'] == 1){
    		  		 return 1;
    		  	}
    		  	else{
    		  		return 2;
    		  	}
		    }	
		}
	}
	

	
	public function getRpcData($path,$param=NULL,$companyCode =NULL){			
		  $certificate = cookie('certificate' . $this->wecha_id);				 	 	
		  if (!$companyCode){
				$companyCode = cookie("welearning_companyCode_".$this->token);
		  }
         //证书已过期
    	 if (!$certificate){			 	
     		if ($this->fans['certificate']){//从member/getByWid证书			 				
     			cookie('certificate'.$this->wecha_id,$this->fans['certificate'],3600*24);
     		}
     		else{ //从login/connectValidate证书
     			 if ($path != 'company/getByToken'){
    	 			$this->getCertificate($companyCode);
    	 		}
     		}
     		$certificate = cookie('certificate' . $this->wecha_id);	
    	 }
    
    	
    	$result = $this->getRpcDataBase($path,$param,$companyCode,$certificate);
    	if ($result == -10001){				
    		//重新获取login/connectValidate证书
    		$this->getCertificate($companyCode);				
    		$certificate = cookie('certificate' . $this->wecha_id);	
    		
    		$result = $this->getRpcDataBase($path,$param,$companyCode,$certificate);				
    		//如果重新获取证书还是失败，则跳转到绑定页面
    		if ($result == -10001){
    			$this->redirect(U('Wap/Index/login',array('token'=>$this->token,'wecha_id'=>$this->wecha_id)));		
    		}
    	}
    	
    	return $result;
	}
	
	private function getCertificate($companyCode){
  	    //获取签名证书
  	    if (!$this->wecha_id ){//&& isset($_GET['from'])){
  	    	$login_conf['wid'] = "Fans".time();
  	    }
  	    else{
	  	    $login_conf['wid'] = $this->wecha_id; 
	  	  }
        $login_conf['ip'] = $this->getclientip(0);
        $login_conf['appId'] = 'welearning_wechat'; 
        $login_conf['appSecret'] = 'learn2strong';
     
		$isLogin = $this->getRpcDataBase('login/connectValidate',$login_conf, $companyCode);
		
		if ($isLogin['errorCode'] == 0 && $isLogin['data']['certificate']){
			cookie('certificate'.$this->wecha_id,$isLogin['data']['certificate'],3600*24);
		}			
	}

	
	// Post Request
	public function postCurl($url, $data){
	    $ch = curl_init();
	    $header = "Accept-Charset: utf-8";
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	    curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	    curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $tmpInfo = curl_exec($ch);
	    $errorno=curl_errno($ch);
	    if ($errorno) {
	        	
	        return array('rt'=>false,'errorno'=>$errorno);
	    }else{
	        $js=json_decode($tmpInfo,1);
	        if ($js['errcode']=='0'){
	
	            return array('rt'=>true,'errorno'=>0);
	        }else {
	            //exit('模板消息发送失败。错误代码'.$js['errcode'].',错误信息：'.$js['errmsg']);
	            return array('rt'=>false,'errorno'=>$js['errcode'],'errmsg'=>$js['errmsg']);
	
	        }
	    }
	}
	
}
?>