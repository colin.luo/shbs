<?php
class LoginAction extends UserAction{
    public function index(){
        if(IS_POST){
         
        	
        	
        	$data['loginName'] = $this->_post('userName', 'htmlspecialchars');
        	$data['public_number_id'] = (int)1;
        	$data['password'] = $this -> _post('userPwd', 'htmlspecialchars');
        	
        	
        	$return=$this->getRpcData("serviceUser/login",$data);
        	if($return['data']["serviceUsedId"]){
        		$result=$this->getRpcData("serviceUser/get",array('id'=>$return['data']["serviceUsedId"]));
        		session('userId', $result['data']["id"]);
        		session('name',$result['data']["nickName"]);
        		session('token', $result['data']["token"]);
        		session('userName', $result['data']['loginName']);
        		$this -> success('登陆成功', U('Index/index'));
        	}else{
        		 $this -> error($return["errorMassage"]);
        	}
        }else{
            $this -> display();
        }
    }
    //退出登录
    public function logout(){
        session('userId', null);
        session('name',null);
        session('userName', null);
        
        unset($_SESSION[userId]);
        unset($_SESSION[name]);
        $this -> redirect(U('Login/index'));
    }
}
?>
