<?php
class IndexAction extends UserAction{
    public $thisWxUser;
    public function _initialize(){
        parent :: _initialize();
        $this -> assign('token', session('token'));
    }
    public function index(){

        $Page = new Page($count, 50);
        $show = $Page -> show();//'token' => $_SESSION['token'],

       //获取好友列表
        foreach($list as $key => $limen){
            $dataUp['joinUpDate'] = time();
            $dataUp['uid'] = session('userId');
            $dataUp['id'] = $limen['id'];
        }

        $this -> assign('page', $show);
        $this -> display();
    }
    function getTime(){
        if (!$sTime) return '';
        $cTime = time();
        $dTime = $cTime - $sTime;
        $dDay = intval(date("z", $cTime)) - intval(date("z", $sTime));
        $dYear = intval(date("Y", $cTime)) - intval(date("Y", $sTime));
        if($type == 'normal'){
            if($dTime < 60){
                if($dTime < 10){
                    return '刚刚';
                }else{
                    return intval(floor($dTime / 10) * 10) . "秒前";
                }
            }elseif($dTime < 3600){
                return intval($dTime / 60) . "分钟前";
            }elseif($dYear == 0 && $dDay == 0){
                return '今天' . date('H:i', $sTime);
            }elseif($dYear == 0){
                return date("m月d日 H:i", $sTime);
            }else{
                return date("Y-m-d H:i", $sTime);
            }
        }elseif($type == 'mohu'){
            if($dTime < 60){
                return $dTime . "秒前";
            }elseif($dTime < 3600){
                return intval($dTime / 60) . "分钟前";
            }elseif($dTime >= 3600 && $dDay == 0){
                return intval($dTime / 3600) . "小时前";
            }elseif($dDay > 0 && $dDay <= 7){
                return intval($dDay) . "天前";
            }elseif($dDay > 7 && $dDay <= 30){
                return intval($dDay / 7) . '周前';
            }elseif($dDay > 30){
                return intval($dDay / 30) . '个月前';
            }
        }elseif($type == 'full'){
            return date("Y-m-d , H:i:s", $sTime);
        }elseif($type == 'ymd'){
            return date("Y-m-d", $sTime);
        }else{
            if($dTime < 60){
                return $dTime . "秒前";
            }elseif($dTime < 3600){
                return intval($dTime / 60) . "分钟前";
            }elseif($dTime >= 3600 && $dDay == 0){
                return intval($dTime / 3600) . "小时前";
            }elseif($dYear == 0){
                return date("Y-m-d H:i:s", $sTime);
            }else{
                return date("Y-m-d H:i:s", $sTime);
            }
        }
    }

    //获取好友列表
    public function ajaxList(){
        if(IS_AJAX){
            if($data != false){
                foreach($data as $key => $limen){
                    $dataUp['joinUpDate'] = time();
                    $dataUp['uid'] = session('userId');
                    $dataUp['id'] = $limen['id'];
                    $list1[$key]['endtime'] = $this -> getTime($limen['enddate'], 'mohu');
                }
                echo json_encode($list1);
            }
        }else{
            exit('erorr:3306');
        }
    }

    //获取聊天信息
    public function ajaxMain(){
        if(IS_AJAX){
            if(!$time = $this -> _post('time', 'htmlspecialchars')){
                exit(1);
            }
            if($list != false){
                $list = array_reverse($list);
                echo json_encode($list);
            }else{
                echo 1;
            }
        }else{
            exit('{eror:2031}');
        }
    }
    public function main(){
        foreach($msgList as $key => $List){
            $msgList[$key]['type'] = 1;
        }
        foreach($logs as $key => $List){
            $logs[$key]['type'] = 2;
        }
        $newarray = array_merge($msgList, $logs);
        $enddata = $logs?$this -> array2sort($newarray, 'enddate'):$msgList;
        $this -> assign('msgList', $enddata);
        $endtime = $msgList[0];
        $userInfo['openid'] = $where['openid'];
        $this -> assign('endtime', $endtime['enddate']);
        $this -> assign('userInfo', $userInfo);
        $this -> display();
    }
    private function array2sort($a, $sort, $d = ''){
        $num = count($a);
        if(!$d){
            for($i = 0;$i < $num;$i++){
                for($j = 0;$j < $num-1;$j++){
                    if($a[$j][$sort] > $a[$j + 1][$sort]){
                        foreach ($a[$j] as $key => $temp){
                            $t = $a[$j + 1][$key];
                            $a[$j + 1][$key] = $a[$j][$key];
                            $a[$j][$key] = $t;
                        }
                    }
                }
            }
        }else{
            for($i = 0;$i < $num;$i++){
                for($j = 0;$j < $num-1;$j++){
                    if($a[$j][$sort] < $a[$j + 1][$sort]){
                        foreach ($a[$j] as $key => $temp){
                            $t = $a[$j + 1][$key];
                            $a[$j + 1][$key] = $a[$j][$key];
                            $a[$j][$key] = $t;
                        }
                    }
                }
            }
        }
        return $a;
    }
    function showExternalPic(){
        $wecha_id = $this -> _get('wecha_id');
        $token = $this -> _get('token');
        $imgData = S($token . '_' . $wecha_id);
        $types = array('gif' => 'image/gif', 'jpeg' => 'image/jpeg', 'jpg' => 'image/jpeg', 'jpe' => 'image/jpeg', 'png' => 'image/png',);
        if (!$imgData){
            $url = htmlspecialchars($_GET['url']);
            $dir = pathinfo($url);
            $host = $dir['dirname'];
            $refer = 'http://www.qq.com/';
            $ch = curl_init($url);
            curl_setopt ($ch, CURLOPT_REFERER, $refer);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
            $data = curl_exec($ch);
            curl_close($ch);
            $ext = strtolower(substr(strrchr($url, '.'), 1, 10));
            $ext = 'jpg';
            $type = $types[$ext] ? $types[$ext] : 'image/jpeg';
            header("Content-type: " . $type);
            echo $data;
        }else{
            $ext = 'jpg';
            $type = $types[$ext] ? $types[$ext] : 'image/jpeg';
            header("Content-type: " . $type);
            echo $imgData;
        }
    }
    public function send(){

     $this -> send_info($this -> _post('keyword'), $this -> _get('openid', 'htmlspecialchars'), session('userId'));
    	   
    }
    public function send_info($content, $openid, $pid = 1, $type = 1){
    	$data['pid'] = (int)$openid;
    	$data['keyword'] = $content;
    	$data['public_number_id'] = (int)1;
    	$data['openid'] = (string)$pid;
    	
    	$field['fields'] = json_encode($data);
    	$return=$this->getRpcData("serviceLogs/insert",$field);
    	echo 1;
    	exit;
        if($api['appid'] == false || $api['appsecret'] == false){
            $this -> error('必须先填写【AppId】【 AppSecret】');
            exit;
        }
        $url_get = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $api['appid'] . '&secret=' . $api['appsecret'];
        $json = json_decode($this -> curlGet($url_get));
        $qrcode_url = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=' . $json -> access_token;
        $data = '{
			"touser":"' . $openid . '",
			"msgtype":"text",
			"text":
			{
				 "content":"' . $content . '"
			}
		}';
        $post = $this -> api_notice_increment($qrcode_url, $data);
        $json_decode = json_decode($post);
        if ($checkRt){
        }else{
        }
        if($json_decode -> errmsg == 'ok'){
            echo 1;
        }else{
            echo 2;
        }
    }
    function api_notice_increment($url, $data){
        $ch = curl_init();
        $header = "Accept-Charset: utf-8";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $tmpInfo = curl_exec($ch);
        if ($errorno){
            return array('rt' => false, 'errorno' => $errorno);
        }else{
            return $tmpInfo;
            $js = json_decode($tmpInfo, 1);
            return $js['ticket'];
        }
    }
    function curlGet($url){
        $ch = curl_init();
        $header = "Accept-Charset: utf-8";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $temp = curl_exec($ch);
        return $temp;
    }
    //查看自己的信息记录
    public function Informationrecord(){
    	if(IS_AJAX){
    		$conf['loginName'] = $this -> _get('username', 'htmlspecialchars');
    	     $page = $_GET['page'];
    		 $rows = $_GET['rows'];
    		 $conf['startTime'] = $_GET['startTime'];
    		 $conf['endTime'] = $_GET['endTime'];
    		 
    		 if (!empty($conf['loginName'])){
    		 	$conf['loginName'] = $conf['loginName'];
    		 }
    		
    		 	
    		 //分页
    		 $conf['pageNo'] = !empty($page)?$page:1;
    		 $conf['pageSize'] = !empty($rows)?$rows:10;
    		 $conf['needTotalSize'] = "true";
    		 $conf['publicNumberId']=(int)1;
    		 $questlist = $this->getRpcData('serviceLogs/queryServiceLogsByLoginName', $conf);
    		 
    		 foreach($questlist['data']['records'] as $key=>$value){
    		 	$records[$key]['id'] = $value['id'];
    		 	$records[$key]['keyword'] = $value['keyword'];
    		 	$records[$key]['itemName'] = $value['itemName'];
    		 	$records[$key]['title'] = $value['title'];
    		 	$records[$key]['useNumber'] = $value['useNumber'];
    		 	$records[$key]['time'] = date('Y-m-d H:i:s',$value['startTime']/1000)."~".date('Y-m-d H:i:s',$value['endTime']/1000);
    		 	if ($value['status'] == "show"){
    		 		$records[$key]['status'] = "开启";
    		 	}
    		 	else{
    		 		$records[$key]['status'] = "关闭";
    		 	}
    		 	 
    		 }
    		 
    		 $data['total'] = $questlist['data']['totalSize'];
    		 $data['rows'] = $records;
    		 
    		 echo json_encode($data);
    		 
    		 
    		 
    	}
    }

}
?>
