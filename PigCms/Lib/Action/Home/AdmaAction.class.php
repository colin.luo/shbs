<?php
class AdmaAction extends BaseAction{
	//关注回复
	public function index(){
		
	    
		$params['token'] = (string)$_GET['token'];
		$companyInfo = $this->getRpcDataBase('company/getByToken',$params);
		
		$adma = $this->getRpcDataBase('mallAdma/get','',$companyInfo['data']);
		
		$isShare= 1;
        $shareTitle=$adma['data']['title'];
        $shareDesc=$adma['data']['info'];
        $shareIcon=spanResUrl($adma['data']['url']);
        $shareUrl= 'http://'.$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"];
	  
	    $shareDesc=str_replace(array("\r\n", "\r", "\n"),"",$shareDesc);
	    $this->assign("shareUrl",$shareUrl);
	    $this->assign("shareTitle",$shareTitle);
	    $this->assign("shareDesc",$shareDesc);
	    $this->assign("shareIcon",$shareIcon);
	    $this->assign("isShare",$isShare);
	    
	    $userAgentType = "weixin";
		$agentInfo =  $_SERVER['HTTP_USER_AGENT'];
		if (strstr($agentInfo, "Html5Plus")){
			$wecha_id = $_REQUEST['wecha_id'];
			$userAgentType = "html5App";
		}
		 $this->assign('moduleName', MODULE_NAME);
        $this->assign('actionName', ACTION_NAME);
	    $this->assign('userAgentType', $userAgentType);
		
		$adma['data']["url"] = spanResUrl($adma['data']["url"]);
		$this->assign('adma',$adma['data']);
		$this->display(C('DEFAULT_THEME') . ':Adma:' . ACTION_NAME);
	}

}
?>