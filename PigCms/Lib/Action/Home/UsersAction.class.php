<?php
class UsersAction extends BaseAction{
	public function index(){
		header("Location: /");
	}
	
	public function checklogin(){
		
		if (!isset($_GET['company']) || $_GET['company'] == ""){			
			$this->error('URL地址无效！');
		}
		
		if (session('companyCode')){
			session("companyCode", null);
		}
		session("companyCode", $_GET['company'],3600*48);
		cookie("companyCode", $_GET['company'],3600*72);
		

		$username =$this->_post('username');
		$password=$this->_post('password');
		
		$login_conf['username'] = $username; 
    $login_conf['password'] = $password;
    $login_conf['ip'] = $this->getclientip(0); 
    //var_dump($login_conf);
		$loginInfo = $this->getRpcDataBase('login/validate',$login_conf,session("companyCode"));
		
		//var_dump($loginInfo);exit;
		if ($loginInfo['errorCode'] == 2){
			$this->error('用户信息不存在！');exit;
		}
		else if ($loginInfo['errorCode'] == 3){
			$this->error('密码不正确！');exit;
		}
		else if ($loginInfo['errorCode'] == 4){
			$this->error('用户不是管理员角色');exit;
		}
		else if ($loginInfo['data']['personId'] <=0){
			$this->error('非法用户登录信息！');exit;
		}
		
		session('certificate',$loginInfo['data']['certificate'],3600*24);
		session('uid',$loginInfo['data']['personId'],3600*24);
		session('uname',$username,3600*24);
	
		session('firstOpen',1);
		session('diynum',0);
		session('connectnum',0);
		session('activitynum',0);

		session('gname',0);
		session('is_open_domain',$loginInfo['data']['is_open_domain']);
		$this->success('登录成功',U('User/Index/welcome'));
	}
	
	 public function logout() {
	 		$companyCode = session('companyCode');			
			session('uid',null);
			session('gid',null);
			session('uname',null);
			session('token',null);
			session('currDomainId',null);
			session("currDomainType",null);
			session("currDomainName",null);
			session('is_open_domain',null);
			session('domainList',null);
			unset($_SESSION[token]);
			unset($_SESSION[uid]);
			unset($_SESSION[uname]);
			$this->success('已经登出！',U('Home/Index/index',array('company'=>$companyCode)));       
   }
     
	function randStr($randLength){
		$randLength=intval($randLength);
		$chars='abcdefghjkmnpqrstuvwxyz';
		$len=strlen($chars);
		$randStr='';
		for ($i=0;$i<$randLength;$i++){
			$randStr.=$chars[rand(0,$len-1)];
		}
		return $randStr;
	}
  private function reg_ucenter() {
      $uc_db  = count(explode(',', C('ucenter_db_set')));
      $uc_web = count(explode(',', C('ucenter_web_set')));
      if ($uc_db != 6 || $uc_web != 3)return;

     $username = $this->_post('username');
     $password = $this->_post('password');
     $email    = $this->_post('email');
     $para = 'username='.$username.'&password='.$password.'&email='.$email;
     $res = file_get_contents("http://" .$_SERVER['SERVER_NAME']. "/UCenter/advanced/examples/ucexample_2.php?from_weixin_url=1&".$para);
     if (substr($res,-1) != 1)die('Ucenter Reg Error'.iconv('gbk', 'utf-8', $res));
     //exit;
  }


	public function sendMsg(){

		if(IS_POST){
			if (strlen($this->_post('mp'))!=11){
				exit('Error Phone Number!');
			}
			for($i=0;$i<6;$i++){
				$code .= rand(0,9);
			}

			session('verify',md5($code));
			session('reg_mp',md5($this->_post('mp')));
			//Sms::sendSms('admin','尊敬的客户，注册验证码是：'.$code.',我们的工作人员不会向您索取本条消息内容，切勿向任何人透漏',$this->_post('mp'));

			require('./PigCms/Lib/ORG/RestSMS.php');

			sendTempSMS($this->_post('mp'),array($code),"4764");//手机号码，替换内容数组，模板ID 4764
		
		}else{
			exit("Error!");
		}
	}

	
}