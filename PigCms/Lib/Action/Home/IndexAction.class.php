<?php
class IndexAction extends BaseAction
{
    public $includePath;
    private $hometheme;
    protected function _initialize()
    {
        parent::_initialize();	
        
      
				session('uid',null);
				session('gid',null);
				session('uname',null);
				session('currDomainId',null);
				session("currDomainType",null);
				session("currDomainName",null);
				session('is_open_domain',null);
				session('domainList',null);
				session('token',null);
				unset($_SESSION[token]);
				unset($_SESSION[uid]);
				unset($_SESSION[uname]);
				
				session_regenerate_id(true);
				
        if ($_GET['company']){
        	$this->hometheme = $_GET['company'];
        }
        else if(session('companyCode')){
        	$this->hometheme = session('companyCode');
        }
        else{
        	$this->hometheme = C('DEFAULT_THEME');
        }
      
        session("hometheme", $this->hometheme);
       
        $this->includePath = './tpl/Home/' . $this->hometheme . '/';
        $themeHead = $this->hometheme;
        if(!is_dir($_SERVER['DOCUMENT_ROOT'].$this->includePath)){
        	$this->includePath = './tpl/Home/default/';
        	$themeHead = "default";
        }
        $this->assign('themeHead', $themeHead);
        $this->assign('includeHeaderPath', $this->includePath . 'Public_header.html');
        $this->assign('includeFooterPath', $this->includePath . 'Public_footer.html');
    
    }
	
    public function index()    {
     	if($_GET['company']!=''){  
     		$themeDir =  './tpl/Home/' . $this->hometheme . '/';
     	
     		if(!is_dir($_SERVER['DOCUMENT_ROOT'].$themeDir)){
        	$this->display('default' . ':Index:' . ACTION_NAME);
        }
        else{     
        	
	        $this->display($this->hometheme . ':Index:' . ACTION_NAME);
	      }
      }
			else {
				$this->display('./tpl/Home/weportal/index.htm');
			}
    }
    
    // 用户登出
    public function logout()
    {
       $hometheme = session('hometheme');		
			
				session('uid',null);
				session('gid',null);
				session('uname',null);
				session('companyCode',null);
				session_destroy();
				unset($_SESSION[uid]);
				unset($_SESSION[uname]);
				unset($_SESSION);
				session('hometheme',$hometheme);
    		$this->error('已经登出！',U('Home/Index/index',array('company'=>$hometheme)));

    }
    
    public function verify()
    {
        Image::buildImageVerify(4, 1, 'png', 0, 28, 'verify');
    }
    public function verifyLogin()
    {
        Image::buildImageVerify(4, 1, 'png', 0, 28, 'loginverify');
    }
    
    public function resetpwd()
    {
        $uid = $this->_get('uid', 'intval');
        $code = $this->_get('code', 'trim');
        $rtime = $this->_get('resettime', 'intval');
        $info = M('Users')->find($uid);//|| $rtime < time()
       // print_r($info);
        //echo md5($info['id'] . $info['password'] . $info['email']);exit;
        if (md5($info['id'] . $info['password'] . $info['email']) !== $code ) {
            $this->error('非法操作', U('Index/index'));
        }
        $this->assign('uid', $uid);
        $this->display($this->home_theme . ':Index:' . ACTION_NAME);
    }

    public function help()
    {
       
        $this->display($this->home_theme . ':Index:' . ACTION_NAME);
    }
    
    public function think_encrypt($data, $key = '', $expire = 0)
    {
        $key = md5(empty($key) ? C('DATA_AUTH_KEY') : $key);
        $data = base64_encode($data);
        $x = 0;
        $len = strlen($data);
        $l = strlen($key);
        $char = '';
        for ($i = 0; $i < $len; $i++) {
            if ($x == $l) {
                $x = 0;
            }
            $char .= substr($key, $x, 1);
            $x++;
        }
        $str = sprintf('%010d', $expire ? $expire + time() : 0);
        for ($i = 0; $i < $len; $i++) {
            $str .= chr(ord(substr($data, $i, 1)) + ord(substr($char, $i, 1)) % 256);
        }
        return str_replace('=', '', base64_encode($str));
    }
    public function text()
    {
        $domain = $_GET['domain'];
        $domains = explode('.', $domain);
        echo '<a href="http://' . $domain . '/index.php?g=Home&m=T&a=test&n=' . $this->think_encrypt($domains[1] . '.' . $domains[2]) . '" target="_blank">http://' . $domain . '/index.php?g=Home&m=T&a=test&n=' . $this->think_encrypt($domains[1] . '.' . $domains[2]) . '</a><br>';
        echo '<a href="http://' . $domain . '/index.php?g=User&m=Create&a=index" target="_blank">http://' . $domain . '/index.php?g=User&m=Create&a=index</a><br>';
    }
    
    public function maintenance(){
    	$maintInfo = C('OPEN_PLATFORM_MAINTENANCE');
    	$this->assign('title', $maintInfo['title']);
    	$this->assign('content', $maintInfo['content']);
    	$this->assign('footer', $maintInfo['footer']);
    	$this->display();
    }
   
}
?>