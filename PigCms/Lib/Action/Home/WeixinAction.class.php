<?php
class WeixinAction extends Action{
	private $token;
	private $fun;
	private $data=array();
	public $fans;
	private $my='思创';
	public $wxuser;
	public $apiServer;
	public $siteUrl;
	public $user;
  public $companyCode;
  public $memberInfo;
  private $certificate;
  private $wecha_id;
  
  
	public function index(){
  
   	$token = $this->_get("token");
    \Log::write($_SERVER['REQUEST_URI'],'WARN');
    
    
		$companyInfo = $this->getRpcData('company/getByToken',array('token'=>$token));
    $this->companyCode = $companyInfo['data'];
   

		
		
		//\Log::write(json_encode($this->wxuser),'WARN');
		$this->siteUrl=C('site_url');
		if (!class_exists('SimpleXMLElement')){
			exit('SimpleXMLElement class not exist');
		}
		if (!function_exists('dom_import_simplexml')){
			exit('dom_import_simplexml function not exist');
		}
		
		$this->token=$this->_get('token',"htmlspecialchars");
		

		if(!preg_match("/^[0-9a-zA-Z]{3,42}$/",$this->token)){
			exit('error token');
		}
		
		$this->wxuser=S('wxuser_'.$this->token);
		if (!$this->wxuser||1){			
			 $result = $this->getRpcData('publicNumber/getByToken',array('token'=>$token));
			//\Log::write(json_encode($result),'WARN');
			S('wxuser_'.$this->token,$this->wxuser);
			$this->wxuser = $result["data"];
		}
		
		$weixin = new Wechat($this->token,$this->wxuser);	
		$maintInfo = C('OPEN_PLATFORM_MAINTENANCE');
		if ($maintInfo['open'] == 1){
			$title = $maintInfo['title'];
			$cover = $this->siteUrl."/tpl/static/notice.png";
			$url = $this->siteUrl.U("Home/Index/maintenance");	
			$weixin->response(array(array($title,"",$cover,$url)),"news");
			exit;
		}	
		$data = $weixin->request();
		$this->data = $data;
		$this->wecha_id	= $data['FromUserName'];
		
		\Log::write("Weixin MsgType=".$data['MsgType']."#Event=".$data['Event'],'WARN');
		
		//用户信息
		$member = $this->getRpcData('member/getByWid',array('wid'=>$data['FromUserName'])); 
		$this->memberInfo = $member['data'];
		
		$content  = $this->reply($data);
		//\Log::write(json_encode($content),'WARN');
		$weixin->response($content[0],$content[1]);
	}
	
	private function subscribeReply($type){
		
		$data['publicNumberId'] = $this->wxuser['id'];
	  $data['replyType'] = $type;
	      
		$List = $this->getRpcData("response/getResponse",$data); 
		//var_dump($List);
		$replyType = $List['data'][0]['type'];
		
		if ($replyType == '内容素材'){
      $contenArr = $List['data'][1]['content'];
      //var_dump($List);
      //var_dump($contenArr);
      if(count($contenArr)>0){
        $contentList = array();
        $i=0;
        foreach ($contenArr as $cont) {
        	if($i>=9) break;
          $tmpList = $this->getRpcData('content/get',array("id"=>$cont['content_id']));
          $contentList[] = $tmpList['data'];
          $i++;
        }
        
        $content = array();
        foreach ($contentList as $randk => $randv) {
        	$cover = $this->spanResUrlC($randv['cover']);//C('RES_URL').htmlspecialchars_decode(C('RES_URL').$randv['cover']);
          //$cover = C('RES_URL').$randv['cover'];
          $url = $this->siteUrl.U("Wap/Content/index",array("contentId"=>$randv['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
          $content[] = array($randv['title'],$randv['summary'],$cover, $url);
        }
        return array($content,"news");
      }
      else{
      	
       	return array("欢迎关注！！！","text");
      }
    }    
    else{
    	 if($List['data'][0]){
		    	\Log::write("#####subscribeReply:".json_encode($List),'WARN');
					return array(htmlspecialchars_decode($List['data'][0]['body']),"text");
			 }
			 else{
			 	return array("欢迎关注！！！","text");
			}
		}
	}
	
	private function customReply($data){
		$contentList = array();
		$url = "";
		$keyword = html_entity_decode($data['Content']);
		$type = "text"; //默认
		
		if ($keyword == '我的账号'){

			$url = U("Wap/Personcenter/index",array('wecha_id'=>$data['FromUserName'],'token'=>$_GET['token'])).'">'.$keyword.'</a>';
		}
		else if ($keyword == '我的通知'){
			$url = U("Wap/Personcenter/notice",array('wecha_id'=>$data['FromUserName'],'token'=>$_GET['token'])).'">'.$keyword.'</a>';
		}else if ($keyword == 'mine@myCentmine@'){
		    //用户信息
    		$member = $this->getRpcData('member/getByWid',array('wid'=>$data['FromUserName']));
    		$memberName=$member['data']['real_name'];
		    $param['memberId']=$member['data']['id'];
		    $centArr=$this->getRpcData('zzxMemberExtend/getCredits', $param);
		    if($centArr['errorCode']==0){
		        //先写死微信号名称，到时候看要不要打开
// 		        $result = $this->getRpcData('publicNumber/getByToken',array('token'=>$_GET['token']));
// 		        $wechaName=$result['data']['name'];
		        $sub=$centArr['data']['subject'];
		        $ele=$centArr['data']['elective'];
		        $date=date("m月d日",time());
		        $content ="亲爱的".$memberName."学员，截至".$date."，本年度您在“贵州农信网络学院”已获取如下学分：\n\n必修课学分：".$sub."分\n选修课学分：".$ele."分\n\n（温馨提示：选修课学分上限为20分，超出20分后的课件学习不予记分）";
		    }else{
		        $url = U("Wap/Index/login",array('wecha_id'=>$data['FromUserName'],'token'=>$_GET['token'])).'">身份验证</a>';
		        $content='无法获取到您的学分！点击进入<a href="'.$this->siteUrl.$url;
		    }
		    return array($content, $type);
		}
		else if ($keyword == '@mineproject@mine'){
				if ($this->memberInfo){
					$classlist = $this->getRpcData('project/all');
					foreach($classlist['data'] as $item){
						if ($item['id']){
							$title = "";
							$summary = "";
							$cover= "";
							$url= "";
							$projectInfo = $this->getRpcData('project/get',array("id"=>$item['id']));
							if ($projectInfo['data']){
								$title = $projectInfo['name'];
							$summary = $projectInfo['summary'];
							$cover= $projectInfo['cover'];
							$url= $this->siteUrl . U("Wap/Index/index",array("classid"=>$projectInfo['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
								$content[] = array($title,$summary,$cover,$url);
							}
						
						}
					
					}
					//$content[] = array($title,$summary,$cover,$url);

      		return array($content,"news");
				}
				
		}
	
		
		if ($url){
			
			$content = '点击进入<a href="'.$this->siteUrl.$url;
	
			return array($content, $type);
		}
		
		return $content;
		
	}
	
	private function reply($data){	
		
		//微信墙
		$content = $this->checkWinXinWall($data);
	  if ($content){
	  	return $content;
	  }

		//文本
		if ('text' == $data['MsgType']){				
			$content = $this->keywordAnswer($data);
		}
		else if ('event' == $data['MsgType']){  //事件
			
			if('subscribe' == $data['Event']){    //关注时应答
				
					return $this->subscribeReply('关注时应答');
			}
			else if('click' == strtolower($data['Event'])){   //单击菜单
				$data['Content']= $data['EventKey'];
				$this->data['Content'] = $data['EventKey'];
				$content = $this->keywordAnswer($data);
			}
			else if('view' == strtolower($data['Event'])){   //跳转链接菜单
				
				exit;
			}
			else if('location' == strtolower($data['Event'])){   //跳转链接菜单
				
				exit;
			}
			else{
				exit;
			}
			
		}
		
		//var_dump($content);
		//回答不上来		
		if (empty($content)){
			\Log::write("#####22".$data['FromUserName'],'WARN');
			return $this->subscribeReply('回答不上来的配置');			
		}
		
		return $content;
	}
	
	private function wallStr($acttype,$thisItem){
		$str='处理成功，您下面发送的所有文字将会显示在“'.$thisItem['title'].'”大屏幕上，如需退出微信墙模式，请输入“wx#quit”';
		if ($acttype==3){
			if ($thisItem['shake_id']){
				$str.="\r\n<a href=\"".$this->siteUrl."/index.php?g=Wap&m=Shake&a=index&id=".$thisItem['id']."&token=".$this->token."&act_type=".$acttype."&wecha_id=".$this->data['FromUserName']."\">点击这里参与摇一摇活动</a>";
			}
			if ($thisItem['vote_id']){
				$str.="\r\n\r\n<a href=\"".$this->siteUrl."/index.php?g=Wap&m=Scene_vote&a=index&id=".$thisItem['id']."&token=".$this->token."&act_type=".$acttype."&wecha_id=".$this->data['FromUserName']."\">点击这里参与投票</a>";
			}
		}
		return $str;
	}
	
	private function checkWinXinWall($data,$walInfo=""){
		if ($this->memberInfo){
			if(strstr(strtolower($data['Content']),'wx#open#') && $walInfo){
				$params['is_open_wall'] = 1; 
				$params['wall_id'] = $walInfo['data']['id']; 
				$fields['fields'] = json_encode($params); 
				$fields['id'] = $this->memberInfo['id'];
				$this->getRpcData('member/update',$fields);
				return array('您已进入微信墙对话模式，您下面发送的所有文字信息都将会显示在大屏幕上，如需退出微信墙模式，请输入“wx#quit”','text');
			}
			elseif(strtolower($data['Content'])=='wx#quit'){
				$params['is_open_wall'] = 0; 
				$params['wall_id'] = 0; 
				$fields['fields'] = json_encode($params); 
				$fields['id'] = $this->memberInfo['id'];
				$this->getRpcData('member/update',$fields);
				return array('成功退出微信墙对话模式','text');
			}
		}
		
		if ($this->memberInfo['is_open_wall'] && !strstr(strtolower($data['Content']),'wx#open#')){//更新之后wallopen等于1的进入这里
			$thisItem = array();
			$wallInfo = $this->getRpcData('gameWall/get',array('id'=>$this->memberInfo['wall_id']));
			$thisItem = $wallInfo['data'];
			
			if (!$thisItem){
				return array('微信墙活动不存在,如需退出微信墙模式，请输入“wx#quit”','text');				
			}
		
			//$thisItemdata = $this->getRpcData("gameWallMessage/all");
			//$type = $thisItemdata['data']['records'];
			//for ($i=0; $i <count($type) ; $i++) { 
				//if($type[$i]['isOpen'] == 0){
					//$j = $j + 1;
					//$thisItem[$j] = $type[$i]; 
				//}
			//}
			
			$row=array();
			if ('image' != $data['MsgType']){
				$message=str_replace('wx#','',$data['Content']);
				$row['content']=$message;
			}else {
				$message='';
				$row['picture']=$data['PicUrl'];
			}
			
			$acttype =1;
			$needCheck = 0;
			$row['wall_id']=intval($thisItem['id']);
			$row['uid']=$this->memberInfo['id'];
			$row['member_id']=$this->memberInfo['id'];
			$row['wecha_id']=$data['FromUserName'];
			$row['token']=$this->token;
			
			$row['time']= date('Y-m-d H:i:s', time());
			$row['check_time']=$row['time'];
		
			if ($acttype==3){
				$row['is_scene']=true;
			}else {
				$row['is_scene']=false;
			}
			$row['is_check']=true;
			if ($needCheck){
				$row['is_check']=false;
			}
					
			$field['field'] = json_encode($row);
	 		$result = $this->getRpcData('gameWallMessage/inserMessage',$field);	
	 		
	 		//$thisItemdata = $this->getRpcData("gameWallMessage/all");
		
				
				$str=$this->wallStr($acttype,$thisItem);
				return array($str,'text');
		
		}
		
			return "";
	}
	
	/**
	 * 关键词应答
	 * @return [type] [description]
	 */
	private function keywordAnswer($data){
		//自定义回复
		$content = $this->customReply($data);
		if (!empty($content)){
			return $content;
		}
		
		if(!empty($data['Content'])){
			if(strstr(strtolower($data['Content']),'wx#open#')){
				$keyword =  str_replace("wx#open#", "",$data['Content']);
			}
			else{
				$keyword =  $data['Content'];
			}
			$math['memberId'] = $this->memberInfo['id'];
      $math['keyword'] = html_entity_decode($keyword);
      $math['keyword'] = str_replace("&", "&amp;",$math['keyword']);
      $math['publicNumberId'] = $this->wxuser['id'];
      $math['wid'] = $data['FromUserName'];
      $return = $this->getRpcData('keywordResponse/matchAll',$math);
      \Log::write("#####keyword:".$math['keyword'],'WARN');
      \Log::write("#####param:".json_encode($math),'WARN');
      \Log::write("#####result:".json_encode($return),'WARN'); 	
     	
     	//作弊不做其它处理
     	if(!(strpos($data['Content'], 'cheat') === FALSE)){
     		
     		exit;
     	}
     	
       $returnList = $return['data'];
       if ($returnList['register'] && $returnList['register'][0] == 1){
					$linkstr = '<a href="'.$this->siteUrl.U("Wap/Index/login",array('wecha_id'=>$data['FromUserName'],'token'=>$_GET['token'])).'">点击进入绑定</a>';
					//$linkstr = '抱歉，您无权访问<a href="'.$this->siteUrl.U("Wap/Index/login",array('wecha_id'=>$data['FromUserName'],'token'=>$_GET['token'])).'">点击进入绑定</a>';
					return array($linkstr,"text");
				}
		
      //-------------
       $contentList = array();
       //...内容学习
       $i = 0;
       $contentIds = $returnList['content'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
	       	 $tmpList = $this->getRpcData('content/get',array("id"=>$contentV));
	       	 $tmpList['data']['linktype'] = 'content';
	       	 $contentList[] = $tmpList['data'];
	       	 $i++;
       	 } 
       }
       
        //添加班级
       $contentIds = $returnList['joinProject'];
       if(count($contentIds)>0){
	       	$project_conf['memberId'] = $this->memberInfo['id'];
	       	$project_conf['publicNumberId'] = $this->wxuser['id'];
	       	$project_conf['keyword'] = html_entity_decode($data['Content']);
	       	$result = $this->getRpcData('project/addByCode', $project_conf);
	       	if ($result['data'] > 0){
	       		return array( "恭喜您，添加班级成功！","text");
	       	}
	       	else{
	       		return array( $result['errorMassage'],"text");
	       	}
       	       	 
       }
       
        //查找班级
       $contentIds = $returnList['project'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
	       	 $tmpList = $this->getRpcData('project/get',array("id"=>$contentV));
	       	 $tmpList['data']['linktype'] = 'project';
	       	 $contentList[] = $tmpList['data'];
	       	 $i++;
       	 }       	 
       }
       //Portal
       $contentIds = $returnList['portal_set'];
       if(count($contentIds)>0){
       	$return = $this->getRpcData('portalSet/get');
		$time = time();
		$startTime = $return['data']['startTime']/1000;
		$endTime = $return['data']['endTime']/1000;
		$math['memberId'] = $this->memberInfo['id'];
		$take = $this->getRpcData('portalMemberReference/isIn',$math);
		if($take['data'] && $endTime > $time  && $startTime < $time){
	       	 foreach ($contentIds as $contentK => $contentV) {
	       	 if($i>=9) break;
		       	 $tmpList = $this->getRpcData('portalSet/get',array("id"=>$contentV));
		       	 $tmpList['data']['linktype'] = 'portalSet';
		       	 $contentList[] = $tmpList['data'];
		       	 $i++;
	       	 }  
		}
       }
       //汇报
       $contentIds = $returnList['evaluation_report'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
	       	 $tmpList = $this->getRpcData('evaluationReport/get',array("id"=>$contentV));
	       	 $tmpList['data']['linktype'] = 'evaluation_report';
	       	 $contentList[] = $tmpList['data'];
	       	 $i++;
       	 }       	 
       }
       
       //猜图游戏
       $contentIds = $returnList['game_guess'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
	       	 $tmpList = $this->getRpcData('gameGuess/get',array("id"=>$contentV));
	       	 $tmpList['data']['linktype'] = 'game_guess';
	       	 $contentList[] = $tmpList['data'];
	       	 $i++;
       	 }       	 
       }
 /*    
大转盘(gameTurn)，     
优惠券(gameCoupon)  
刮刮卡(gameScratch)，    
水果机(gameFruits)，    
砸金蛋(gameEggs)

基本游戏(gameBase)
祝福贺卡(gameGreet)
摇一摇(gameShake)
微信墙(gameWall)
拆图游戏是 gameGuess
   */        
       //祝福贺卡
       $contentIds = $returnList['game_greeting_card'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
	       	 $tmpList = $this->getRpcData('gameGreet/get',array("id"=>$contentV));
	       	 $tmpList['data']['linktype'] = 'game_greeting_card';
	       	 $contentList[] = $tmpList['data'];
	       	 $i++;
       	 }       	 
       }
       //摇一摇
       $contentIds = $returnList['game_shake'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
	       	 $tmpList = $this->getRpcData('gameShake/get',array("id"=>$contentV));
	       	 $tmpList['data']['linktype'] = 'game_shake';
	       	 $contentList[] = $tmpList['data'];
	       	 $i++;
       	 }       	 
       }
       //供应商评价系统
        $contentIds = $returnList['supcmt_set'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
	       	 $tmpList = $this->getRpcData('supcmtSet/get',array("id"=>$contentV));
	       	 $tmpList['data']['linktype'] = 'supcmt_set';
	       	 $contentList[] = $tmpList['data'];
	       	 $i++;
       	 }       	 
       }
           $contentIds = $returnList['supcmt_rate_res'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
	       	 $tmpList = $this->getRpcData('supcmtRateRes/get',array("id"=>$contentV));
	       	 $tmpList['data']['linktype'] = 'supcmt_rate_res';
	       	 $contentList[] = $tmpList['data'];
	       	 $i++;
       	 }       	 
       }
       //微信墙
       $contentIds = $returnList['game_wall'];
       if(count($contentIds)>0){
       	
       		
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
	       	 $tmpList = $this->getRpcData('gameWall/get',array("id"=>$contentV));
	       	 if ($tmpList['errorCode'] == 0 && $tmpList['data']){
	       	 	
	       	 	$haveC['id'] = $this->memberInfo['id'];
				 	 	$haveC['resourceType'] = "game_wall";
				 	 	$haveC['resourceId'] = (int)$contentV;
				  	$haveR = $this->getRpcData('member/haveRight',$haveC);
					
					
						if ($haveR['errorCode'] != 0 && $haveR['errorCode'] != 13){
							return array( $haveR['errorMassage'],"text");
							exit;	
						}
					
		       	 $wallMsg = $this->checkWinXinWall($data, $tmpList);
		       	 return $wallMsg;		       	 
		       }
	       	 
       	 }       	 
       }
       
       //基本游戏
       $contentIds = $returnList['game_base'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
	       	 $tmpList = $this->getRpcData('gameBase/get',array("id"=>$contentV));
	       	 $tmpList['data']['linktype'] = $tmpList['data']['type'];
	       	 $contentList[] = $tmpList['data'];
	       	 $i++;
       	 }       	 
       }
       
       //...活动      
       $contentIds = $returnList['active'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $acctiveK => $acctiveV) {
       	 if($i>=9) break;
       	 $tmpList = $this->getRpcData('active/get',array("id"=>$acctiveV));
       	 $tmpList['data']['linktype'] = 'active';
       	 $contentList[] = $tmpList['data'];
       	 $i++;
       	 }
       }
       
       //...论坛   
       $contentIds = $returnList['forum'];
       
       if(count($contentIds)>0){
       	 foreach ($contentIds as $forumK => $forumV) {
	       	 if($i>=9) break;
	       	 $forumC['publicNumberId'] = $this->wxuser['id'];
	       	 $tmpList = $this->getRpcData('forum/getByPublicNumberId', $forumC);
	       	  
	       	 if ($forumV == $tmpList['data']['id']){
			       	 $tmpList['data']['linktype'] = 'forum';
			       	 $contentList[] = $tmpList['data'];
			       	 $i++;
		       }
       	 }
       }
       
       //...作业       
       $contentIds = $returnList['homework'];
       if(count($contentIds)>0){       	
       	 foreach ($contentIds as $homeK => $homeV) {
       	 	if($i>=9) break;
       	 $tmpList = $this->getRpcData('homework/get',array("id"=>$homeV));
       	 $tmpList['data']['linktype'] = 'homework';
       	 $contentList[] = $tmpList['data'];
       	$i++; 
       	}
       }
       //...签到      
       $contentIds = $returnList['attendance'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $attenK => $attenV) {
       	 	if($i>=9) break;
       	 $tmpList = $this->getRpcData('attendance/get',array("id"=>$attenV));
       	 $tmpList['data']['linktype'] = 'attendance';
       	 $contentList[] = $tmpList['data'];
       	 $i++;
       	 }
       }
       //...调研       
       $contentIds = $returnList['research'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $researchK => $researchV) {
       	 	if($i>=9) break;
       	 $tmpList = $this->getRpcData('research/get',array("id"=>$researchV));
       	 $tmpList['data']['linktype'] = 'research';
       	 $contentList[] = $tmpList['data'];
       	 $i++;
       	 }
       }
       //...考试
     
       $contentIds = $returnList['exam_info'];
       if(count($contentIds)>0){
       	 $isExam = 0;   
			
       	 foreach ($contentIds as $examK => $examV) {
	       	 	if($i>=9) break;
		       	 	 //$examconf['memberId'] =$this->memberInfo['id'];
			       	 $examconf['id'] = $examV;
							 $tmpList = $this->getRpcData('examArrange/get',$examconf);
							//var_dump($tmpList);
			       	 $tmpList['data']['linktype'] = 'exam_info';
			       	 $contentList[] = $tmpList['data'];
			       	 $i++;	
			       	 $isExam = 1;		
       	 }
       }
       
      
       //...关键词回复1     
       $contentIds = $returnList['keyword_mp3'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $keywordK => $keywordV) {
       	 if($i>=9) break;
       	 $tmpList = $this->getRpcData('keywordResponse/get',array("id"=>$keywordV));
       	 $tmpList['data']['linktype'] = 'keyword_mp3';
       	 return array (								
								array (
										
										$tmpList['data'][0]['keyword'],
										
										$tmpList['data'][0]['description'],
										
										$tmpList['data'][0]['audio_url'],
										
										$tmpList['data'][0]['audio_url']
										),
								'music' 
								);
   	 $contentList[] = $tmpList['data'];
       	 $i++;
       	 }
       }
        
       //...关键词回复2      
       $contentIds = $returnList['keyword_textAndLink'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $keyword2K => $keyword2V) {
						if($i>=9) break;
						$resp = $this->getRpcData('keywordResponse/get',array("id"=>$keyword2V));
						if ($resp['data'][0]['type'] == 'textAndLink'){
							return array( htmlspecialchars_decode($resp['data'][0]['body']),"text");
						}
						else if ($resp['data'][0]['type'] == 'content'){
							foreach($resp['data'][1]['content'] as $content_item){
								 $tmpList = $this->getRpcData('content/get',array("id"=>$content_item['content_id']));
				       	 $tmpList['data']['linktype'] = 'content';
				       	 $contentList[] = $tmpList['data'];
			       	 	$i++;
			       	}
	       	 	}
       	 }
       }
       
       $contentIds = $returnList['keyword_content'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $keyword2K => $keyword2V) {
						if($i>=9) break;
						$resp = $this->getRpcData('keywordResponse/get',array("id"=>$keyword2V));
						if ($resp['data'][0]['type'] == 'textAndLink'){
							return array( $resp['data'][0]['body'],"text");
						}
						else if ($resp['data'][0]['type'] == 'content'){
							foreach($resp['data'][1]['content'] as $content_item){
								if($i>=9) break;
								 $tmpList = $this->getRpcData('content/get',array("id"=>$content_item['content_id']));
				       	 $tmpList['data']['linktype'] = 'content';
				       	 $contentList[] = $tmpList['data'];
			       	 	$i++;
			       	}
	       	 	}
       	 }
       }
       
        //周周学
       $contentIds = $returnList['zzx_week_plan'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
	       	 $tmpList = $this->getRpcData('zzxWeekPlan/get',array("id"=>$contentV));
	       	 $tmpList['data']['linktype'] = 'zzx_week_plan';
	       	 $contentList[] = $tmpList['data'];
	       	 $i++;
       	 }       	 
       }
       
        //学院
       $contentIds = $returnList['college'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
	       	 $tmpList = $this->getRpcData('college/get',array("id"=>$contentV));
	       	 $tmpList['data']['linktype'] = 'college';
	       	 $contentList[] = $tmpList['data'];
	       	 $i++;
       	 }       	 
       }
       
       //相册
       $contentIds = $returnList['mall_photo_album'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
	       	 $tmpList = $this->getRpcData('mallPhotoAlbum/get');
	       	 
	       	 $tmpList['data']['obj']['linktype'] = 'mall_photo_album';
	       	 $contentList[] = $tmpList['data']['obj'];
	       	 $i++;
       	 }       	 
       }
       
        //商城
       $contentIds = $returnList['mall_reply_info'];
       if(count($contentIds)>0){
       	 foreach ($contentIds as $contentK => $contentV) {
       	 if($i>=9) break;
       	 	 $tmpList = $this->getRpcData("mallReplyInfo/get");	
	       	 $tmpList['data']['linktype'] = 'mall_reply_info';
	       	 $contentList[] = $tmpList['data'];
	       	 $i++;
       	 }       	 
       }
       
       $contentList = array_filter($contentList);       
       if(empty($contentList)){
       	 return false;
       }
       
      $maxLength = count($contentList)>9?9:count($contentList);
      $randKey = array_rand($contentList,$maxLength);
			//$url = $this->siteUrl;
      foreach ($contentList as $key => $item) {  
      	$url = $this->siteUrl;
	      $cover = $this->spanResUrlC($item['cover']);
	      $title = htmlspecialchars_decode($item['title']);
	      $summary = htmlspecialchars_decode($item['summary']);
	      if ($item['linktype'] == 'content'){
			     $url .= U("Wap/Content/index",array("contentId"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'active'){
		       $url .= U("Wap/Activity/index",array("activeId"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }	    
		    else if ($item['linktype'] == 'supcmt_set'){
		    	if($item['status'] =='show'){
		       		$url .= U("Wap/Supplier/index",array('wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));	       
		         	$cover = $this->spanResUrlC($item['pic']);
		         	$title = htmlspecialchars_decode($item['keyword']);
		        }
		    }
		    else if($item['linktype'] == 'portalSet'){
		    	if($item['status'] =='show'){
		       		$url .= U("Wap/Portal/index",array('wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));	       
		         	$cover = $this->spanResUrlC($item['pic']);
		         	$title = htmlspecialchars_decode($item['title']);
		        }		    	
		    }
		    else if ($item['linktype'] == 'attendance'){
		      $url .= U("Wap/Sign/index",array("attendanceId"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'homework'){
		      $url .= U("Wap/Homework/index",array("homeworkId"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'research'){
		      $url .= U("Wap/Research/index",array("researchId"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'project'){
		    	$title = $item['name'];
		      $url .= U("Wap/Index/index",array("classid"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'exam_info'){		    	
		    	$title = htmlspecialchars_decode($item['examName']);	    	
	        $summary = htmlspecialchars_decode($item['examExplain']);
		    	$cover = $this->spanResUrlC($item['picturePath']);//htmlspecialchars_decode(C('RES_URL').$item['pic']);
		      $url .= U("Wap/Exam/index",array('examId'=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'evaluation_report'){ 		    	
		      $content = '点击进入<a href="'.$url.U("Wap/EvaluationReport/getReportByMember",array("id"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token'])).'">'.$title.'</a>';
		    	return array($content,"text");
		    	exit;
		    }else if ($item['linktype'] == 'supcmt_rate_res'){ 		    	
		      $content = '点击进入<a href="'.$url.U("Wap/Supplier/share",array("id"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token'])).'">'.$item['keyword'].'</a>';
		    	return array($content,"text");
		    	exit;
		    }		    
		    else if ($item['linktype'] == 'forum'){
		    	$title = htmlspecialchars_decode($item['name']);
	        $summary = htmlspecialchars_decode($item['intro']);
		    	$cover = $this->spanResUrlC($item['pic_url']);//htmlspecialchars_decode( C('RES_URL').$item['pic_url']);
		      $url .= U("Wap/Forum/index",array("id"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'game_guess'){
		    	$cover = $this->spanResUrlC($item['pic']);//htmlspecialchars_decode( C('RES_URL').$item['pic']);
		      $url .= U("Wap/Caitu/index",array("gameGuessId"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }		    
		    else if ($item['linktype'] == 'game_greeting_card'){
	        $summary = htmlspecialchars_decode($item['title']);
		    	$cover = $this->spanResUrlC($item['picurl']);//htmlspecialchars_decode( C('RES_URL').$item['picurl']);
		      $url .= U("Wap/Greeting_card/index",array("id"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'game_shake'){
	        $summary = htmlspecialchars_decode($item['title']);
		    	$cover = $this->spanResUrlC($item['thumb']);//htmlspecialchars_decode( C('RES_URL').$item['thumb']);
		      $url .= U("Wap/Shake/index",array("id"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'game_wall'){
	        $summary = htmlspecialchars_decode($item['title']);
		    	$cover = $this->spanResUrlC($item['logo']);//htmlspecialchars_decode( C('RES_URL').$item['logo']);
		      $url .= U("Wap/Wall/index",array("id"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'scratch'){
	        $summary = htmlspecialchars_decode(htmlspecialchars_decode($item['info']));
		    	$cover = $this->spanResUrlC($item['startPic']);//htmlspecialchars_decode( C('RES_URL').$item['startPic']);
		      $url .= U("Wap/Guajiang/index",array("id"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    
		    else if ($item['linktype'] == 'turn'){
	        $summary = htmlspecialchars_decode(htmlspecialchars_decode($item['info']));
		    	$cover = $this->spanResUrlC($item['startPic']);//htmlspecialchars_decode( C('RES_URL').$item['startPic']);
		      $url .= U("Wap/Lottery/index",array("id"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'coupon'){
	        $summary = htmlspecialchars_decode(htmlspecialchars_decode($item['info']));
		    	$cover = $this->spanResUrlC($item['startPic']);//htmlspecialchars_decode( C('RES_URL').$item['startPic']);
		      $url .= U("Wap/Coupon/index",array("id"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'fruits'){
	        $summary = htmlspecialchars_decode(htmlspecialchars_decode($item['info']));
		    	$cover = $this->spanResUrlC($item['startPic']);//htmlspecialchars_decode( C('RES_URL').$item['startPic']);
		      $url .= U("Wap/LuckyFruit/index",array("id"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'eggs'){
	        $summary = htmlspecialchars_decode(htmlspecialchars_decode($item['info']));
		    	$cover = $this->spanResUrlC($item['startPic']);//htmlspecialchars_decode( C('RES_URL').$item['startPic']);
		      $url .= U("Wap/GoldenEgg/index",array("id"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'zzx_week_plan'){
		    	$title = htmlspecialchars_decode($item['weekName']);
	        $summary = htmlspecialchars_decode($item['descInfo']);
		    	$cover = $this->spanResUrlC($item['cover']);//htmlspecialchars_decode( C('RES_URL').$item['cover']);
		      $url .= U("Wap/WeekPlan/index",array("weekPlanId"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'college'){
		    	$summary = htmlspecialchars_decode($item['info']);
		  		$cover = $this->spanResUrlC($item['picurl']);//htmlspecialchars_decode( C('RES_URL').$item['picurl']);

		      $url .= U("Wap/College/index",array("collegeId"=>$item['id'],'wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }//mall_reply_info
		    else if ($item['linktype'] == 'mall_photo_album'){
		    	$summary = htmlspecialchars_decode($item['info']);
		  		$cover = $this->spanResUrlC($item['picurl']);//htmlspecialchars_decode( C('RES_URL').$item['picurl']);

		      $url .= U("Wap/Photo/index",array('wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		    else if ($item['linktype'] == 'mall_reply_info'){
		    	$summary = htmlspecialchars_decode($item['info']);
		  		$cover = $this->spanResUrlC($item['picurl']);//htmlspecialchars_decode( C('RES_URL').$item['picurl']);

		      $url .= U("Wap/Store/index",array('wecha_id'=>$data['FromUserName'],'token'=>$_GET['token']));
		    }
		   
	      $content[] = array($title,$summary,$cover,$url);
      }
      //var_dump($contentList);
      return array($content,"news");
    }
   
    return '';
	}
	
		
	private function curlGet($url,$method='get',$data=''){
		$ch = curl_init();
		$header = "Accept-Charset: utf-8";
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$temp = curl_exec($ch);
		return $temp;
	}
	private function get_tags($title,$num=10){
		vendor('Pscws.Pscws4', '', '.class.php');
		$pscws = new PSCWS4();
		$pscws->set_dict(CONF_PATH . 'etc/dict.utf8.xdb');
		$pscws->set_rule(CONF_PATH . 'etc/rules.utf8.ini');
		$pscws->set_ignore(true);
		$pscws->send_text($title);
		$words = $pscws->get_tops($num);
		$pscws->close();
		$tags=array();
		foreach ($words as $val) {
			$tags[] = $val['word'];
		}
		return implode(',',$tags);
	}
	
	

  
	
	public function getclientip($type = 0,$adv=false) {
	    $type       =  $type ? 1 : 0;
	    static $ip  =   NULL;
	    if ($ip !== NULL) return $ip[$type];
	    if($adv){
	        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
	            $ip     =   $_SERVER['HTTP_CLIENT_IP'];
	        }elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	            $arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
	            $pos    =   array_search('unknown',$arr);
	            if(false !== $pos) unset($arr[$pos]);
	            $ip     =   trim($arr[0]);
	        }elseif (isset($_SERVER['REMOTE_ADDR'])) {
	            $ip     =   $_SERVER['REMOTE_ADDR'];
	        }
	    }elseif (isset($_SERVER['REMOTE_ADDR'])) {
	        $ip     =   $_SERVER['REMOTE_ADDR'];
	    }
	    // IP地址合法验证
	    $long = sprintf("%u",ip2long($ip));
	    $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
	    return $ip[$type];
	}
	
	private function spanResUrlC($url){
    
	$url = htmlspecialchars_decode($url);
	$newUrl = str_replace('RES_URL',C('RES_URL'), $url);

		$urlhttp = strtolower(substr($newUrl,0,7));
		$urlhttps = strtolower(substr($newUrl,0,8));
		
		if($urlhttp != 'http://' && $urlhttps != 'https://') {
			$newUrl = C('RES_URL').$newUrl;

		}
		return $newUrl;
	}
	
	private function getRpcData($path,$param,$companyCode =''){	
		
			 $certificate = cookie('certificate' . $this->wecha_id);	
			 	 	
			if (!$companyCode){
				$companyCode = $this->companyCode;
			}
				//证书已过期
			 if (!$certificate){
			 	
			 		if ($this->memberInfo['certificate']){//从member/getByWid证书
			 				
			 			cookie('certificate'.$this->wecha_id,$this->fans['certificate'],3600*24);
			 		}
			 		else{ //从login/connectValidate证书
			 			 if ($path != 'company/getByToken'){
				 			$this->getCertificate($companyCode);
				 		}
			 		}
			 		$certificate = cookie('certificate' . $this->wecha_id);	
			 }
		
			
			$result = $this->getRpcDataBase($path,$param,$companyCode,$certificate);

			if ($result == -10001){
				
				//重新获取login/connectValidate证书
				$this->getCertificate($companyCode);
				
				$certificate = cookie('certificate' . $this->wecha_id);	
				
				$result = $this->getRpcDataBase($path,$param,$companyCode,$certificate);				
				//如果重新获取证书还是失败，则跳转到绑定页面
				if ($result == -10001){
					$this->redirect(U('Wap/Index/login',array('token'=>$this->token,'wecha_id'=>$this->wecha_id)));		
				}
			}
			
			return $result;
	}
	
	private function getCertificate($companyCode){
		//获取签名证书
		$login_conf['wid'] = $this->wecha_id; 
    $login_conf['ip'] = $this->getclientip(0);
    $login_conf['appId'] = 'welearning_wechat'; 
    $login_conf['appSecret'] = 'learn2strong';
     
		$isLogin = $this->getRpcDataBase('login/connectValidate',$login_conf, $companyCode);
		
		if ($isLogin['errorCode'] == 0 && $isLogin['data']['certificate']){
			cookie('certificate'.$this->wecha_id,$isLogin['data']['certificate'],3600*24);
		}			
	}
	
	public function getRpcDataBase($path,$param,$companyCode,$certificate="", $json=true){		
		$url =C('INTERFACE_URL').$path;
	 
		if ($path != "company/getByToken" && $companyCode !== 'welearning'){
			//if ($companyCode){
				//$companyCode = session('companyCode');
			//}		
			$param['companyCode'] = $companyCode;
			if ($param['companyCode'] == ""){
				$param['companyCode'] = "gznxbank";
				\Log::write("compnayCode is null!".$path,'WARN');				
			}
		}		
		
		if ($certificate){
		   if (C('OPEN_CERTIFICATE') ){
			  $param['certificate']= $certificate;
		   }
		}
		
		$timeout = 300;
		if ($path == 'project/getExcelContentExam'){
			$timeout = 3600;
		}
		
		$data = http_build_query($param); 		
		$opts = array(  
			'http'=>array(  
			'method'=>"POST",
			'timeout'=>$timeout,  
			'header'=>"Content-type: application/x-www-form-urlencoded\r\n",
			"Content-length:".strlen($data)."\r\n",  
			"Cookie: foo=bar\r\n" . "\r\n",  
			'content' => $data,  
			)  
		); 	
		
		$start_time = time();
		$start_time = time();
		$cxContext = stream_context_create($opts);  							
		$result = file_get_contents($url, false, $cxContext); 
		$resultData = json_decode($result,true);
		
		//统计耗时接口
		$end_time = time();
		$length_time = $end_time - $start_time;
		if (C('OPEN_API_LOG') ){			
			$log = "API:".$path."#TIMELENGHT=".$length_time."#ERRORCODE=".$resultData['errorCode']."#ERRORMSG=".$resultData['errorMassage'];
			if (C('OPEN_API_DETAIL_LOG') && $path != 'courseStudyProcess/updateContentTimes'){
				
				$log .="\r\n#PARAMS=".json_encode($param)."\r\n#RESULT=".json_encode($resultData['data']);
			}			
			\Log::write($log,'WARN');		
		}
        /*
        * 11 证书正确
        * 12 证书错误    (证书无法解析)
        * 13 没有证书    (证书参数为空)
        * 14 证书过期    (有效时间为24小时)
        * 15 证书信息不一致     （传递companyCode和证书所保存的company信息不一致）
        */

		if ($resultData['errorCode'] >11 && $resultData['errorCode'] <= 15){			
			\Log::write("[".session('certificate')."]certificate failure,API=".$path."#errorCode=".$resultData['errorCode'],'WARN');
			return -10001;
		}
		
		if ( $path == "content/getBody"
		 	|| $path=='forumComment/getBody' 
		  || $path=='contentTemplate/getBody'
		  || $path=='forumTopic/getBody'
		  || $path=='publicNumber/getBody' 
		  || $path == 'noticePcSend/getContent'
		  || $path=='notice/getBody'
		  || $path=='mallProduct/getProductIntro'){	
	  		
			
			return $result;	
		}

	 	if ($json){
			return json_decode($result,true);		
		}
		else{
			return $resultData;
		}
	}
}//end class
