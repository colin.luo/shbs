<?php 
class WechatAddr 
{
	private $appId		= '';
	private $appSecret	= '';
	private $url 		= '';
	private $token = '';
	private $isQy = 0;
	
	//构造函数获取access_token
	function __construct($appId,$appSecret,$token,$publicType =''){
		$this->appId		= $appId;
		$this->appSecret	= $appSecret;
		$this->token = $token;
		if ($publicType == '企业号'){
			$this->isQy = 1;
		}
	}

	public function getSignPackage() {
    $jsapiTicket = $this->getJsApiTicket();
		
    // 注意 URL 一定要动态获取，不能 hardcode.
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    $timestamp = time();
    $nonceStr = $this->createNonceStr();

    // 这里参数的顺序要按照 key 值 ASCII 码升序排序
    $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";

    $signature = sha1($string);

    $signPackage = array(
      "appId"     => $this->appId,
      "nonceStr"  => $nonceStr,
      "timestamp" => $timestamp,
      "url"       => $url,
      "signature" => $signature,
      "rawString" => $string
    );
    return $signPackage; 
  }

  private function createNonceStr($length = 16) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = "";
    for ($i = 0; $i < $length; $i++) {
      $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }
    return $str;
  }

  private function getJsApiTicket() {
    // jsapi_ticket 应该全局存储与更新，以下代码以写入到文件中做示例
 
      $accessToken = $this->getAccessToken();
	  $jsapi_ticket_info = S('jsapi_ticket_'.$this->token);
			 
      if (!$jsapi_ticket_info || $jsapi_ticket_info->expire_time < time()) {
	      // 如果是企业号用以下 URL 获取 ticket
	      if ($this->isQy){
		      $url = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=$accessToken";
		  }else{
		      $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
		  }
	      $res = json_decode($this->httpGet($url));
	      $ticket = $res->ticket;
	      if ($res->errcode==0) {
    	      $jsapi_ticket_info->expire_time = time() + 7000;
    	      $jsapi_ticket_info->jsapi_ticket = $ticket;
		      S('jsapi_ticket_'.$this->token,$jsapi_ticket_info, 6900);	
	      }else if($res->errcode==40001){
	          $accessToken=$this->recreateAccessToken();
	          if ($this->isQy)
	              $url = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=$accessToken";
	          else
	              $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
	          $res = json_decode($this->httpGet($url));
	          $ticket = $res->ticket;
	          $jsapi_ticket_info->expire_time = time() + 7000;
	          $jsapi_ticket_info->jsapi_ticket = $ticket;
	          S('jsapi_ticket_'.$this->token,$jsapi_ticket_info, 6900);
	      }
		  $jsapi_ticket_info = S('jsapi_ticket_'.$this->token);
     }
     return $jsapi_ticket_info->jsapi_ticket;
  }

  public function getAccessToken() {
    // access_token 应该全局存储与更新，以下代码以写入到文件中做示例
    $access_token_info = S('access_token_'.$this->token);

    if (!$access_token_info || $access_token_info->expire_time < time()) {
    	
      // 如果是企业号用以下URL获取access_token
      if ($this->isQy){
	      $url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$this->appId&corpsecret=$this->appSecret";
	    }
	    else{
	      $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$this->appId&secret=$this->appSecret";
	    }

      $res = json_decode($this->httpGet($url));
      $access_token = $res->access_token;
      if ($access_token) {
        $access_token_info->expire_time = time() + 7000;
        $access_token_info->access_token = $access_token;
        S('access_token_'.$this->token,$access_token_info, 6900);	
      }
			$access_token_info = S('access_token_'.$this->token);

    }  
    \Log::write("public_access_token".$this->token,'WARN');
    \Log::write("access_token".$access_token_info->access_token,'WARN');

    return $access_token_info->access_token;
  }
  
  //重新创建accessToken
  public function recreateAccessToken(){
      $access_token_info = S('access_token_'.$this->token);
      // 如果是企业号用以下URL获取access_token
      if ($this->isQy){
          $url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$this->appId&corpsecret=$this->appSecret";
      }
      else{
          $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$this->appId&secret=$this->appSecret";
      }
  
      $res = json_decode($this->httpGet($url));
      $access_token = $res->access_token;
      if ($access_token) {
          $access_token_info->expire_time = time() + 7000;
          $access_token_info->access_token = $access_token;
          S('access_token_'.$this->token,$access_token_info, 6900);
      }
      $access_token_info = S('access_token_'.$this->token);
      \Log::write("public_access_token".$this->token,'WARN');
      \Log::write("access_token".$access_token_info->access_token,'WARN');
      return $access_token_info->access_token;
  }

  private function httpGet($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 500);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_URL, $url);

    $res = curl_exec($curl);
    curl_close($curl);

    return $res;
  }
}

?>