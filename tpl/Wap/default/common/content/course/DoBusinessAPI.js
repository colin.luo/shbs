

function LMSInitializeMethod(parameter){

  var keySet = valueMap.keySet(); 
  
  for(var i in keySet){ 
  	
    //doLMSSetValue(keySet[i],valueMap.get(keySet[i]));
    //alert(keySet[i]);
  } 

 
 //alert("[初始化方法:]"+parameter);

return "true";

}

function LMSFinishMethod(parameter){
//alert("【LMSFinishMethod:】"+parameter);
LMSSubmitData(g_curr_type, g_curr_value);

return "true";

}

function LMSCommitMethod(parameter){
	LMSSubmitData(g_curr_type, g_curr_value);
//提交页面参数
return "true";

}

function LMSGetValueMethod(element){
//alert("【LMSGetValueMethod:】"+element);
var backData=valueMap.get(element);
if (!backData){
	return "";
}
return  backData;

}

function LMSSetValueMethod(element, value){
//alert("【LMSSetValueMethod:(element:【"+element+"】&"+"value:【"+value+"】)");
valueMap.put(element, value);
g_curr_type = element;
g_curr_value = value;

return "true";

}

function LMSGetErrorStringMethod(errorCode){
return "No error";

}

function LMSGetLastErrorMethod(){
	//alert("LMSGetLastErrorMethod");
	return "0";
}

function LMSGetDiagnosticMethod(errorCode){
	//alert("LMSGetDiagnosticMethod");
	return "No error. No errors were encountered. Successful API call.";

}
 


function logout_onclick() 
{
   // two known potential difficulties exist with having the logout
   // button in this frame...   The first is that the user may not
   // have exited the lesson before attempting to log out.  The second
   // problem is that a child window may be open containing the lesson
   // if the user has not exited.   To deal with these two cases, we'll
   // force the user to exit the lesson before we allow a logout.

   if (LMSIsInitialized() == true)
   {
      // we're making an assumtion that the user is trying
      // to log out without first exiting the lesson via the
      // appropriate means - because if the user had exited
      // the lesson, the LMS would not still be initialized.
      computeTime();
      var  mesg = "You must exit the lesson before you logout";
      //alert(mesg);
   }
  

   return;
}