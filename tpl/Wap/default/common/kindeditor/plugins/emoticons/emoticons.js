/*******************************************************************************
 * KindEditor - WYSIWYG HTML Editor for Internet
 * Copyright (C) 2006-2011 kindsoft.net
 *
 * @author Roddy <luolonghao@gmail.com>
 * @site http://www.kindsoft.net/
 * @licence http://www.kindsoft.net/license.php
 *******************************************************************************/

KindEditor.plugin('emoticons', function(K) {
	var self = this,
		name = 'emoticons',
		path = (self.emoticonsPath || self.pluginsPath + 'emoticons/images/'),
		allowPreview = self.allowPreviewEmoticons === undefined ? true : self.allowPreviewEmoticons,
		currentPageNum = 1;
	self.clickToolbar(name, function() {
		var rows = 3,
			cols = 9,
			total = 135,
			startNum = 0,
			cells = rows * cols,
			pages = Math.ceil(total / cells),
			colsHalf = Math.floor(cols / 2),
			wrapperDiv = K('<div class="ke-plugin-emoticons"></div>'),
			elements = [],
			menu = self.createMenu({
				name: name,
				beforeRemove: function() {
					removeEvent();
				}
			});

		menu.div.append(wrapperDiv);
		var previewDiv, previewImg;
		if (allowPreview) {
			previewDiv = K('<div class="ke-preview"></div>').css('right', 0);
			previewImg = K('<img class="ke-preview-img" src="' + path + startNum + '.gif" />');
			wrapperDiv.append(previewDiv);
			previewDiv.append(previewImg);
		}

		menu.div.removeClass('ke-menu');
		menu.div.css('left', menu.options.x-5+'px');
		menu.div.css('right', '7px');

		function bindCellEvent(cell, j, num) {
			if (previewDiv) {
				cell.mouseover(function() {
					if (j > colsHalf) {
						previewDiv.css('left', 0);
						previewDiv.css('right', '');
					} else {
						previewDiv.css('left', '');
						previewDiv.css('right', 0);
					}
					previewImg.attr('src', path + num + '.gif');
					K(this).addClass('ke-on');
				});
			} else {
				cell.mouseover(function() {
					K(this).addClass('ke-on');
				});
			}
			cell.mouseout(function() {
				K(this).removeClass('ke-on');
			});
			cell.click(function(e) {
//				self.insertHtml('<img src="' + path + num + '.gif" border="0" alt="" />').hideMenu().focus();
				self.insertHtml('<img src="' + path + num + '.gif" border="0" alt="" />').focus();//点击表情不隐藏菜单
				e.stop();
			});
		}

		function createEmoticonsTable(pageNum, parentDiv) {
			var table = document.createElement('table');
			parentDiv.append(table);
			if (previewDiv) {
				K(table).mouseover(function() {
					previewDiv.show('block');
				});
				K(table).mouseout(function() {
					previewDiv.hide();
				});
				elements.push(K(table));
			}
			table.className = 'ke-table';
			table.cellPadding = 0;
			table.cellSpacing = 0;
			table.border = 0;
			var num = (pageNum - 1) * cells + startNum;
			for (var i = 0; i < rows; i++) {
				var row = table.insertRow(i);
				for (var j = 0; j < cols; j++) {
					var cell = K(row.insertCell(j));
					cell.addClass('ke-cell');
					bindCellEvent(cell, j, num);
					var span = K('<span class="ke-img"></span>')
						.css('background-position', '-' + (24 * num) + 'px 0px')
						.css('background-image', 'url(' + path + 'static.gif)');
					cell.append(span);
					elements.push(cell);
					num++;
				}
			}
			return table;
		}
		var table = createEmoticonsTable(currentPageNum, wrapperDiv);

		function removeEvent() {
			K.each(elements, function() {
				this.unbind();
			});
		}
		var pageDiv;

		function showPage(pageNum) {
			if(pageNum <= 0 || pageNum > pages){
				return;
			}
			removeEvent();
			table.parentNode.removeChild(table);
			pageDiv.remove();
			table = createEmoticonsTable(pageNum, wrapperDiv);
			createPageTable(pageNum);
			currentPageNum = pageNum;
		}

		function bindPageEvent(el, pageNum) {
			el.click(function(e) {
				showPage(pageNum);
				e.stop();
			});
		}

		var startX, startY, endX, endY;
		function bindTouchEvent(el) {
			el.addEventListener("touchstart", function(event) {
				//event.preventDefault();
				var touch = event.touches[0];
				startY = touch.pageY;
				startX = touch.pageX;
			}, false);

			el.addEventListener("touchend", function(event) {
				//event.preventDefault();
				if (-100 <= (startX - endX) && (startX - endX) <= 100) {
					return;
				}
				pageNum = currentPageNum;
				if ((startX - endX) > 100) {
					++pageNum;
				} else if ((startX - endX) < -100) {
					--pageNum;
				}
				showPage(pageNum);
			}, false);

			el.addEventListener('touchmove', function(event) {
				event.preventDefault();
				var touch = event.touches[0];
				endX = touch.pageX;
				encX = touch.pageY;
			}, false);

		}
		bindTouchEvent(wrapperDiv[0]);

		function createPageTable(currentPageNum) {
			pageDiv = K('<ul class="ke-page"></ul>');
			wrapperDiv.append(pageDiv);
			for (var pageNum = 1; pageNum <= pages; pageNum++) {
				if (currentPageNum !== pageNum) {
					var a = K('<li class>' + '</li>');
					bindPageEvent(a, pageNum);
					pageDiv.append(a);
					elements.push(a);
				} else {
					pageDiv.append(K('@<li class="active"></li>'));
				}
				pageDiv.append(K('@&nbsp;'));
			}
		}
		createPageTable(currentPageNum);
	});
});