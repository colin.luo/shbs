9月下旬
创建文件夹，引入amaze，创建通用css（内含常用颜色、按钮）
commonDialog.js使用微信的样式重写alert弹出对话框，参数alert('信息','图标地址',是否隐藏确定按钮);

9月30号左右
1.将通用文件放入Common_header.html中，使用时将其包含到head标签中，上拉加载没有放到通用文件中，按需加载相关文件
2.将微信搜索框放入Common_searchBar.html中，按需加载该文件
3.重写通用上拉加载，引用非常简单，详见wap/common/js/pullUpLoad.js以及Demo_pullUpload.html
以上相关改动的使用方式参照template.html
