$(function(){
	var dialogHtml ='<div class="weui_dialog_confirm" id="dialog1" style="display: none;">';
	dialogHtml +='<div class="weui_mask"></div>';
	dialogHtml +='<div class="weui_dialog">';
	dialogHtml +='<div class="weui_dialog_hd"><strong class="weui_dialog_title"></strong></div>';
	dialogHtml +='<div class="weui_dialog_bd" id="dialog_confirm">自定义弹窗内容，居左对齐显示，告知需要确认的信息等</div>';
	dialogHtml +='<div class="weui_dialog_ft">';
	dialogHtml +='<a href="javascript:;"  id="cancelCallback" class="weui_btn_dialog default">取消</a>';
	dialogHtml +='<a href="javascript:;"  id="confirmCallback" class="weui_btn_dialog primary">确定</a>';
	
	dialogHtml +='</div>';
	dialogHtml +='</div>';
	dialogHtml +='</div>';
	
	dialogHtml +='<div class="weui_dialog_alert" id="dialog2" style="display:none;">';
	dialogHtml +='<div class="weui_mask"></div>';
	dialogHtml +='<div class="weui_dialog">';
	dialogHtml +='<div class="weui_dialog_hd"><strong class="weui_dialog_title"></strong></div>';
	dialogHtml +='<div class="weui_dialog_bd" id="dialog_alert">弹窗内容，告知当前页面信息等</div>';
	dialogHtml +='<div class="weui_dialog_ft">';
	dialogHtml +='<a href="javascript:;" class="weui_btn_dialog primary">确定</a>';
	dialogHtml +='</div>';
	dialogHtml +='</div>';
	dialogHtml +='</div>';
	$(document.body).append(dialogHtml);
});

function alert(msg,icon,hideBtn){
	if(icon){
		var str='';
		str+="<div class='cm-alert-icon'>";
			str+="<img src='"+icon+"' width='100%' height='100%' />";
		str+="</div>";
		$(".weui_dialog_hd").html(str);
		var str2='';
		str2+="<div class='cm-alert-title'>"+msg+"</div>";
		$("#dialog_alert").html(str2);
	}else{
		$("#dialog_alert").text(msg);
	}
	
	if(hideBtn){
		$(".weui_dialog_ft").hide();
		setTimeout(function(){
			$('#dialog2').hide();
			$(".weui_dialog_ft").show();
		},2000);
	}
	
	$('#dialog2').show().on('click', '.weui_btn_dialog', function () {
		$('#dialog2').off('click').hide();
	});				
}

function confirm(msg,confirmCallback,cancelCallback,hideDialog){
	$("#cancelCallback").removeAttr("onclick");
	if (cancelCallback){
		$("#cancelCallback").attr("onclick",cancelCallback);
	}
	
	$("#confirmCallback").removeAttr("onclick")
	if (confirmCallback){
		$("#confirmCallback").attr("onclick",confirmCallback);
	}
	if(msg!='')
	$("#dialog_confirm").html(msg);
	$('#dialog1').show().on('click', '.weui_btn_dialog', function (){
		if(!hideDialog){
			$('#dialog1').off('click').hide();
		}
	});				
	
}

