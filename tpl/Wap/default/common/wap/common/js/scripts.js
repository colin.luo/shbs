jQuery(function($) {

	// ---- 全部课程下拉菜单: --------------------------------------------------------
	$('#btn-all-course-menu').click(function(event) {
		event.preventDefault();

		var url = this.href,
				menuid = 'all-course-menu',
				$menu = $('#'+ menuid);

		if ($menu.length > 0) {
			$menu.removeClass('fadeOutUp').addClass('fadeInDown');
			$menu.show();
		} else {
			$menu = $('<div id="'+ menuid +'" class="all-course-menu fadeInDown"></div>').appendTo('body').load(url);
			$menu.on('click', function(event) {

				var target = event.target;

				if (target.nodeName === 'A') {
					var href = target.getAttribute('href');

					if (!href.match(/^(javascript|\#)/)) {
						event.preventDefault();
						$menu.removeClass('fadeInDown').addClass('fadeOutUp');
						setTimeout(function() {
							document.location = href;
						}, 200);
					} else {

						if ($(target).parent().hasClass('opened')) {
							$(target).parent()
								.removeClass('opened').addClass('closed');
						} else {
							$(target).parent()
								.removeClass('closed').addClass('opened')
								.siblings('.opened')
								.removeClass('opened').addClass('closed');
						}
					}
				} else {
					$menu.removeClass('fadeInDown').addClass('fadeOutUp');
					setTimeout(function() {
						$menu.hide();
					}, 400);
				}

			});
		}
	});

	// ---- 课程详情的评论排序:  --------------------------------------------------------------------
	var commentsSortorTicket;
	$('.comments-sortor')
		.on('click', '>a', function(event) {
			var $sorter = $(this).parent();

			if ($sorter.hasClass('active')) {
				$sorter.removeClass('active');
			} else {
				$sorter.addClass('active');

				clearTimeout(commentsSortorTicket);
				commentsSortorTicket = setTimeout(function() {
					$(document).on('click.comments-sortor', function(event) {
						$sorter.removeClass('active');
						$(document).off('.comments-sortor');
					});
				}, 200);
			}
		})
		.on('click', 'li>a', function(event) {
			event.preventDefault();

			var url = this.getAttribute('href');
			$('#comment-ajax-load').load(url);

			$(this).addClass('cur')
				.parents('li:first').siblings('li').find('.cur').removeClass('cur');
		})
		.find('li>a:first').trigger('click');


	// --- 评论“赞”按钮 ------------------------------------------------------------------------
	$('body').on('click', '.btn-agreen', function(event) {
		event.preventDefault();

		var $btn = $(this);
		var url = $btn.attr('href');

		if (!$btn.hasClass('disabled')) {
			$.get(url).then(function(result) {
				if (result.success) {
					var count = parseInt($btn.find('>span').text() || '');
					$btn.find('>span').text(count+1);
					$btn.addClass('disabled');
				} else {
					alert('您已经赞过。');
				}
			});
		}
	});

	// --- 评论：添加回复 ------------------------------------------------------------------------
	$('body').on('click', '.btn-reply', function(event) {
		event.preventDefault();

		var $btn = $(this);
		var url = $btn.attr('href');

		var $modal = $('#modalReply').modal();

		$modal
		.off('click.reply')
		.on('click.reply', 'a.btn-send-reply', function(event) {
			var reply = encodeURIComponent($modal.find('.reply-editor').html());

			$.get(url, {reply: reply}, function(result) {
				$modal.find('.reply-editor').html('');
				alert('回复成功');
			})
			.done(function() {
				$.modal.close();
			});
		});

	});

	// --- 评价按钮: ------------------------------------------------------------------------

	$('body').on('click', '.btn-star', function(event) {
		event.preventDefault();

		var $btn = $(this);
		var url = $btn.attr('href') || $btn.attr('data-url');

		if ($btn.hasClass('disabled')) {
			$('#modalStarSuccess').modal();
			return;
		}

		var $modal = $('#modalStar').modal();

		$modal.off('.star')

		// 评价星星
		.on('click.star', 'a.btn-star-item', function(event) {
			event.preventDefault();

			var $star = $(this);
			$star.parent().attr('data-star', $star.index() + 1);

		})

		// 评价标签
		.on('click.star', 'a.btn-tag', function(event) {
			$modal.find('input').val($(this).text());
		})

		// 评价确定按钮
		.on('click.star', 'a.btn-send-star', function(event) {

			var data = {
				star: parseInt($modal.find('.star-plugin').attr('data-star') || '0'),
				comment: encodeURIComponent($modal.find('input').val())
			};

			$.get(url, data)
			.then(function(result) {
				$.modal.close();
				$modal.find('.star-plugin').attr('data-star', 5);
				$('#modalStarSuccess').modal();

				$btn.addClass('disabled').text('已评价');
				$modal.find('input').val('');
			});
		});

	});

	// ---- 报名按钮： ----------------------------------------------------------------------
	$('body').on('click', '.btn-join', function(event) {
		event.preventDefault();

		var $btn = $(this);
		var url = $btn.attr('href');

		if ($btn.hasClass('disabled')) {
			$('#modalJoinedAlready').modal();
		} else {
			$.get(url).then(function(result) {
				if (result.success) {
					$('#modalJoinSuccess').modal();
					$btn.addClass('disabled').text('已预报名');
				}
			});
		}
	});
});
