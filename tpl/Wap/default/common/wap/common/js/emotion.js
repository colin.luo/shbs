/**
 * 通用表情框的处理
 * zcj 20161021
 */
(function($){
	$.fn.extend({
		emotionInit:function(config){
			var options=$.extend({},defaults,config);
			var textArea=$(this);
			var wordCountTxt=$(this.selector+"Count");
			var emotionContainer=$(this.selector+"EmotionCont");
			var emotionIcon=$(this.selector+'EmotionIcon');
			var maxWord=options['maxWord'];
			var debug=options['debug'];
			
			var c=Number(wordCountTxt.text())||0;
			
			var insertEmotion=function(myValue){
				var $t = textArea[0];
			    if (document.selection) {
			        sel = document.selection.createRange();
			        sel.text = myValue;
			    } else if ($t.selectionStart || $t.selectionStart == '0') {
			        var startPos = $t.selectionStart;
			        var endPos = $t.selectionEnd;
			        var scrollTop = $t.scrollTop;
			        $t.value = $t.value.substring(0, startPos) + myValue + $t.value.substring(endPos, $t.value.length);
			        $t.selectionStart = startPos + myValue.length;
			        $t.selectionEnd = startPos + myValue.length;
			        $t.scrollTop = scrollTop;
			    } else {
			        this.value += myValue;
			    }
			    textArea.blur();
			};
			

			var countWords=function(type){
				var str=textArea.val();
				var reg =/\[[\u4E00-\u9FA5]{1,3}\]/g;
				var result=str.replace(reg,'');
				var arr =str.match(reg);
				if(!arr)
					var count=maxWord-result.length;
				else
					var count=maxWord-arr.length-result.length;
				if(type==1)
					count--;
				wordCountTxt.text(count);
				if(count<=0){
					wordCountTxt.text(0);
				}
				c=Number(wordCountTxt.text())||0;
			};

			var showEmotion=function(){
				var v=emotionContainer.css('visibility');
				if(debug)
					console.log('visibility',v);
				if(v=='hidden'){
					emotionIcon.siblings('.cj-send').removeClass('cm-grey').addClass('cm-blue');
					emotionIcon.attr('src','./tpl/Wap/default/common/wap/content/img/emotion-active.png');
					emotionContainer.addClass('my-emotion-show');
				}else{
					emotionIcon.siblings('.cj-send').addClass('cm-grey').removeClass('cm-blue');
					emotionIcon.attr('src','./tpl/Wap/default/common/wap/content/img/emotion.png');
					emotionContainer.removeClass('my-emotion-show');
				}
			};
			
			var delWords=function(type){
				if(debug)
					console.log('delWords');
				var v=textArea.val();
				var lastWord=v.charAt(v.length - 1);
				if(lastWord!=']'){
					var str=v.substr(0,v.length-1);
					textArea.val(str);
				}else{
					var last=v.lastIndexOf(']');
					var prev=v.lastIndexOf('[');
					var content=String(v.substring(prev+1,last));
					var len=content.length;
					var yes=(new RegExp("^[\\u4e00-\\u9fa5]+$")).test(content);
					if(yes&&len<4){
						if(last>=0&&prev>=0){
							var str=v.substr(0,prev);
							textArea.val(str);
						}
					}else{
						var str=v.substr(0,v.length-1);
						textArea.val(str);
					}
				}
				var $t = textArea[0];
				if (document.selection) {
			        sel = document.selection.createRange();
			    } else if ($t.selectionStart || $t.selectionStart == '0') {
			        var startPos = $t.selectionStart;
			        var endPos = $t.selectionEnd;
			        var scrollTop = $t.scrollTop;
			        $t.selectionStart = startPos;
			        $t.selectionEnd = startPos;
			        $t.scrollTop = scrollTop;
			    }
				if(type=='blur')
					textArea.blur();
				else
					textArea.focus();
				countWords();
			};
			
			textArea.keydown(function(evt){
				if(evt.keyCode==8||evt.keyCode==46){
					evt.preventDefault();
					evt.stopPropagation();
					delWords();
				}else{
					if(c>0){
						if((evt.keyCode>47&&evt.keyCode<58)||(evt.keyCode>64&&evt.keyCode<91)||(evt.keyCode>95&&evt.keyCode<108)||(evt.keyCode>108&&evt.keyCode<112)||(evt.keyCode>185&&evt.keyCode<223)){
							countWords(1);
						}
					}else{
						evt.preventDefault();
						evt.stopPropagation();
					}
				}
			});
			
			emotionIcon.on('click',function(){
				showEmotion();
			});
			
			emotionContainer.find('.my-emo-del').on('click',function(){
				delWords('blur');
			});
			
			emotionContainer.find('.J_emoticon>li>img').on('click',function(){
				if(debug){
					console.log('emoclick');
				}
				
				if(c>0){
					insertEmotion($(this).attr('alt'));
					countWords();
				}
		    });
		}
	});
	
	var defaults={
		maxWord:150,
		debug:0
	};
})(jQuery);