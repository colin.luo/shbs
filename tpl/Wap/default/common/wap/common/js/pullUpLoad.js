/**
 * 通用上拉加载的方法
 * v=1.0 20160930
 * 依赖插件：jquery.min.js,jquery.tmpl.min.js,amazeui.min.js
 * 样式配置:滚动容器绝对定位，例：#wrapper{position:absolute;top:45px;left:0;right:0;bottom:0;overflow:hidden;}
 * 使用方法：
 * 		简化使用：$("#id").pullUpInit({api:api});
 * 		详细配置：$("#id").pullUpInit({api:api,params{page:1,size:50,personId:13},searchBy:'page',tpl:'item',debug:1});
 * 默认参数
   var defaults = {
        api: '',//接口地址
        searchBy: 'pageNo',//分页变量，分页变量必须是param中传递的某个变量
        tpl: 'tpl-list-item',//模板的元素id
        debug: 0,//是否开启调试，开启后console中可以看到调试信息
        params: {//数据参数
            pageNo: 1,
            pageSize: 10
        }
    };
 */

(function($) {
    $.fn.extend({
        "pullUpInit": function(config) {
            var options = $.extend({},defaults,config); //使用jQuery.extend 覆盖插件默认参数
            var $main = $(this);
            var $list = $main.find('.events-list');
            var $pullUpLabel = $main.find('.pull-up-label');
            var $pullUpLabelTxt = $pullUpLabel.text();
            var $pullUp = $main.find('.pull-up');
            var $nodata=$('#pull-up-nodata');
            var stop = 0;

            if (options.debug) 
            	console.log('options', options);

            var searchBy = options.searchBy;
            var tpl = $("#" + options.tpl);
            var next = Number(options['params'][searchBy]) + 1;

            var myScroll = new $.AMUI.iScroll($main[0], {
                click: true
            });

            var sendPara = function(params) {
                var queries = new Array();
                for (var key in params) {
                    if (key !== searchBy) {
                        queries.push('&' + key + '=' + params[key]);
                    }
                }
                return queries.join('');
            };

            var renderList = function(page) {
                var $el = $pullUp;
                $.ajax({
                    url: options.api,
                    data: searchBy + '=' + page + sendPara(options.params),
                    type: 'post',
                    dataType: 'json'
                }).then(function(res) {
                	$pullUpLabel.text($pullUpLabelTxt);
                    if (options.debug) {
                        console.log('res', res);
                        console.log('url', options.api + "&" + searchBy + '=' + page + sendPara(options.params));
                    }
                	if(res.errorCode==0){
                		var html = tpl.tmpl(res.data);
                        var len = res.data.records.length;
                        if (len > 0) {
                            $pullUp.show();
                            $list.append(html);
                        } else if (page == 1 && len <= options.params[searchBy]) {
                            stop = 1;
                            $nodata.show();
                        } else {
                            stop = 1;
                            $pullUp.show();
                            $pullUpLabel.text('———没有更多内容啦———');
                        }

                        setTimeout(function() {
                            myScroll.refresh();
                        },
                        100);
                	}else{
                		stop = 1;
                        $list.html('<div class="am-text-center">无法获取数据!</div>');
                	}
                },
                function() {
                    $pullUpLabel.text('无法连接到服务器');
                }).always(function() {
                    resetLoading($el);
                });
            };

            var setLoading = function($el) {
                $el.addClass('loading');
            };

            var resetLoading = function($el) {
                $el.removeClass('loading');
            };

            var init = function() {
                var pullStart;
                if(options['initLoad']){
                	$list.find('li').eq(0).nextAll().remove();
                }
                $nodata.hide();
                renderList(options['params'][searchBy]);

                myScroll.on('scrollStart',
                function() {
                    pullStart = this.y;
                });

                myScroll.on('scrollEnd',
                function() {
                    if (pullStart === this.y && (this.directionY === 1)) {
                        handlePullUp();
                    }
                });
            };

            var handlePullUp = function() {
                if (!stop) {
                    setLoading($pullUp);
                    renderList(next);
                    next++;
                } else {
                    resetLoading($pullUp);
                }
            }

            $main.bind("touchmove",function(event) {
                event.preventDefault();
            });

            init();
        }
    });

    //默认参数
    var defaults = {
        api: '',
        searchBy: 'pageNo',
        tpl: 'tpl-list-item',
        debug: 0,
        initLoad:1,
        params: {
            pageNo: 1,
            pageSize: 10
        }
    };
})(jQuery)