/**
 * 上拉加载的方法
 * v=4.0
 * 去除totalPage
 * 修改显示上拉加载的条件
 * 新增重置参数的方法reset
 */

var EventsList = function(element,config) {
	//默认参数
    var defaults = {
        api:'',//数据接口地址
        searchBy: 'pageNo',//分页变量，分页变量必须是param中传递的某个变量
        tpl: 'tpl-list-item',//模板引擎的元素id
        debug: 0,//是否开启调试，开启后console中可以看到调试信息
        initLoad:1,//初始化时便读取数据，为false的话要手动调用reset();才能访问数据，用于tab面板或者搜索条件
        ajaxType:'post',//ajax请求方式
        params: {
            pageNo: 1,
            pageSize: 10
        }//数据参数
    };
	var options = $.extend({},defaults,config);
    var $main = $(element);
    var $list = $main.find('.events-list');
    var $pullUpLabel = $main.find('.pull-up-label');
    var $pullUp = $main.find('.pull-up');
    var searchBy=options.searchBy;
    var tpl=$('#'+options['tpl']);
    var initLoad=options.initLoad;
    var ajaxType=options.ajaxType;
    var next = Number(options['params'][searchBy])+1;
    var stop = 0;
    var myScroll;
    var $pullUpLabelTxt=$pullUpLabel.text();
    
    var sendPara = function(params) {
	    var queries =new Array();
	    for (var key in  params) {
	      if (key !== searchBy) {
	        queries.push('&' + key + '=' + params[key]);
	      }
	    }
	    return queries.join('');
	};
    
	if (options.debug) 
    	console.log('options', options);
	
    var renderList = function(page) {
        $.ajax({
            url: options.api,
            data: searchBy + '='+page + sendPara(options.params),
            type: ajaxType,
            dataType: 'json'
        })
        .then(function(res) {
        	if (options.debug) {
                console.log('res', res);
                console.log('url', options.api + "&" + searchBy + '=' + page + sendPara(options.params));
            }
        	$pullUpLabel.text($pullUpLabelTxt);
        	if(res.errorCode==0){
	        	$pullUp.show();
	        	var html = tpl.tmpl(res.data);
	        	var len = res.data.records.length;
	        	if (len>0&&len < options.params['pageSize']) {//数据少于pageSize，停止访问数据
	                stop = 1;
	                $pullUpLabel.removeClass('am-icon-arrow-up');
	                $pullUpLabel.text('———没有更多内容啦———');
	            }else if(len <= 0) {//数据为空时，显示没有数据文字，停止访问数据
	        		stop=1;
	        		$pullUpLabel.removeClass('am-icon-arrow-up');
	        		$pullUpLabel.text('———没有数据———');
	            }else{//数据正常获取
	                stop =0;
	            }
	        	$list.append(html);
	
	            setTimeout(function() {
	                myScroll.refresh();
	            },100);
        	}else{
        		$pullUpLabel.text('获取数据失败！'+res.errorMassage);
        	}
        },
        function() {
        	$pullUpLabel.text('无法连接到服务器');
        }).always(function() {
            resetLoading($pullUp);
        });
    };

    var setLoading = function($el) {
        $el.addClass('loading');
    };

    var resetLoading = function($el) {
        $el.removeClass('loading');
    };

    var handlePullUp = function() {
    	if (!stop) {
            setLoading($pullUp);
            renderList(next);
            next++;
        } else {
            resetLoading($pullUp);
        }
    };
    
    $main.bind("touchmove",function(event) {
        event.preventDefault();
    });
    
    this.init = function() {
    	myScroll = new $.AMUI.iScroll($main[0], {
            click:true
        });
        var pullStart;
        $pullUp.hide();

        //初始化加载
        if(initLoad){
        	renderList(options['params'][searchBy]);
        }
        
        myScroll.on('scrollStart',function() {
            pullStart = this.y;
        });
        
        myScroll.on('scrollEnd',function() {
            if (pullStart === this.y && (this.directionY === 1)) {
                handlePullUp();
            }
        });
    };
    
    this.reset=function(resetOption){
    	$list.empty();
    	$pullUp.hide();
    	options = $.extend({},defaults,resetOption);
        $list = $main.find('.events-list');
        $pullUpLabel = $main.find('.pull-up-label');
        $pullUp = $main.find('.pull-up');
        searchBy=options.searchBy;
        tpl=$('#'+options['tpl']);
        initLoad=options.initLoad;
        ajaxType=options.ajaxType;
        next = Number(options['params'][searchBy])+1;
        stop = 0;
        $pullUpLabelTxt=$pullUpLabel.text();
    	renderList(options['params'][searchBy]);
    }
};