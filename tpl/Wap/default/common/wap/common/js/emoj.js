(function($) {
	var emojItem = '<a href="javascript:void(0);" class="emoj" data-code="$"></a>'
	$.fn.emoj = function(config) {

		var defaults = {
			editor: '.content-editor',
			container: '.face-list'
		};
		var options = $.extend({}, defaults, config);

		var $editor = $(options.editor);
		var $container = $(options.container);
		var faces = [];

		for (var i=0; i<101; i++) {
			faces.push(emojItem.replace(/\$/g, i));
		}
		$container.empty().append(faces.join(''));

		// 过滤粘贴
		function pasteHandler() {

			setTimeout(function() {

				var content = $editor.html();

				valiHTML = ["br"];

				content = content
					.replace(/_moz_dirty=""/gi, "")
					.replace(/\[/g, "[[-")
					.replace(/\]/g, "-]]")
					.replace(/<\/ ?tr[^>]*>/gi, "[br]")
					.replace(/<\/ ?td[^>]*>/gi, "&nbsp;&nbsp;")
					.replace(/<(ul|dl|ol)[^>]*>/gi, "[br]")
					.replace(/<(li|dd)[^>]*>/gi, "[br]")
					.replace(/<p [^>]*>/gi, "[br]")
					.replace(new RegExp("<(/?(?:" + valiHTML.join("|") + ")[^>]*)>", "gi"), "[$1]")
					.replace(new RegExp('<span([^>]*class="?at"?[^>]*)>', "gi"), "[span$1]")
					.replace(/<[^>]*>/g, "")
					.replace(/\[\[\-/g, "[")
					.replace(/\-\]\]/g, "]")
					.replace(new RegExp("\\[(/?(?:" + valiHTML.join("|") + "|img|span)[^\\]]*)\\]", "gi"), "<$1>");

				if (!$.browser.mozilla) {
					content = content .replace(/\r?\n/gi, "<br>");
				}

				$editor.html(content).focus();
			}, 1);
		}

		// 点击表情按钮后，显示表情列表
		$(this).on('click', function(event) {

			if ($container.hasClass('show')) {
				$container.removeClass('show');

				if ($.isFunction(options.onHide) ) {
					options.onHide();
				}
			} else {

				if ($.isFunction(options.onBeforeShow) ) {
					options.onBeforeShow();
				}

				$container.addClass('show');

				setTimeout(function(){

					$editor.focus();

					// $(document).off('.emoj')
					// .on('click.emoj', function(event) {
					// 	$container.removeClass('show');
					// 	if ($.isFunction(options.onHide) ) {
					// 		options.onHide();
					// 	}
					// 	$(document).off('.emoj');
					// });
				}, 200);
			}
		});

		// 选择表情
		$container.on('click', '.emoj', function(event) {
			event.preventDefault();
			var code = $(this).attr('data-code');
			var face = '<img src="'+ options.imgBaseUrl + code +'.gif" />';
			insertEmoj(face);
		});

		//锁定编辑器中鼠标光标位置。。
		function insertEmoj(str) {

			var selection = window.getSelection ? window.getSelection() : document.selection;
			var range = selection.createRange ? selection.createRange() : selection.getRangeAt(0);

			if (!window.getSelection) {

				$editor[0].focus();
				var selection = window.getSelection ? window.getSelection() : document.selection;
				var range = selection.createRange ? selection.createRange() : selection.getRangeAt(0);
				range.pasteHTML(str);
				range.collapse(false);
				range.select();

			} else {

				$editor[0].focus();
				range.collapse(false);
				var hasR = range.createContextualFragment(str);
				var hasR_lastChild = hasR.lastChild;

				while (hasR_lastChild && hasR_lastChild.nodeName.toLowerCase() == "br" && hasR_lastChild.previousSibling && hasR_lastChild.previousSibling.nodeName.toLowerCase() == "br") {
					var e = hasR_lastChild;
					hasR_lastChild = hasR_lastChild.previousSibling;
					hasR.removeChild(e)
				}

				range.insertNode(hasR);
				if (hasR_lastChild) {
					range.setEndAfter(hasR_lastChild);
					range.setStartAfter(hasR_lastChild)
				}
				selection.removeAllRanges();
				selection.addRange(range)
			}
		}
	}


}(jQuery));
