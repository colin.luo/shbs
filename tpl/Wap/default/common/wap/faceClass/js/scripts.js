window._alert = alert;
window.alert = function(text) {

	if (typeof text !== 'string') {
		text = JSON.stringify(text);
	}

	var $dialog = $('<dialog class="modal"><div class="modal-title">温馨提示</div>' +
		'<div class="modal-body">'+ text +'</div>' +
		'<div class="modal-footer"><a href="javascript:void(0)" class="btn btn-block" rel="modal:close">知道了</a></div>' +
	'</dialog>');

	$dialog.on('click', '[rel="modal:close"]', function() {
		setTimeout(function() {
			$dialog.remove();
			$dialog = null;
		}, 200);
	});

	$dialog.modal({
		closeExisting: false
	});
}

jQuery(function($) {

	var MENU_ID = 'all-course-menu';
	var menu_visibled = false;

	function loadAllCourseMenu(show) {
		var url = $('#btn-all-course-menu').attr('href');
		$menu = $('<div id="'+ MENU_ID +'" class="all-course-menu loading"></div>');

		if (show === false) {
			$menu.hide().appendTo('body');
		} else {
			menu_visibled = true;
			$menu.show().appendTo('body');
		}

		$.get(url).then(function(html){
			$menu.html(html).removeClass('loading');
		});

		$menu.on('click', function(event) {

			var target = event.target;

			if (target.nodeName === 'A') {
				var href = target.getAttribute('href');

				if (!href.match(/^(javascript|\#)/)) {
					event.preventDefault();
					$menu.removeClass('fadeIn').addClass('fadeOut');
					setTimeout(function() {
						document.location = href;
					}, 200);
				} else {

					if ($(target).parent().hasClass('opened')) {
						$(target).parent()
							.removeClass('opened').addClass('closed');
					} else {
						$(target).parent()
							.removeClass('closed').addClass('opened')
							.siblings('.opened')
							.removeClass('opened').addClass('closed');
					}
				}
			} else {
				$menu.removeClass('fadeIn').addClass('fadeOut');
				setTimeout(function() {
					$menu.hide();
				}, 400);
			}

		});
	}

	if ($('#btn-all-course-menu').length>0) {
		loadAllCourseMenu(false);
	}

	// ---- 全部课程下拉菜单: --------------------------------------------------------
	$('#btn-all-course-menu').click(function(event) {
		event.preventDefault();

		var $menu = $('#'+ MENU_ID);

		if ($menu.length > 0) {
			if (menu_visibled) {
				menu_visibled = false;
				$menu.hide();
			} else {
				menu_visibled = true;
				$menu.removeClass('fadeOut').addClass('fadeIn');
				$menu.show();
			}
		} else {
			loadAllCourseMenu(true);
		}
	});

	// ---- 扩展列表中链接的点击区域为整个item:  --------------------------------------------------------------------
	$('body').on('click', '.sessional-list dl, .course-list dl', function(event) {
		var $target = $(event.target);

		if (!$target.hasClass('btn') && $target.parents('.btn').length ===0 ) {
			var url = $(this).find('a:not(.btn):first').attr('href');
			window.location.href = url;
		}
		// if ($(event))
	});

	// ---- 课程详情的评论排序:  --------------------------------------------------------------------
	var commentsSortorTicket;
	var commentIscrollInited = false;
	$('.comments-sortor')
		.on('click', '>a', function(event) {
			var $sorter = $(this).parent();

			if ($sorter.hasClass('active')) {
				$sorter.removeClass('active');
			} else {
				$sorter.addClass('active');

				clearTimeout(commentsSortorTicket);
				commentsSortorTicket = setTimeout(function() {
					$(document).on('click.comments-sortor', function(event) {
						$sorter.removeClass('active');
						$(document).off('.comments-sortor');
					});
				}, 200);
			}
		})
		.on('click', 'li>a', function(event) {
			event.preventDefault();

			$('.comemnt-list').empty();

			window.COMMENT_SORT_TYPE = $(this).attr('data-sort');
			window.PULLER.setPageNo(1);

			$('#sort-type-label').html($(this).text());
			$(this).addClass('cur')
				.parents('li:first').siblings('li').find('.cur').removeClass('cur');
		});

	// --- 课程详情底部评论的发送按钮: ------------------------------------------------------------——

	$('body').on('click', '.btn-send-comment', function(event) {
		event.preventDefault();

		var $editor = $('.comments-add .content-editor');
		var comment = $editor.html();
		var url = $(this).attr('href');

		if (!comment) {
			alert('请先输入评论。');
		} else {
			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'JSON',
				data: {comment: encodeURIComponent(comment)},
				success: function() {

					// 评论数+1
					var count = parseInt($('.comment-count').text()||'0');
					$('.comment-count').html(count+1);

					// 禁用评星
					// $('#comment-ajax-load .btn-star').addClass('disabled');

					// 禁用详情页底部的评论
					// window.MY_COMMENT_COUNT = 1;
					// $('.comments-add-wrapper .content-editor').attr('placeholder', '您已经评论过了');
					// $('.comments-add-wrapper .btn-face-seletor').removeClass('btn-face-seletor').attr('disabled','true');

					// 刷新评论列表
					var activeLink = $('.comments-sortor li a.cur');
					activeLink = activeLink.length >0 ? activeLink : $('.comments-sortor li a:first');
					activeLink.trigger('click');
					$editor.html('');
				}
			});
		}
	});

	// --- 评论“赞”按钮： ------------------------------------------------------------------------
	$('body').on('click', '.btn-praise', function(event) {
		event.preventDefault();

		var $btn = $(this);
		var url = $btn.attr('href');
		var cancelUrl = $btn.attr('cancel-url');

		if (!$btn.hasClass('disabled')) {
			// 增加赞
			$.getJSON(url).then(function(result) {
				result = result || {};
				if (result.errorCode == 0) {
					var count = parseInt($btn.find('>span').text() || '');
					$btn.find('>span').text(count+1);
					$btn.addClass('disabled');
				} else {
					//alert('您已经赞过。');
				}
			});
		} else {
			// 取消赞
			$.getJSON(cancelUrl).then(function(result) {
				result = result || {};
				if (result.errorCode == 0) {
					var count = parseInt($btn.find('>span').text() || '');
					$btn.find('>span').text(Math.max(0, count-1));
					$btn.removeClass('disabled');
				} else {
					//alert('您已经赞过。');
				}
			});
		}
	});

	// --- 评论：添加回复 ------------------------------------------------------------------------
	$('body').on('click', '.btn-reply', function(event) {
		event.preventDefault();

		var $btn = $(this);
		var url = $btn.attr('href');

		var $modal = $('#modalReply').modal();


		$modal.find('.reply-editor').html('');

		$modal
		.off('click.reply')
		.on('click.reply', 'a.btn-send-reply', function(event) {
			var reply = encodeURIComponent($modal.find('.reply-editor').html());

			if (!reply) {
				alert('请输入回复内容.');
			} else {

				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'JSON',
					data: {comment: reply},
					success: function(result) {

						if ($('.reply-list').length > 0) {
							window.location.reload();
							window.location.href = window.location.href;
						} else {
							// 显示新增的回复
							var newReply = '<div><em>您</em>'+ decodeURIComponent(reply) +'</div>'
							var $replies = $btn.parents('dd:first').find('.top-replies').show();
							var $replyCount = $replies.find('.reply-count');
							var count = parseInt($replyCount.text()||'0');

							$replyCount.html(count+1);  // 回复数+1
							$replies.prepend(newReply); // 在回复列表中显示新增的回复

							// 隐藏超过2条的回复
							if ($replies.find('>div').length > 2) {
								$replies.find('.more-replies').show(); // 更多回复的链接

								$replies.find('>div').each(function(i, o) {
									if (i>1) {
										$(o).remove();
									}
								});
							}

						}

						$modal.find('.reply-editor').html('');
						$.modal.close();
						alert('回复成功');
					},
					error: function() {
						$.modal.close();
					}
				});
			}
		});

	});

	// --- 评价按钮: ------------------------------------------------------------------------

	$('body').on('click', '.btn-star', function(event) {
		event.preventDefault();

		var $btn = $(this);
		var id = $btn.attr('data-id');
		var url = $btn.attr('href') || $btn.attr('data-url');

		if ((window.MY_COMMENT_COUNT && window.MY_COMMENT_COUNT!=='0') || $btn.hasClass('disabled')) {
			$('#modalStarSuccess').modal();
			return;
		}

		var $modal = $('#modalStar').modal();


		if ($modal.find('.label-spliter').length===0) {
			var $secondTag = $modal.find('.tags .btn-tag').eq(1);
			$('<div class="label-spliter"></div>').insertAfter($secondTag);
		}

		// 评价星星
		$modal.off('.star')
		.on('click.star', 'a.btn-star-item', function(event) {
			event.preventDefault();

			var $star = $(this);
			$star.parent().attr('data-star', $star.index() + 1);
		})

		// 评价标签
		.on('click.star', 'a.btn-tag', function(event) {
			$modal.find('input').val($(this).text());
		})

		// 评价确定按钮
		.on('click.star', 'a.btn-send-star', function(event) {

			var $stars = $modal.find('.star-plugin');
			var data = {
				star: parseInt($stars.attr('data-star') || '0'),
				comment: encodeURIComponent($modal.find('input').val())
			};

			if (!data.comment) {
				alert('请输入评价内容或者选择标签');
			} else {

				data.star = data.star || 5; // 默认5星好评；

				$.ajax({
					url: url,
					data: data,
					dataType: 'JSON',
					type: 'POST',
					success: function(result) {
						result = result || {};

						if (result.errorCode == 0) {
							// 评论数+1
							var $count = $('.comment-count');
							var $target = $count.filter('[data-id="'+ id +'"]');
							if ($target.length > 0) {
								$count = $target;
							}
							var number = parseInt($count.text() || 0);
							$count.html(number + 1);

							// 禁用详情页底部的评论
							// window.MY_COMMENT_COUNT = 1;
							// $('.comments-add-wrapper .content-editor').attr('placeholder', '您已经评论过了');
							// $('.comments-add-wrapper .btn-face-seletor').removeClass('btn-face-seletor').attr('disabled','true');


							// 刷新评论列表
							var activeLink = $('.comments-sortor li a.cur');
							activeLink = activeLink.length >0 ? activeLink : $('.comments-sortor li a:first');
							activeLink.trigger('click');


							$.modal.close();
							$modal.find('.star-plugin').attr('data-star', 5);
							$modal.find('input').val('');
							$btn.addClass('disabled').text('已评价');

							$('#modalStarSuccess').modal();
						} else {
							alert(result.errorMassage || '服务器保存评论失败, 并且response[].errorMassage为空.');
						}
					}
				});
			}
		});

	});

	// ---- 报名按钮： ----------------------------------------------------------------------
	$('body').on('click', '.btn-join', function(event) {
		event.preventDefault();

		var $btn = $(this);
		var url = $btn.attr('href');

		if ($btn.hasClass('disabled')) {
			$('#modalJoinedAlready').modal();
		} else {
			$.getJSON(url).then(function(result) {
				result = result || {};

				if (result.errorCode == 0) {
					$('#modalJoinSuccess').modal();
					$btn.addClass('disabled').text('已预报名');
				} else {
					alert(result.errorMassage || '服务器保存失败, 并且response[].errorMassage为空.');
				}
			});
		}
	});

	// 评论框限制输入字数
	$('body').on('keyup', '.content-editor', function(event) {
		var $editor = $(this);
		var textLength = $editor.text().length;
		var imojLength = $editor.find('img').length;
		var length = textLength + imojLength;
		var maxLength = parseInt($editor.attr('maxlength') || $editor.attr('max-length') || "150");

		if (length > maxLength) {

			var html = $editor.html();
			var imgs = html.match(/<img[^>]*>/g);
			var clearText = '';
			var clearHtml = '';

			if (imgs && imgs.length > 0) {
				var splited = html.split(/<img[^>]*>/);

				for (var i=0; i<splited.length; i++) {
					if ((clearText.length + splited[i].length) < maxLength) {
						clearText += (splited[i] + '$');
						clearHtml += (splited[i] + imgs[i]);
					} else {
						var spaceLength = maxLength - clearText.length;
						clearText += splited[i].substr(0, spaceLength);
						clearHtml += splited[i].substr(0, spaceLength);
					}
				}
			} else {
				clearHtml = html.substr(0, maxLength);
			}
			$editor.html('');
			insertString($editor[0], clearHtml);

			length = maxLength;
		}

		$editor.parent().find('.words-count').text(length + '/'+ maxLength +'字');
	});
});

// 在光标位置插入字符。。
function insertString(elem, str) {

	var selection = window.getSelection ? window.getSelection() : document.selection;
	var range = selection.createRange ? selection.createRange() : selection.getRangeAt(0);

	if (!window.getSelection) {

		elem.focus();
		var selection = window.getSelection ? window.getSelection() : document.selection;
		var range = selection.createRange ? selection.createRange() : selection.getRangeAt(0);
		range.pasteHTML(str);
		range.collapse(false);
		range.select();

	} else {

		elem.focus();
		range.collapse(false);
		var hasR = range.createContextualFragment(str);
		var hasR_lastChild = hasR.lastChild;

		while (hasR_lastChild && hasR_lastChild.nodeName.toLowerCase() == "br" && hasR_lastChild.previousSibling && hasR_lastChild.previousSibling.nodeName.toLowerCase() == "br") {
			var e = hasR_lastChild;
			hasR_lastChild = hasR_lastChild.previousSibling;
			hasR.removeChild(e)
		}

		range.insertNode(hasR);
		if (hasR_lastChild) {
			range.setEndAfter(hasR_lastChild);
			range.setStartAfter(hasR_lastChild)
		}
		selection.removeAllRanges();
		selection.addRange(range)
	}
}