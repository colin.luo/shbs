/**
 * 通用上拉加载的方法
 * v=1.0 20160930
 * 依赖插件：jquery.min.js,jquery.tmpl.min.js,amazeui.min.js
 * 样式配置:滚动容器绝对定位，例：#wrapper{position:absolute;top:45px;left:0;right:0;bottom:0;overflow:hidden;}
 * 使用方法：
 * 		简化使用：$("#id").pullUpInit({api:api});
 * 		详细配置：$("#id").pullUpInit({api:api,params{page:1,size:50,personId:13},searchBy:'page',tpl:'item',debug:1});
 * 默认参数
   var defaults = {
		api: '',//接口地址
		searchBy: 'pageNo',//分页变量，分页变量必须是param中传递的某个变量
		tpl: 'tpl-list-item',//模板的元素id
		debug: 0,//是否开启调试，开启后console中可以看到调试信息
		params: {//数据参数
			pageNo: 1,
			pageSize: 10
		}
	};
 */
(function($) {
	$.fn.extend({
		"pullUpInit": function(config) {
			var options = $.extend({},defaults,config); //使用jQuery.extend 覆盖插件默认参数
			var $main = $(this);
			var $list = $main.find(options.list || '.list');
			var $pullUpLabel = $main.find('.pull-up-label');
			var $pullUpLabelTxt = $pullUpLabel.text();
			var $pullUp = $main.find('.pull-up');
			var $nodata=$('#pull-up-nodata');
			var stop = 0;

			if (options.debug)
				console.log('options', options);

			var searchBy = options.searchBy;
			var tpl = $("#" + options.tpl);
			var next = Number(options['params'][searchBy]) + 1;

			var myScroll = new IScroll($main[0], {
				click: true,
				probeType: 2
				// snap: '.panel-title.fixed'
			});

			var sendPara = function(params) {
				var queries = new Array();
				for (var key in params) {
					if (key !== searchBy) {
						queries.push('&' + key + '=' + params[key]);
					}
				}
				return queries.join('');
			};

			var renderList = function(page) {
				var $el = $pullUp;
				var api = $.isFunction(options.api) ? options.api() : options.api;

				$.ajax({
					url: api,
					data: searchBy + '=' + page + sendPara(options.params),
					type: 'get',
					dataType: 'json'
				}).then(function(res) {
					$pullUpLabel.text($pullUpLabelTxt);
					if (options.debug) {
						console.log('res', res);
						console.log('url', options.api + "&" + searchBy + '=' + page + sendPara(options.params));
					}

					if(res.errorCode==0){
						var html = tpl.tmpl({records: res.data});
						var len = res.data.length;

						// 只要有内容就将其追加到列表中
						if (len > 0) {
							$pullUp.show();
							$nodata.hide();
							$main.find(options.list).append(html);
						}

						if (len == options.params.pageSize) {
							$pullUp.show();
							$nodata.hide();
						} else if (page==1 && len == 0) {
							stop = 1;
							$pullUp.hide();
							$nodata.show();
						} else if (len < options.params.pageSize) {
							stop = 1;
							$pullUpLabel.text('———没有更多内容啦———');
						} else {
							stop = 1;
							$pullUp.show();
							$nodata.hide();
							$pullUpLabel.text('———没有更多内容啦———');
						}

						setTimeout(function() {
							myScroll.refresh();
						}, 100);
					}else{
						stop = 1;
						$main.find(options.list).html('<div class="am-text-center">无法获取数据!</div>');
					}
				},
				function() {
					$pullUpLabel.text('无法连接到服务器');
				}).always(function() {
					resetLoading($el);
				});
			};

			var setLoading = function($el) {
				$el.addClass('loading');
			};

			var resetLoading = function($el) {
				$el.removeClass('loading');
			};

			var init = function() {
				var pullStart;
				setLoading($pullUp);
				if(options['initLoad']){
					$list.find('li').eq(0).nextAll().remove();
				}
				$nodata.hide();
				renderList(options['params'][searchBy]);

				myScroll.on('scrollStart', function() {
					myScroll.__loading = false;
					pullStart = this.y;
				});

				myScroll.on('scroll', function() {

					var top = $('.pull-up').offset().top;

						// alert(myScroll.__loading +',  '+ top +', '+ $main.height());
					if (!myScroll.__loading && top < $main.height()-50) {
						handlePullUp();
					}
				});

				myScroll.on('scrollEnd', function() {
					if (pullStart === this.y && (this.directionY === 1)) {
						handlePullUp();
					}
				});
			};

			var handlePullUp = function() {
				if (!stop) {
					setLoading($pullUp);
					myScroll.__loading = true;
					renderList(next);
					next++;
				} else {
					resetLoading($pullUp);
				}
			}

			$main.bind("touchmove",function(event) {
				event.preventDefault();
			});

			init();

			// Public Methods
			return {
				iscroll: myScroll,
				setPageNo: function(pageNo) {
					next = pageNo || next;
					setLoading($pullUp);
					renderList(next);
					next++;
				},
				resetIScroll: function() {
					myScroll.refresh();
				}
			}
		}
	});

	//默认参数
	var defaults = {
		api: '',
		searchBy: 'pageNo',
		tpl: 'tpl-list-item',
		debug: 0,
		initLoad:1,
		params: {
			pageNo: 1,
			pageSize: 10
		}
	};
})(jQuery)
