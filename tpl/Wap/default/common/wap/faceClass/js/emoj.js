(function($) {
	var emojItem = '<input type="button" href="javascript:void(0);" class="emoj" data-code="$"/>'
	$.fn.emoj = function(config) {

		var defaults = {
			editor: '.content-editor',
			container: '.face-list'
		};
		var options = $.extend({}, defaults, config);
		var inited = false;

		var $editor = $(options.editor);
		var $container = $(options.container).empty();
		var $swiper = $('<div class="swipe"/>').appendTo($container);
		var $swiperNav = $('<div class="swipe-nav"/>').appendTo($container);

		function renderImojs() {
			var $sizeTester = $(emojItem.replace(/\$/g, '0'));
			var $swipeWrap = $('<div class="swipe-wrap"/>').appendTo($swiper);
			var $group;
			var size = $sizeTester.appendTo('body').width();
			var faces = [];
			var cols = Math.floor($container.width() / size);
			var rows = options.row || 2;

			$sizeTester.remove();

			for (var i=0; i<101; i++) {
				if (i!==0 && i%(cols*rows) === 0 || i===100) {
					$group = $('<div class="emojs-group"></div>');
					$group.html(faces.join('')).appendTo($swipeWrap);
					$swiperNav.append('<span class='+ (i===cols*rows ? 'cur' : '') +'/>');
					faces.length = 0;
				}

				if (i!==0 && i%cols === 0) {
					faces.push('<div/>');
				}

				faces.push(emojItem.replace(/\$/g, i));
			}
		}

		// 过滤粘贴
		function pasteHandler() {

			setTimeout(function() {

				var content = $editor.html();

				valiHTML = ["br"];

				content = content
					.replace(/_moz_dirty=""/gi, "")
					.replace(/\[/g, "[[-")
					.replace(/\]/g, "-]]")
					.replace(/<\/ ?tr[^>]*>/gi, "[br]")
					.replace(/<\/ ?td[^>]*>/gi, "&nbsp;&nbsp;")
					.replace(/<(ul|dl|ol)[^>]*>/gi, "[br]")
					.replace(/<(li|dd)[^>]*>/gi, "[br]")
					.replace(/<p [^>]*>/gi, "[br]")
					.replace(new RegExp("<(/?(?:" + valiHTML.join("|") + ")[^>]*)>", "gi"), "[$1]")
					.replace(new RegExp('<span([^>]*class="?at"?[^>]*)>', "gi"), "[span$1]")
					.replace(/<[^>]*>/g, "")
					.replace(/\[\[\-/g, "[")
					.replace(/\-\]\]/g, "]")
					.replace(new RegExp("\\[(/?(?:" + valiHTML.join("|") + "|img|span)[^\\]]*)\\]", "gi"), "<$1>");

				if (!$.browser.mozilla) {
					content = content .replace(/\r?\n/gi, "<br>");
				}

				$editor.html(content).focus();
			}, 1);
		}

		// 点击表情按钮后，显示表情列表
		$(this).on('click', function(event) {

			if ($(this).attr('disabled') || $(this).hasClass('disabled')) {
				return;
			}

			if ($container.hasClass('show')) {
				$container.removeClass('show');

				if ($.isFunction(options.onHide) ) {
					options.onHide();
				}
			} else {

				if ($.isFunction(options.onBeforeShow) ) {
					options.onBeforeShow();
				}

				$container.addClass('show');

				setTimeout(function(){

					if (!inited) {
						inited = true;

						renderImojs();
						window.mySwipe = new Swipe($swiper[0], {
						  startSlide: 0,
						  speed: 400,
						  auto: 0,
						  continuous: true,
						  disableScroll: false,
						  stopPropagation: false,
						  callback: function(index, elem) {},
						  transitionEnd: function(index, elem) {
						  	$swiperNav.children().removeClass('cur').eq(index).addClass('cur');
						  }
						});
					}

					$editor.focus();

					$(document).off('.emoj')
					.on('click.emoj', function(event) {
						var $target = $(event.target);

						if ($target.parents('.face-list').length === 0
									 && !$target.hasClass('face-list')
									 && $target.parents().filter($editor).length==0
									 && $target[0] !== $editor[0]) {
							$container.removeClass('show');
							if ($.isFunction(options.onHide) ) {
								options.onHide();
							}
							$(document).off('.emoj');
						}
					});

				}, 200);
			}
		});

		// 选择表情
		$container.on('click', '.emoj', function(event) {
			event.preventDefault();
			event.stopPropagation();

			this.blur();
			var code = $(this).attr('data-code');
			var face = '<img src="'+ options.imgBaseUrl + code +'.gif" />';
			insertEmoj(face);
		});

		// 在光标位置插入表情。。
		function insertEmoj(str) {

			var selection = window.getSelection ? window.getSelection() : document.selection;
			var range = selection.createRange ? selection.createRange() : selection.getRangeAt(0);

			if (!window.getSelection) {

				$editor[0].focus();
				var selection = window.getSelection ? window.getSelection() : document.selection;
				var range = selection.createRange ? selection.createRange() : selection.getRangeAt(0);
				range.pasteHTML(str);
				range.collapse(false);
				range.select();

			} else {

				$editor[0].focus();
				range.collapse(false);
				var hasR = range.createContextualFragment(str);
				var hasR_lastChild = hasR.lastChild;

				while (hasR_lastChild && hasR_lastChild.nodeName.toLowerCase() == "br" && hasR_lastChild.previousSibling && hasR_lastChild.previousSibling.nodeName.toLowerCase() == "br") {
					var e = hasR_lastChild;
					hasR_lastChild = hasR_lastChild.previousSibling;
					hasR.removeChild(e)
				}

				range.insertNode(hasR);
				if (hasR_lastChild) {
					range.setEndAfter(hasR_lastChild);
					range.setStartAfter(hasR_lastChild)
				}
				selection.removeAllRanges();
				selection.addRange(range)
			}

			$editor.trigger('keyup');
		}
	}


}(jQuery));
