var i=getcookie("time")||60;
function getcookie(name) {
	var cookie_start = document.cookie.indexOf(name);
	var cookie_end = document.cookie.indexOf(";", cookie_start);
	return cookie_start == -1 ? '' : unescape(document.cookie.substring(cookie_start + name.length + 1, (cookie_end > cookie_start ? cookie_end : document.cookie.length)));
}
function setcookie(cookieName, cookieValue, seconds, path, domain, secure) {
    var expires = new Date();
    expires.setTime(expires.getTime() + seconds);
    document.cookie = escape(cookieName) + '=' + escape(cookieValue)
                                            + (expires ? '; expires=' + expires.toGMTString() : '')
                                            + (path ? '; path=' + path : '/')
                                            + (domain ? '; domain=' + domain : '')
                                            + (secure ? '; secure' : '');
}
function timedCount(){
	i=i+1;
	setcookie("time",i,3000);
	postMessage(i);
	setTimeout("timedCount()",1000);
}

timedCount();