
$(function(){

var dialogHtml ='<div class="weui_dialog_confirm" id="dialog1" style="display: none;">';
dialogHtml +='<div class="weui_mask"></div>';
dialogHtml +='<div class="weui_dialog">';
dialogHtml +='<div class="weui_dialog_hd"><strong class="weui_dialog_title"></strong></div>';
dialogHtml +='<div class="weui_dialog_bd" id="dialog_confirm">自定义弹窗内容，居左对齐显示，告知需要确认的信息等</div>';
dialogHtml +='<div class="weui_dialog_ft">';
dialogHtml +='<a href="javascript:;"  id="confirmCallback" class="weui_btn_dialog primary">确定</a>';
dialogHtml +='<a href="javascript:;"  id="cancelCallback" class="weui_btn_dialog default">取消</a>';

dialogHtml +='</div>';
dialogHtml +='</div>';
dialogHtml +='</div>';

dialogHtml +='<div class="weui_dialog_alert" id="dialog2" style="display: none;">';
dialogHtml +='<div class="weui_mask"></div>';
dialogHtml +='<div class="weui_dialog">';
dialogHtml +='<div class="weui_dialog_hd"><strong class="weui_dialog_title"></strong></div>';
dialogHtml +='<div class="weui_dialog_bd" id="dialog_alert">弹窗内容，告知当前页面信息等</div>';
dialogHtml +='<div class="weui_dialog_ft">';
dialogHtml +='<a href="javascript:;" class="weui_btn_dialog primary">确定</a>';
dialogHtml +='</div>';
dialogHtml +='</div>';
dialogHtml +='</div>';
$(document.body).append(dialogHtml);

});

function alert(msg){
	$("#dialog_alert").text(msg);
	$('#dialog2').show().on('click', '.weui_btn_dialog', function () {
	 $('#dialog2').off('click').hide();
	});				
}

function confirm(msg,confirmCallback, cancelCallback){
	
	$("#cancelCallback").removeAttr("onclick")
	if (cancelCallback){
		$("#cancelCallback").attr("onclick",cancelCallback);
	}
	
	$("#confirmCallback").removeAttr("onclick")
	if (confirmCallback){
		$("#confirmCallback").attr("onclick",confirmCallback);
	}
	
	$("#dialog_confirm").text(msg);
	$('#dialog1').show().on('click', '.weui_btn_dialog', function () {
	 $('#dialog1').off('click').hide();
	});				
}

