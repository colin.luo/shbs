/**
 * 上拉加载的方法
 */

var EventsList = function(eleId, options) {
    var $main = $('#' + eleId);
    var $list = $main.find('#events-list');
    var $pullDown = $main.find('#pull-down');
    var $pullDownLabel = $main.find('#pull-down-label');
    var $pullUpLabel = $main.find('#pull-up-label');
    var $pullUpLabelTxt=$pullUpLabel.text();
    var $pullUp = $main.find('#pull-up');
    var topOffset = -$pullDown.outerHeight();
    var searchBy=options.searchBy;
    this.compiler = Handlebars.compile($('#tpi-list-item').html());
    this.prev = this.next = options['params'][searchBy];
    this.total = null;
    
    this.sendPara = function(params) {
	    var queries =new Array();
	    for (var key in  params) {
	      if (key !== searchBy) {
	        queries.push('&' + key + '=' + params[key]);
	      }
	    }
	    return queries.join('');
	};
    
    this.renderList = function(page, type) {
        var _this = this;
        var $el = $pullDown;
        if (type === 'load') {
            $el = $pullUp;
        }
        $.ajax({
            url: options.api,
            data: searchBy + '='+page + _this.sendPara(options.params),
            type: 'post',
            dataType: 'json'
        })
        .then(function(res) {
            _this.total = res.data.totalSize;
           	var totalnum = res.data.totalnum;
            var	totalfinish = res.data.totalfinish;
						var	totalrate = res.data.totalrate;
            var html = _this.compiler(res.data.records);
            if (type === 'refresh') {
            	$list.html(html);
            } else if (type === 'load') {
                $list.append(html);
                
            }else{
            	if(res.data.records.length!=0){
            		$list.html(html);
            		if (totalnum > 0){
var totalstr = '<li><div class="am-fl rank"></div>';
totalstr += '<div class="am-fl name" style="text-decoration: none;">总计</div>';
totalstr += '<div class="am-fl total_number">'+totalnum+'</div>';
totalstr += '<div class="am-fl finished_number">'+totalfinish+'</div>';
totalstr += '<div class="am-fl pass_rate">'+totalrate+'</div></li>';
							$('#events-list').append(totalstr);
						}
            	}else{
            		$list.html('<li><div>没有数据</div></li>');
            	}
            }
            setTimeout(function() {
                _this.iScroll.refresh();
            },
            100);
        },
        function() {
        	$pullUpLabel.text('无法连接到服务器');
        }).always(function() {
            _this.resetLoading($el);
            //使用下拉刷新时，会滚动到上面一段距离以便隐藏‘下拉刷新’的文字和div
            /*if (type !== 'load') {
                _this.iScroll.scrollTo(0, topOffset, 800, $.AMUI.iScroll.utils.circular);
            }*/
        });
    };

    this.setLoading = function($el) {
        $el.addClass('loading');
    };

    this.resetLoading = function($el) {
        $el.removeClass('loading');
    };

    this.init = function() {
    	var myScroll = this.iScroll = new $.AMUI.iScroll('#'+eleId, {
            click: true
        });
        var _this = this;
        var pullFormTop = false;
        var pullStart;
        this.renderList(options['params'][searchBy]);
        myScroll.on('scrollStart',function() {
            if (this.y >= topOffset) {
                pullFormTop = true;
            }
            pullStart = this.y;
        });

        myScroll.on('scrollEnd',function() {
            if (pullFormTop && this.directionY === -1) {
                _this.handlePullDown();
            }
            pullFormTop = false;

            if (pullStart === this.y && (this.directionY === 1)) {
                _this.handlePullUp();
            }
        });
    };

    this.handlePullDown = function() {
//    	this.setLoading($pullDown);
//    	this.prev = this.next = options['params'][searchBy];
//    	this.renderList(this.next, 'refresh');
//    	$pullUpLabel.text($pullUpLabelTxt);
//    	console.log('next',this.next);
    	//alert('res');
    };

    this.handlePullUp = function() {
        if (this.next < this.total) {
        	this.setLoading($pullUp);
            this.next++;
            this.renderList(this.next, 'load');
        } else {
        	this.resetLoading($pullUp);
            $pullUpLabel.text('没有数据了');
        }
    }
};