/**
 * 上拉加载的方法
 * v=3.0
 * 修改total参数为totalPage
 * 新增初始加载参数initLoad
 * 修改显示上拉加载的条件
 * 去除下拉刷新
 * 2016.10.28回滚代码
 */

var EventsList = function(eleId, options) {
    var $main = $('#' + eleId);
    var $list = $main.find('#events-list');
    var $pullDown = $main.find('#pull-down');
    var $pullDownLabel = $main.find('#pull-down-label');
    var $pullUpLabel = $main.find('#pull-up-label');
    var $pullUpLabelTxt=$pullUpLabel.text();
    var $pullUp = $main.find('#pull-up');
    var topOffset =$pullDown.outerHeight();
    var searchBy=options.searchBy;

    var initLoad=options.initLoad||false;
    this.compiler = Handlebars.compile($('#tpi-list-item').html());
    this.next = Number(options['params'][searchBy])+1;
    this.total = options['totalPage'];
    
    this.sendPara = function(params) {
	    var queries =new Array();
	    for (var key in  params) {
	      if (key !== searchBy) {
	        queries.push('&' + key + '=' + params[key]);
	      }
	    }
	    return queries.join('');
	};
    
    this.renderList = function(page, type) {
        var _this = this;
        var $el = $pullDown;
        if (type === 'load') {
            $el = $pullUp;
        }
        $.ajax({
            url: options.api,
            data: searchBy + '='+page + _this.sendPara(options.params),
            type: 'post',
            dataType: 'json'
        })
        .then(function(res) {
        	//内容小于高度隐藏加载文字
            if(page<_this.total){
            	$pullUp.show();
            }
			var rs =res.data.records;
            var html = _this.compiler(rs);
			
            if (type === 'refresh') {
            	$list.html(html);
            } else if (type === 'load') {
                $list.append(html);
                
            }else{
            	if(rs.length!=0){
					if(0<rs.length<11){
						$pullUp.hide();
					}
            		$list.html(html);
            	}else{
					$pullUp.hide();
            		$list.html('<li><div class="am-text-center">没有数据</div></li>');
            	}
            }
            
            setTimeout(function() {
                _this.iScroll.refresh();
            },
            100);
        },
        function() {
        	$pullUpLabel.text('无法连接到服务器');
        }).always(function() {
            _this.resetLoading($el);
        });
    };

    this.setLoading = function($el) {
        $el.addClass('loading');
    };

    this.resetLoading = function($el) {
        $el.removeClass('loading');
    };

    this.init = function() {
    	var myScroll = this.iScroll = new $.AMUI.iScroll('#'+eleId, {
            click: true
        });
    	
        var _this = this;
        var pullStart;
        var pageNo=options['params'][searchBy];
        //初始化加载
        if(initLoad){
        	this.renderList(options['params'][searchBy]);
        }
        
        myScroll.on('scrollStart',function() {
            pullStart = this.y;
        });
        
        myScroll.on('scrollEnd',function() {
            if (pullStart === this.y && (this.directionY === 1)) {
                _this.handlePullUp();
            }
        });
    };

    this.handlePullUp = function() {
        if (this.next <= this.total) {
        	this.setLoading($pullUp);
            this.renderList(this.next, 'load');
            this.next++;
        } else {
        	this.resetLoading($pullUp);
            $pullUpLabel.text('没有数据了');
        }
    }
};