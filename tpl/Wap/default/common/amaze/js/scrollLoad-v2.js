/**
 * 上拉加载的方法
 * v=2.0
 */

var EventsList = function(eleId, options) {
    var $main = $('#' + eleId);
    var $list = $main.find('#events-list');
    var $pullDown = $main.find('#pull-down');
    var $pullDownLabel = $main.find('#pull-down-label');
    var $pullUpLabel = $main.find('#pull-up-label');
    var $pullUpLabelTxt=$pullUpLabel.text();
    var $pullUp = $main.find('#pull-up');
    var topOffset =$pullDown.outerHeight();
    var searchBy=options.searchBy;
    this.compiler = Handlebars.compile($('#tpi-list-item').html());
    this.prev = this.next = options['params'][searchBy];
    this.total = options['total'];
    
    this.sendPara = function(params) {
	    var queries =new Array();
	    for (var key in  params) {
	      if (key !== searchBy) {
	        queries.push('&' + key + '=' + params[key]);
	      }
	    }
	    return queries.join('');
	};
    
    this.renderList = function(page, type) {
        var _this = this;
        var $el = $pullDown;
        if (type === 'load') {
            $el = $pullUp;
        }
        $.ajax({
            url: options.api,
            data: searchBy + '='+page + _this.sendPara(options.params),
            type: 'post',
            dataType: 'json'
        })
        .then(function(res) {
//            _this.total = res.data.totalSize;
            var html = _this.compiler(res.data.records);
            if (type === 'refresh') {
            	$list.html(html);
            } else if (type === 'load') {
                $list.append(html);
                
            }else{
            	if(res.data.records.length!=0){
            		$list.html(html);
            	}else{
            		$list.html('<li><div>没有数据</div></li>');
            	}
            }
            setTimeout(function() {
                _this.iScroll.refresh();
            },
            100);
        },
        function() {
        	$pullUpLabel.text('无法连接到服务器');
        }).always(function() {
            _this.resetLoading($el);
            //使用下拉刷新时，会滚动到上面一段距离以便隐藏‘下拉刷新’的文字和div
            /*if (type !== 'load') {
                _this.iScroll.scrollTo(0, topOffset, 800, $.AMUI.iScroll.utils.circular);
            }*/
            
        });
    };

    this.setLoading = function($el) {
        $el.addClass('loading');
    };

    this.resetLoading = function($el) {
        $el.removeClass('loading');
    };

    this.init = function() {
    	var myScroll = this.iScroll = new $.AMUI.iScroll('#'+eleId, {
            click: true
        });
        var _this = this;
        var pullFormTop = false;
        var pullStart;
        var containerHeight=$('#'+eleId).height();
        var scrollerHeight=$('#'+eleId+' .scroller').height();//第一个子元素即scroller的高度
        //内容小于高度隐藏加载文字
        if(scrollerHeight<containerHeight){
        	$pullUp.hide();
        }
//        this.renderList(options['params'][searchBy]);
        myScroll.on('scrollStart',function() {
        	//topOffset
            if (this.y >= 7) {
                pullFormTop = true;
            }
            pullStart = this.y;
            
        });
        
        myScroll.on('scrollEnd',function() {
            if (pullFormTop && this.directionY === -1) {
                _this.handlePullDown();
            }
            pullFormTop = false;
            
            if (pullStart === this.y && (this.directionY === 1)) {
                _this.handlePullUp();
            }
        });
    };

    this.handlePullDown = function() {
//    	this.setLoading($pullDown);
//    	this.prev = this.next = options['params'][searchBy];
//    	this.renderList(this.next, 'refresh');
//    	$pullUpLabel.text($pullUpLabelTxt);
//    	console.log('next',this.next);
    	//alert('res');
    };

    this.handlePullUp = function() {
        if (this.next <= this.total) {
        	this.setLoading($pullUp);
            this.renderList(this.next, 'load');
            this.next++;
        } else {
        	this.resetLoading($pullUp);
            $pullUpLabel.text('没有数据了');
        }
    }
};