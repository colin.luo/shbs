function zipImg(eleId){
var that=document.querySelector("#"+eleId);
var file = that.files[0];
var URL = window.URL || window.webkitURL;
var blob = URL.createObjectURL(file);
var oImg=new Image();
oImg.onload=function(){
	var w=oImg.width;
	var h=oImg.height;
	if(w>=h)
		var pa={width:1280,fieldName:eleId};
	else
		var pa={height:950,fieldName:eleId};
	lrz(that.files[0],pa).then(function (rst) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '');
        xhr.onload = function () {
            var data = xhr.response;
            if (xhr.status === 200) {
            	$('#outputJs').html(data);
            } else {
            	alert('上传失败');
	            that.value = null;
            }
        };
        xhr.onerror = function (err) {
            alert('未知错误:' + JSON.stringify(err, null, 2));
	            that.value = null;
	        };
	        xhr.send(rst.formData);
	    });
	}
oImg.src=blob;
}
