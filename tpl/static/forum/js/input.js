var myInput = (function(){
	var mi = function(){
		this.maxLength = 500,
		this.currentLength = 0
	}
	mi.prototype = {
		listen: function(thi, evt){
			var that = this;
			if(evt && evt.keyCode && 13==evt.keyCode){
//				evt.preventDefault();
//				var s=thi.innerHTML;
//				thi.innerHTML=s.replace("<div><br></div>","");
			}
			if("/:del" == evt.value){
				thi = evt.srcElement;
				var imgs = thi.querySelectorAll("img");
				if(imgs.length){
					imgs[imgs.length-1].remove();
				}
				return;
			}
			if("paste" == evt.type){
				var text = evt.clipboardData.getData('text/plain');
				if(text.length > (that.maxLength - that.currentLength) ){
					evt.preventDefault();
					evt.stopPropagation();
					return false;
				}
			}

			if(evt.keyCode && evt.keyCode>0 && that.currentLength>=that.maxLength){
				if(evt.keyCode == 8 || evt.keyCode == 46 ){
					
				}else{
					evt.preventDefault();
					evt.stopPropagation();
					return false;
				}
			}
			if(evt.keyCode && -10 == evt.keyCode){
				if(evt.value.length > (that.maxLength - that.currentLength) ){
					return that;
				}
				thi = evt.srcElement;
//				var s=thi.innerHTML;
//				s=s.replace("<div>","");
//				s=s.replace("</div>","");
//				s=s.replace(/<div><br><\/div>/g,"<br>");
//				s=s.replace(/<div>/g,"<br>");
//				s=s.replace(/<\/div>/g,"");
//				thi.innerHTML=s+evt.value;
				this.insertStr(thi,evt.value);
			}

			//不再计算图片表情的字数。
//			var imgs = thi.querySelectorAll("img");
//			var em_count = 0;
//			for(var i=0,ci; ci = imgs[i]; i++){
//				em_count +=ci.getAttribute("data-innerHTML").length;
//			}
			var fc = document.getElementById("form_count");
			that.currentLength = thi.value.length;

			if(that.maxLength < that.currentLength){
				thi.value = thi.value.slice(0, that.maxLength);
				that.currentLength = that.maxLength;
			}
			
			fc.innerHTML = that.maxLength - that.currentLength + " 字";
		},
		insertStr:function(obj, str){
			if (document.selection) {
		        var sel = document.selection.createRange();
		        sel.text = str;
		    } else if (typeof obj.selectionStart === 'number' && typeof obj.selectionEnd === 'number') {
		        var startPos = obj.selectionStart,
		            endPos = obj.selectionEnd,
		            cursorPos = startPos,
		            tmpStr = obj.value;
		        obj.value = tmpStr.substring(0, startPos) + str + tmpStr.substring(endPos, tmpStr.length);
		        cursorPos += str.length;
		        obj.selectionStart = obj.selectionEnd = cursorPos;
		    } else {
		        obj.value += str;
		    }
		},
		active: function(thi, evt){
			var that = this;
			that.curPos = getCaretCharacterOffsetWithin(thi);
			console.log('currPos',that.curPos);
			return that;
		}
	}

	return new mi();
})();