<!--
/* 错题集新增：
*/
$(function () {
    $('#win_eidt').window({
        onBeforeClose: function () {
        	 $("#table3_1_edit").tabs('close', '任务内容设置');
        	 var columnPid = $('#columnPid').val();
        	  var columnId = $('#columnId').val();
        	 if (columnId > 0){
        	 	viewsutask(columnPid);
            	}
            	else{
               $('#dg-tt_rw').datagrid('reload');
             }
        }
    });
  	datagrid_partion_style("dg-tt_rw");
  	
});
function return_pre(){
	var columnids = $('#return_pre').attr('columnids');
	var columnidsarr = columnids.split(',');
	if (columnidsarr.length > 1){
		var precolumnid = columnidsarr[columnidsarr.length-2];
		columnidsarr.pop();
		if (columnidsarr.length <=1){
			$('#span_column').hide();
			$("#columnPid").val('');
		}
		else{
			$("#columnPid").val(precolumnid);
		}
		
		$('#return_pre').attr('columnids',columnidsarr.join(","))
		var url = "{pigcms::U('Management/view',array('check'=>'rw','id'=>$projectId))}&parent_id="+precolumnid;	
		$('#dg-tt_rw').datagrid({
				url: url
		});
	}
	else{
		$('#span_column').hide();
		$("#columnPid").val('');
	}
}
		
function viewsutask(pid){
	
	var url = "{pigcms::U('Management/view',array('check'=>'rw','projectId'=>$projectId))}&parent_id="+pid;	
	$('#dg-tt_rw').datagrid({
			url: url
	});
	var columnids = $('#return_pre').attr('columnids');
	var checkdd = columnids.indexOf(pid);
	//alert(ii);
	if (checkdd <=0){
		$('#return_pre').attr('columnids',columnids+","+pid);	
	}
	
	$("#columnPid").val(pid);	
	$('#span_column').show();		
	
}
function formatStatusOper(val,row,index){
	var et = '<a style="margin-left:5px;" href="#" onclick="setstatus('+"'"+row['id']+"','"+row['status']+"'"+')">'+row['status']+'</a>';
	return et;
}
function setstatus(id,status){
	//alert(status);
	$.post("{pigcms::U('Management/task',array('check'=>'setStatus'))}",{'id':id,'status':status},function(data){
		$('#dg-tt_rw').datagrid('loadData', { total: 0, rows: [] });//清空下方DateGrid 
		$('#dg-tt_rw').datagrid('load');
	});/**/
}
function formatOper_edit(val,row,index){
	var et ='';
	if (row['hasTasks']){
		et ='<span style="margin-left:8px;"  title="添加子栏目">添加子栏目</span>'+
		'<span style="margin-left:8px;"  title="查看">查看子栏目</span>';
	}
	else{
		
		et ='<a style="margin-left:8px;" href="#" onclick="acton3_2(0,'+row['id']+')" title="添加子栏目">添加子栏目</a>'+
		'<a style="margin-left:8px;" href="#" onclick="viewsutask('+row['id']+')" title="查看">查看子栏目</a>';
	}
	et =et+'<a style="margin-left:8px;" href="#" onclick="acton3_2('+row['id']+',0)" title="编辑">编辑</a>'+
	'<a style="margin-left:8px;" href="#" onclick="projec_del('+row['id']+')" title="删除">删除</a>'
	return et;
}

function projec_del(id){
	$.post("{pigcms::U('Management/task',array('check'=>'delete'))}",{'id':id},function(data){
		//alert(data);
		$('#dg-tt_rw').datagrid('loadData', { total: 0, rows: [] });//清空下方DateGrid 
		$('#dg-tt_rw').datagrid('load');
	});
}
			
$('#form2_edit').form({
	success:function(data){
		var data = eval('(' + data + ')'); 
		
		if (data.code >0){
			//$("#columnId").val(data.code);
			//$("#columnId2").val(data.code);
		}
		$.messager.alert('info', data.desc, 'info');
	}
});
function loadData(tabCode) {
	var columnId = $('#columnId').val();
	var url =  "{pigcms::U('Management/getTasks')}&id="+columnId;
	datagrid_init('dg-'+tabCode,url);
	$('#allcheck-'+tabCode).attr("checked",false);
}
		
function select_tab(){
	var title = $('#title').val();
	if (title==''){
		//alert('请先保存错题集名称');
		//return;
	}
	var url = "{pigcms::U('Management/examContent')}";
	//alert(url);return false;
    $("#table3_1_edit").tabs('add',{
        title: '任务内容设置',
        content: createTabContent(url),
        closable: false,
        index: 1
   
    });
}

function createTabContent(url){
   return '<iframe id="tasklists" style="width:100%;height:100%;" scrolling="auto" frameborder="0" src="' + url + '"></iframe>';
}
	
	function formatOper_task(val,row,index){
	var et =
	'<a style="margin-left:8px;" href="#" onclick="task_edit('+index+')" title="编辑">编辑</a>'
	+'<a style="margin-left:8px;" href="#" onclick="task_batchdel('+row['id']+')" title="删除">删除</a>'

	return et;
}
function task_edit(index){
	var row = $('#dg-task').datagrid('getData').rows[index];
	
	if (row['is_compulsory'] =='必修'){
		$("#is_compulsory1").attr("checked","checked");
	}
	else{
		$("#is_compulsory2").attr("checked","checked");
	}
	$("#task_id2").val(row['id']);
	$("#task_name2").text(row['title']);
	$("#task_sort").val(row['rank']);
	$("#task_time").datebox('setValue',row['start_time']);
	$("#win_task_eidt").window("open");				
}
var g_need_update = 1;
function task_batchdel(id){
		if (id ==0){
	  	var checkedItems = $('#dg-task').datagrid('getChecked');
	    var ids = [];
	    $.each(checkedItems, function(index, item){
	      ids.push(item.id);
	    }); 
	    if (ids.length <=0){
	    	alert("至少选择一条！");
	    	return;
	    }
	    	    
			var id = ids.join(",");
		}
		$.post("{pigcms::U('Management/action',array('check'=>'edit_get_rw_2_delete'))}",{'ids':id},function(data){
				//alert('删除成功');
			loadData("task"); 
			g_need_update = 1; 
		});	
}

function batchcompulsory(type){ 

  	var checkedItems = $('#dg-task').datagrid('getChecked');
    var ids = [];
    $.each(checkedItems, function(index, item){
      ids.push(item.id);
    }); 
    if (ids.length <=0){
    	alert("至少选择一条！");
    	return;
    }
	    
	var id = ids.join(",");	
	$.post("{pigcms::U('Management/changeTaskStatus')}",{'ids':ids,'compulsory':type},function(data){
			//alert('修改成功');
			loadData("task"); 
	});
}
	
	function fn_check_task(index){  

  if ($('#allcheck-'+index).is(":checked")) { 
     $('#dg-'+index).datagrid('checkAll');
  }
  else{
   $('#dg-'+index).datagrid('uncheckAll');
  }
}
function save(){
	var url = "";
	var project_id = $('#new_projectId').val();
	if (project_id > 0){
		url = '{pigcms::U("edit")}'
	}
	else{
		url = '{pigcms::U("add")}'
	}
	
  	$('#form1').form('submit',{
			url:url,
			onSubmit:function(){
				
			},
		  success:function(data){
		  	var data = eval('(' + data + ')'); 
				$.messager.alert('Info',data.msg, 'info');
				if (data.code > 0 && data.id >0){
					$('#new_projectId').val(data.id);
				}
		 }
	});
}
$('#form2').form({
	  success:function(data){
			$.messager.alert('Info', data, 'info');
	  }
});

var i=0;
function action_load(){
	if(i==0){
		$('#content').datagrid('reload');
	}
}

function fn_check1(){
	if ($('#allcheck_person').is(":checked")) {
	
		$('#detaillist').datagrid('checkAll');
	}
	else{
		$('#detaillist').datagrid('uncheckAll');
	}
}

$(function() {});
//-->