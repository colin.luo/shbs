var initstring='';
initstring+='<div class="areacontent">';
initstring+='<div data-widget="tabs" class="m JD-stock" id="JD-stock">';
initstring+='<div class="mt">';
initstring+='<ul class="tab">';
initstring+='<li id="tab-prov" data-widget="tab-item" class="curr"><a href="#none" class="hover"><em>请选择</em><i></i></a></li>';
initstring+='<li id="tab-city" data-widget="tab-item" style="display:none;"><a href="#none" class=""><em>请选择</em><i></i></a></li>';
initstring+='<li id="tab-area" data-widget="tab-item" style="display:none;"><a href="#none" class=""><em>请选择</em><i></i></a></li>';
initstring+='</ul>';
initstring+='<div class="stock-line"></div>';
initstring+='</div>';
initstring+='<div class="mc" data-area="0" data-widget="tab-content" id="stock_province_item">';
initstring+='<ul class="area-list">';
initstring+='</ul>';
initstring+='</div>';
initstring+='<div class="mc" data-area="1" data-widget="tab-content" id="stock_city_item"></div>';
initstring+='<div class="mc" data-area="2" data-widget="tab-content" id="stock_area_item"></div>';
initstring+='</div>';
initstring+='</div>';             
initstring+='<div onclick="closeTab();" class="close"></div>';
$("#store-selector").append(initstring);
var areaTabContainer=$("#JD-stock .tab li");
var provinceContainer=$("#stock_province_item");
var cityContainer=$("#stock_city_item");
var areaContainer=$("#stock_area_item");
var townaContainer=$("#stock_town_item");
function areaInit(){
	var str='';
	$.ajax({
		url:"/index.php?g=User&a=getProvinces",
		type:'post',
		async:false,
		success:function(res){
			data=JSON.parse(res);
			for(var i=0;i<data.length;i++){
				var name=data[i].name;
				var id=data[i].id;
				str+='<li><a href="#none" onclick="choseProv(this.text,'+id+');" data-value="'+id+'">'+name+'</a></li>';
			}
		},
		error:function(res){
			str='无法连接到服务器';
		}
	});
	
	$("#nameCont").click(function(){
		var status=$("#store-selector .areacontent").is(":hidden");
		if(status){
			$("#store-selector .text").css('border-bottom','none');
			$('#store-selector .areacontent').show();
			$('#stock_province_item .area-list').html(str);
			$(".close").show();
		}else{
			closeTab();
		}
	});
	
	$("#tab-prov").find("a").click(function(){
		areaTabContainer.removeClass("curr");
		areaTabContainer.eq(0).addClass("curr").show();
		areaTabContainer.eq(1).hide();
		areaTabContainer.eq(2).hide();
		provinceContainer.show();
		cityContainer.hide();
		areaContainer.hide();
	});
	
	$("#tab-city").find("a").click(function(){
		areaTabContainer.removeClass("curr");
		areaTabContainer.eq(1).addClass("curr").show();
		areaTabContainer.eq(2).hide();
		provinceContainer.hide();
		cityContainer.show();
		areaContainer.hide();
	});
	
	$("#tab-area").find("a").click(function(){
		areaTabContainer.removeClass("curr");
		areaTabContainer.eq(2).addClass("curr").show();
		provinceContainer.hide();
		cityContainer.hide();
		areaContainer.show();
	});
}

function closeTab(){
	$("#store-selector .areacontent").hide();
	$("#store-selector .close").hide();
	$("#store-selector .text").css('border-bottom','1px solid #95B8E7');
}

function choseProv(val,pid){
	areaTabContainer.eq(0).removeClass("curr").find("em").html(val);
	areaTabContainer.eq(1).addClass("curr").show().find("em").html("请选择");
	areaTabContainer.eq(2).hide();
	provinceContainer.hide();
	cityContainer.show();
	areaContainer.hide();
	var v1=areaTabContainer.eq(0).find("em").text();
	$("#cityTxt").html(v1);
	$("#selectedCityId").val(pid);
	$.ajax({
		url:"/index.php?g=User&a=getCityOrArea",
		type:'post',
		data:{pid:pid},
		success:function(res){
			data=JSON.parse(res);
			var s='<ul class="area-list">';
			for(var i=0;i<data.length;i++){
				var name=data[i].name;
				var id=data[i].id;
				s+='<li><a href="#none" onclick="choseCity(this.text,'+id+');" data-value="'+id+'">'+name+'</a></li>';
			}
			s+='</ul>';
			cityContainer.html(s);
		},
		error:function(res){
			cityContainer.html('无法连接到服务器');
		}
	});
}
function choseCity(val,pid){
	provinceContainer.hide();
	areaTabContainer.eq(1).removeClass("curr").find("em").html(val);
	areaTabContainer.eq(2).addClass("curr").show().find("em").html("请选择");
	cityContainer.hide();
	areaContainer.show();
	var v1=areaTabContainer.eq(0).find("em").text();
	var v2=areaTabContainer.eq(1).find("em").text();
	$("#cityTxt").html(v1+'|'+v2);
	$("#selectedCityId").val(pid);
	$.ajax({
		url:"/index.php?g=User&a=getCityOrArea",
		type:'post',
		data:{pid:pid},
		success:function(res){
			data=JSON.parse(res);
			var s='<ul class="area-list">';
			for(var i=0;i<data.length;i++){
				var name=data[i].name;
				var id=data[i].id;
				s+='<li><a href="#none" onclick="choseArea(this.text,'+id+');" data-value="'+id+'">'+name+'</a></li>';
			}
			s+='</ul>';
			areaContainer.html(s);
		},
		error:function(res){
			areaContainer.html('无法连接到服务器');
		}
	});
}

function choseArea(v3,id){
	var v1=areaTabContainer.eq(0).find("em").text();
	var v2=areaTabContainer.eq(1).find("em").text();
	var val=v1+'|'+v2+'|'+v3;
	$("#cityTxt").html(val);
	$("#selectedCityId").val(id);
	closeTab();
	areaTabContainer.eq(2).find("em").html(v3);
}

$(function(){
	areaInit();
});