function closeWin(eleId){
	$("#"+eleId).window('close');
}
function openWin(eleId){
	$("#"+eleId).window('open');
}
function showAddNewWin(){
	for(var i=1;i<7;i++){
		$("#input"+i).val($("#diy"+i).text());
	}
	openWin('window_addNew');
}
function checkAddNew(val,id){
	var reg=/^[\u4E00-\u9FA5]{1,2}$/;
	if(val==''){
		$("#addNewTxt"+id).text('输入为空!');
		return;
	}
	if(!reg.test(val)){
		$("#addNewTxt"+id).text('输入的不是两位以下的汉字!');
		return;
	}
	$("#addNewTxt"+id).text('');
}
function loadRequiredLv(){
	ajaxLoading('.content');
	$.ajax({
		url:"/index.php?g=User&m=Integral&a=loadRequiredLv",
		type:'post',
		success:function(data){
			data=JSON.parse(data);
			ajaxLoadEnd();
			if(data.errorCode==0){
				$("#requiredLv tr").eq(0).nextAll().remove();
				var str='';
				for(var i=0;i<data['data'].length;i++){
					var lv=data['data'][i]['honor'];
					var start_lev=data['data'][i]['start_lev'];
					var end_lev=data['data'][i]['end_lev']; 
					var start_s=data['data'][i]['start_s'];
					var end_s=data['data'][i]['end_s'];
					var id=data['data'][i]['id'];
					var maxLv=$('#maxLv').val();
					var start;
					var prevStartIndex=i-1;
					start=$("#end_s"+prevStartIndex).text();
					start++;
					if(i==0){
						start=0;
						str="<tr><td>"+lv+"</td><td><span class='warning'></span><input type='text' name='lv[startLv0]' readonly='readonly' id='startLv0' value='1' style='border:none;' class='editLv'/>-<input type='text' name='lv[endLv"+i+"]' onblur='changeLv("+i+");' value='"+end_lev+"' id='endLv"+i+"' class='editLv inputTxt' /><input type='hidden' name='lv[lvId"+i+"]' value='"+id+"' /></td><td><span id='start_s"+i+"'>"+start+"</span>-<span id='end_s"+i+"'>"+end_s+"</span></td></tr>";
					}else if(i==5){
						str="<tr><td>"+lv+"</td><td><span class='warning'></span><input type='text' name='lv[startLv"+i+"]' id='startLv"+i+"'  value='"+start_lev+"' readonly='readonly' class='editLv'>-<input type='text' name='lv[endLv"+i+"]' readonly='readonly' value='"+maxLv+"' id='endLv"+i+"' class='editLv' /><input type='hidden' name='lv[lvId"+i+"]' value='"+id+"' /></td><td><span id='start_s"+i+"'>"+start+"</span>-<span id='end_s"+i+"'></span></td></tr>";
						getCentByLv(maxLv,'end_s5');
					}else{
						str="<tr><td>"+lv+"</td><td><span class='warning'></span><input type='text' name='lv[startLv"+i+"]' id='startLv"+i+"'  value='"+start_lev+"' readonly='readonly' class='editLv'>-<input type='text' name='lv[endLv"+i+"]' onblur='changeLv("+i+");' value='"+end_lev+"' id='endLv"+i+"' class='inputTxt editLv' /><input type='hidden' name='lv[lvId"+i+"]' value='"+id+"' /></td><td><span id='start_s"+i+"'>"+start+"</span>-<span id='end_s"+i+"'>"+end_s+"</span></td></tr>";
					}
					var currHonor=data['data'][i]['honor_id'];
					$("#currHonor").val(currHonor);
					$("#requiredLv").append(str);
				}
				
			}else{
				$.messager.alert('Info','获取所需等级失败！'+data.errorMassage,'error');
			}
		},
		error:function(res){
			$.messager.alert('Info','无法连接到服务器!','error');
		}
	});
}
function loadLvAndCents(){
	ajaxLoading('#window_cents');
	$.ajax({
		url:"/index.php?g=User&m=Integral&a=loadLvAndCents",
		type:'post',
		dataType:'json',
		success:function(res){
			ajaxLoadEnd();
			if(res.errorCode==0){
				$("#add_cents tr").eq(0).nextAll().remove();
				for(var i=0;i<res.data.length;i++){
					$("#add_cents").append("<tr><td><input type='checkbox' /></td><td><span class='lv'></span></td><td><span class='lv'></span></td><td><input type='text' class='inputTxt' onblur='checkAddCent();' value='"+res['data'][i]['score']+"' /><label class='red'></label></td></tr>");
				}
				orderLv();
			}else{
				$.messager.alert('Info','获取积分和等级失败！'+res.errorMassage,'error');
			}
		},
		error:function(res){
			$.messager.alert('Info','无法连接到服务器!','error');
		}
	});
}

function checkLv(id){
	var reg=/^[0-9]*[1-9][0-9]*$/;
	var elem=$("#endLv"+id).parents().find('tr');
	var startVal=Number($("#startLv"+id).val());
	var endVal=Number($("#endLv"+id).val());
	var starts=Number($("#start_s"+id).text());
	var ends=Number($("#end_s"+id).text());
	var flag=0;
	if(endVal!=''&&reg.test(endVal)){
		flag++;
	}
	if(startVal!=''&&startVal<=endVal){
		flag++;
	}
	if(starts!='null'&&ends!='null'){
		flag++;
	}
	if(flag==3){
		elem.find(".warning").text('');
		$("#endLv"+id).removeClass('warning_border');
	}
}
function checkAllLv(){
	var totalFlag=1;
	var id=1;
	var reg=/^[0-9]*[1-9][0-9]*$/;
	$("#requiredLv tr").eq(1).nextAll().each(function(){
		var startVal=Number($("#startLv"+id).val());
		var endVal=Number($("#endLv"+id).val());
		var starts=Number($("#start_s"+id).text());
		var ends=Number($("#end_s"+id).text());
		var flag=true;
		if(endVal==''){
			$(this).find(".warning").text('输入为空!');
			$("#endLv"+id).addClass('warning_border');
			flag=false;
		}
		if(!reg.test(endVal)){
			$(this).find(".warning").text('输入不是正整数!');
			$("#endLv"+id).addClass('warning_border');
			flag=false;
		}
		if(startVal!=''&&startVal>endVal){
			$("#endLv"+id).addClass('warning_border');
			$(this).find(".warning").text('起始等级应小于等于最大等级!');
			flag=false;
		}
		
		if(starts=='null'||ends=='null'){
			$(this).find(".warning").text('没有对应等级!');
			flag=false;
		}
		
		if(flag){
			$("#endLv"+id).removeClass('warning_border');
			$(this).find(".warning").text('');
			totalFlag=totalFlag*1;
		}else{
			totalFlag=totalFlag*0;
		}
		id++;
	});
	return totalFlag;
}

function changeLv(id){
	var val=Number($("#endLv"+id).val());
	var next=val+1;
	var nextId=id+1;
	$("#startLv"+nextId).val(next);
	checkLv(id);
	getCentByLv(val,'end_s'+id);
	getCentByLv(val,'start_s'+nextId,1);
}

function getCentByLv(lv,eleId,plus){
	$.ajax({
		url:"/index.php?g=User&m=Integral&a=getCentByLv",
		type:'post',
		data:{lv:lv},
		dataType:'json',
		success:function(res){
			var cent=res['data'];
			if(plus){
				cent++;
				$("#"+eleId).text(cent);
			}else{
				$("#"+eleId).text(cent);
			}
		},
		error:function(res){
			$.messager.alert('Info','无法连接到服务器!','error');
		}
	});
}
function saveRequiredLv(){
	if(checkAllLv()){
		 $.ajax({
			url:"/index.php?g=User&m=Integral&a=saveRequiredLv",
			type:'post',
			data:$("#lvForm").serialize(),
			dataType:'json',
			success:function(res){
				if(res.errorCode==0){
					$.messager.alert('Info','保存成功!','info');
				}else{
					$.messager.alert('Info','保存失败!'+res.errorMassage,'error');
				}
			},
			error:function(res){
				$.messager.alert('Info','无法连接到服务器!','error');
			}
		});
	}
}
function checkAllCents(){
	if ($('#allcheck-cents').is(":checked")) {
		$("#add_cents input[type='checkbox']").prop('checked',true);
	}
	else{
		$("#add_cents input[type='checkbox']").prop('checked',false);
	}
}
function saveAddNew(){
	var flag=true;
	$("#window_addNew [id^='addNewTxt']").each(function(){
		var t=$(this).text();
		if(t!=''){
			flag=false;
			return false;
		}
	});
	if(!flag){
		return;
	}
	$.ajax({
		url:"/index.php?g=User&m=Integral&a=addHonor",
		type:'post',
		data:$("#formAddNew").serialize(),
		dataType:'json',
		success:function(res){
			if(res.errorCode==0){
				var input1=$("#input1").val();
				$('#diy1').text(input1);
				var input2=$("#input2").val();
				$('#diy2').text(input2);
				var input3=$("#input3").val();
				$('#diy3').text(input3);
				var input4=$("#input4").val();
				$('#diy4').text(input4);
				var input5=$("#input5").val();
				$('#diy5').text(input5);
				var input6=$("#input6").val();
				$('#diy6').text(input6);
				$("#window_addNew").window('close');
			}else{
				$.messager.alert('Info','保存失败!'+res.errorMassage,'error');
			}
		},
		error:function(res){
			$.messager.alert('Info','无法连接到服务器!','error');
		}
	});
}
function showCentsWindow(){
	loadLvAndCents();
	openWin('window_cents');
}
function addCents(){
	$("#add_cents").append("<tr><td><input type='checkbox' /></td><td><span class='lv'></span></td><td><span class='lv'></span></td><td><input type='text' onblur='checkAddCent();' class='inputTxt' /><label class='red'></label></td></tr>");
	orderLv();
	$("#add_cents tr").last().find('.inputTxt').focus();
}

function checkAddCent(){
	var reg=/^[0-9]*[1-9][0-9]*$/;
	var totalFlag=true;
	$("#add_cents tr").eq(0).nextAll().each(function(){
		var flag=true;
		var prev=Number($(this).prev().find('.inputTxt').val());
		var val=Number($(this).find('.inputTxt').val());
		if(val<=prev){
			$(this).find('.inputTxt').next('label').text('等级不得小于等于上一等级！');
			flag=false;
		}
		if(!reg.test(val)){
			$(this).find('.inputTxt').next('label').text('输入的不是正整数！');
			flag=false;
		}
		if(val==''){
			$(this).find('.inputTxt').next('label').text('输入为空！');
			flag=false;
		}
		if(flag){
			$(this).find('.inputTxt').next('label').text('');
			totalFlag=totalFlag*1;
		}else{
			totalFlag=totalFlag*0;
		}
	});
	return totalFlag;
}
function orderLv(){
	var k=0;
	$("#add_cents tr").each(function(){
		$(this).find('.lv').text(k++);
	});
}
function delCents(){
	$("#add_cents input[type='checkbox']").each(function(){
		var checked=$(this).is(":checked");
		if(checked){
			$(this).parents('tr').remove();
		}
	});
	orderLv();
	$('#allcheck-cents').prop('checked',false);
}
function saveCents(){
	var vals=[];
	var flag=checkAddCent();
	$("#add_cents tr").not(":first").each(function(){
		var k=$(this).find('.lv').last().text();
		var val=$(this).find('.inputTxt').val();
		if(val==''){
			$.messager.alert('Info','等级为'+k+'的积分为空!','warning');
			flag=false;
			return false;
		}else{
			vals.push(Number(val));
			return true;
		}
	});
	if(flag){
		$.ajax({
			url:"/index.php?g=User&m=Integral&a=saveCents",
			type:'post',
			data:{vals:vals},
			dataType:'json',
			success:function(res){
				if(res.errorCode==0){
					var max=$("#add_cents tr").last().find('.lv').last().text();
					$("#maxLv").val(max);
					loadRequiredLv();
					$("#window_cents").window('close');
				}else{
					$.messager.alert('Info','保存失败!'+res.errorMassage,'error');
				}
			},
			error:function(res){
				$.messager.alert('Info','无法连接到服务器!','error');
			}
		});
	}
}
function saveBatchAddCents(){
	var vals=$("#batchAdd_textarea").val();
	$.ajax({
		url:"/index.php?g=User&m=Integral&a=batchAddCents",
		type:'post',
		data:{vals:vals},
		dataType:'json',
		success:function(res){
			if(res.errorCode==0){
				loadLvAndCents();
				$("#window_batchAdd").window('close');
			}else{
				$.messager.alert('Info','保存失败!'+res.errorMassage,'error');
			}
		},
		error:function(res){
			$.messager.alert('Info','无法连接到服务器!','error');
		}
	});
}
function choseHonor(thi,id){
	$("#window_lv .lvCont div").removeClass('chose');
	var div=$(thi).find("div").last();
	div.addClass('chose');
	$("#currHonor").val(id);
}
function saveHonor(){
	var id=$("#currHonor").val();
	$.ajax({
		url:"/index.php?g=User&m=Integral&a=choseHonor",
		type:'post',
		data:{id:id},
		dataType:'json',
		success:function(res){
			if(res.errorCode==0){
				loadRequiredLv();
				closeWin('window_lv');
			}else{
				$.messager.alert('Info','保存失败!'+res.errorMassage,'error');
			}
		},
		error:function(res){
			$.messager.alert('Info','无法连接到服务器!','error');
		}
	});
}
function ajaxLoading(ele){
	var w=$(ele).width();
	var left=(w-100)/2;
    $("<div class=\"datagrid-mask\"></div>").css({display:"block",width:"100%",height:$(document).height()}).appendTo($(ele)); 
    $("<div class=\"datagrid-mask-msg\"></div>").html("正在加载，请稍候。。。").appendTo($(ele)).css({display:"block","margin-left":left,"top":350}); 
 } 
 function ajaxLoadEnd(){
     $(".datagrid-mask").remove(); 
     $(".datagrid-mask-msg").remove();             
}