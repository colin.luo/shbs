(function($){

	var AJAX_PROCESS_COUNT = 0;
	$(document).on({
		'ajaxSend': function() {
			AJAX_PROCESS_COUNT ++;
			$('.navbar').addClass('loading');
		},

		'ajaxComplete': function() {
			AJAX_PROCESS_COUNT --;
			if (AJAX_PROCESS_COUNT <= 0) {
				$('.navbar').removeClass('loading');
			}
		}
	})
	.on('click', '[target^="ajax"]', function(event) {
		event.preventDefault();

		var url = $(this).attr('href');
		changeHash(url);
	});

	$.ajaxSetup({
		dataType: 'json',
		cache: false,
		timeout: 15000
	});

	/**
	 * 异步加载页面
	 * @param  {[type]} url    [description]
	 * @param  {[type]} target [description]
	 * @return {[type]}        [description]
	 */
	function loadPage(url, target) {
		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'html',
			success: function(html) {
				$(target).html(html);
			},
			error: function() {

			}
		});
	}

	/**
	 * [encodeHash description]
	 * @param  {String} hash
	 * @return {String}
	 */
	function encodeHash (hash) {
		hash = (hash || document.location.hash || '').replace(/^#/, '');
		return encodeURIComponent(window.btoa ? btoa(hash) : encodeURIComponent(hash));
	}

	/**
	 * [encodeHash description]
	 * @param  {String} hash
	 * @return {String}
	 */
	function decodeHash (hash) {
		hash = (hash || document.location.hash || '').replace(/^#/, '');

		return window.atob ? atob(decodeURIComponent(hash)) : decodeURIComponent(decodeURIComponent(hash));
	}

	/**
	 * 修改hash
	 * @param  {[type]} hash [description]
	 * @return {[type]}      [description]
	 */
	function changeHash (hash) {
		document.location.hash = encodeHash(hash);
	}

	window.getPageParams = function() {
		var url = decodeHash();
		var queryString = url.split('?')[1] || '';
		var query = queryString.split('&').map(function(p) {
			return p.split('=')
		});
		var params = {};

		for (var i=0; i<query.length; i++) {
			params[query[i][0]] = query[i][1] || '';
		}

		return params;
	}

	window.getPageParam = function(key) {
		var params = getPageParams();

		return params[key] || '';
	}

	/**
	 * hash 改变后的处理方法
	 * @return {[type]} [description]
	 */
	function onHashChange () {
		var url = decodeHash();
		loadPage(url, '#main');
	}
	window.addEventListener('hashchange', onHashChange);

	$(function() {

		$('#winWaitShow').fadeOut();
		$('#RightContent').fadeIn();

		// if (document.location.hash) {
		// 	onHashChange();
		// } else {
		// 	// 加载默认页面
		// 	$('.sidebar a.current').trigger('click');
		// }

	});

}(jQuery));


