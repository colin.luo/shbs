<?php
/**
 * 上传附件和上传视频
 * User: Jinqn
 * Date: 14-04-09
 * Time: 上午10:17
 */
include "Uploader.class.php";
$upload_type = "";
function curlGet($url,$param='',$method='get'){
		/*
		$data = http_build_query($param); 
		$opts = array(  
			'http'=>array(  
			'method'=>"POST",  
			'header'=>"Content-type: application/x-www-form-urlencoded\r\n",
			"Content-length:".strlen($data)."\r\n",  
			"Cookie: foo=bar\r\n" . "\r\n",  
			'content' => $data,
			'timeout'=>3600  
			)  
		); 			

		$cxContext = stream_context_create($opts);  							
		$result = file_get_contents($url, false, $cxContext); 	
*/
//ini_set("max_execution_time",3000); 
//set_time_limit(3000); 

		
		$ch = curl_init();
		$header = "Accept-Charset: utf-8";
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		//curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2800);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);

		return $result;	
	}
	
$token = trim($_COOKIE['token'],"\"");

/* 上传配置 */
$base64 = "upload";
switch (htmlspecialchars($_GET['action'])) {
    case 'uploadimage':
        $pathFormat = str_replace("token",$token, $CONFIG['imagePathFormat']);
        $config = array(
            "pathFormat" => $pathFormat,
            "maxSize" => $CONFIG['imageMaxSize'],
            "allowFiles" => $CONFIG['imageAllowFiles']
        );
        $fieldName = $CONFIG['imageFieldName'];
        $upload_type = "image";
        break;
    case 'uploadscrawl':
        $config = array(
            "pathFormat" => $CONFIG['scrawlPathFormat'],
            "maxSize" => $CONFIG['scrawlMaxSize'],
            "allowFiles" => $CONFIG['scrawlAllowFiles'],
            "oriName" => "scrawl.png"
        );
        $fieldName = $CONFIG['scrawlFieldName'];
        $base64 = "base64";
       
        break;
    case 'uploadvideo':
    		$pathFormat = str_replace("token",$token, $CONFIG['videoPathFormat']);
        $config = array(
            "pathFormat" => $pathFormat,
            "maxSize" => $CONFIG['videoMaxSize'],
            "allowFiles" => $CONFIG['videoAllowFiles']
        );
        $fieldName = $CONFIG['videoFieldName'];
         $upload_type = "video";
        break;
    case 'uploadfile':
    default:
        $config = array(
            "pathFormat" => $CONFIG['filePathFormat'],
            "maxSize" => $CONFIG['fileMaxSize'],
            "allowFiles" => $CONFIG['fileAllowFiles']
        );
        $fieldName = $CONFIG['fileFieldName'];
         $upload_type = "other";
        break;
}

/* 生成上传实例对象并完成上传 */
$up = new Uploader($fieldName, $config, $base64);

/**
 * 得到上传文件所对应的各个参数,数组结构
 * array(
 *     "state" => "",          //上传状态，上传成功时必须返回"SUCCESS"
 *     "url" => "",            //返回的地址
 *     "title" => "",          //新文件名
 *     "original" => "",       //原始文件名
 *     "type" => ""            //文件类型
 *     "size" => "",           //文件大小
 * )
 */

/* 返回数据 */

$certificate = trim($_COOKIE['certificate'],"\"");

$localUpload = $up->getFileInfo();
$uploadurl = "http://".$_SERVER['SERVER_NAME'].'/index.php?g=User&m=Upyun&a=kindedtiropic&ismove=0&type='.$upload_type.'&dir='.$upload_type.'&token='.$token.'&certificate='.$certificate;
$uploadData['name'] = $localUpload['title'];

$uploadData['tmpPath'] = $_SERVER['DOCUMENT_ROOT'].$localUpload['url'];//str_replace ($localUpload['title'], "", $localUpload['url']);
$remoteLoadInfo = curlGet($uploadurl, $uploadData);
$remoteLoad = json_decode($remoteLoadInfo,true);
$sortArr = explode("_", $_POST['id']);
$localUpload['title'] = $sortArr[2]."_".$localUpload['title'];
$localUpload['url']= $remoteLoad['url'];
$localUpload['token'] = $_COOKIE['token'];
return json_encode($localUpload);
