var gCurrDataGrid;
function datagrid_partion_style(dg){	
	var pager = $("#"+dg).datagrid('getPager');	
		var options = $("#"+dg).datagrid('getPager').data("pagination").options;  
		var curr = options.pageSize; 
		gCurrDataGrid = dg;
	pager.pagination({
		pageSize: 15,//每页显示的记录条数，默认为10 
		pageList: [15,30,100],//可以设置每页记录条数的列表 
		beforePageText: '',//页数文本框前显示的汉字 
		afterPageText: '    <a href="javascript:void(0);" onclick="skipPage($(this));" style="color: #666;">GO</a>/ {pages} 页', 
		displayMsg: '查询到{total}条', 
			
		layout:['list','sep','prev','links','next','sep','manual'],
		
	});
	$('.pagination').find('table').css('float','right');	
}

function skipPage(obj){
	var pageNum = obj.parent().parent().prev().find("input").val();
	
	if (!pageNum){
		pageNum = 1;
	}
	
	 $('#'+gCurrDataGrid).datagrid({	 		
	      pageNumber:pageNum,
	      pageSize:15,
		  pageList: [15,30,100],
	    }); 
	datagrid_partion_style(gCurrDataGrid);
}

function datagrid_init(dg,url){
	
	$('#'+dg).datagrid('loadData', { total: 0, rows: [] });
	var pagination = $('#'+dg).datagrid('options').pagination;
	if (pagination){
		//var options = $("#"+dg).datagrid('getPager').data("pagination").options;  
		//var curr = options.pageSize; 
		
		$('#'+dg).datagrid({
			url: encodeURI(url),
				pageNumber:1,			
				pageSize:15,
				pageList: [15,30,100],
				
		});		
		datagrid_partion_style(dg);
	}
	else{
		$('#'+dg).datagrid({
				url: encodeURI(url)
		});	
		
	}
}

function datagrid_reload(dg){
	$('#'+dg).datagrid('loadData', { total: 0, rows: [] });
	$('#'+dg).datagrid('reload'); 
}


function fn_check(dg,ck){
	
	if ($('#'+ck).is(":checked")) {
	
		$('#'+dg).datagrid('checkAll');
	}
	else{

		$('#'+dg).datagrid('uncheckAll');
	}
}