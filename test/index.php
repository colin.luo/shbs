<!doctype html>
<html class='no-js'>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="jquery.min.js"></script>
<style type="text/css">
body{padding:15px;;margin:0;}
.green{color:green;}
.red{color:red;}
#url{border:solid 1px lightblue;margin-top:10px;word-break:break-all;padding:5px;border-radius:3px;width:750px;box-sizing:border-box;}
</style>
<script type="text/javascript">
//Jquery Ajax处理跳转
function testApi(){
	var ip="172.16.18.117:8080";//测试环境
	var datetime=new Date().toLocaleTimeString();
	var url = "http://"+ip+"/wetogether/api/";
	var funName=$("#methodName").val();
	url+=funName;
	var data=$("#params").val()+"&companyCode=ruixue_test&certificate=sinoStrong";
	
    $.ajax( {
        url : url,
        type : "post", //传参方式，get 或post
        data : data,
        error : function(msg) {
            console.log('error',msg);
        	$("#txt").removeClass('green').addClass('red');
        	$("#url").html(url+"?"+data+"<br/>");
        	document.getElementById("txt").innerHTML = datetime+"---失败:<br/>" + msg;
        },
        success : function(text) {
        	var str =text;
        	str=JSON.stringify(str,null,4); // 缩进一个tab
        	$("#txt").removeClass('red').addClass('green');
        	$("#url").html(url+"?"+data+"<br/>");
            document.getElementById("txt").innerHTML = datetime+"---成功:<br/>" +str;
            console.log("success",text);
        }
    });
}

function pressEve(e){
	var e = e || window.event; 
	if(e.keyCode == 13){
    	testApi();
	} 
}
</script>
<title>API接口测试</title>
</head>
<body>
    <form method="post" id='form'>
	 方法名:<br><input type='text' id='methodName' autocomplete="on" style='width:750px;' value='' /><br>
	 参数:<br><input type='text' id='params' onKeyPress="pressEve(event)" onKeyDown="pressEve(event)"  style='width:750px;'/><br>
     <input type="button" onKeyPress="" name="doAjax" value="Ajax动态处理" onClick="testApi();"  /><br>
     <div id="url"></div>
     <pre id="txt" class="green"></pre><br>
     </form>
</body>
</html>
    